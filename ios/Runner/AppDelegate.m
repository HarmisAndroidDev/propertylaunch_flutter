#include "AppDelegate.h"
#include "GeneratedPluginRegistrant.h"
#import <UIKit/UIKit.h>
#import "GoogleMaps/GoogleMaps.h"
//#import <location/LocationPlugin.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
      [GMSServices provideAPIKey:@"AIzaSyDynSy7CIAMdBdomnmnO_p4TW8Im6utprA"];

  [GeneratedPluginRegistrant registerWithRegistry:self];
  // Override point for customization after application launch.
  return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
