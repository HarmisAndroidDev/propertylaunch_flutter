import 'package:flutter/material.dart';
import 'package:property_launch/ui/AgentList.dart';
import 'package:property_launch/ui/HistoryPropertyDetailPage.dart';
import 'package:property_launch/ui/OrderHistory.dart';
import 'package:property_launch/ui/OwnerList.dart';
import 'package:property_launch/ui/PropertList.dart';
import 'package:property_launch/ui/ThankYouPage.dart';
import 'package:property_launch/ui/VideoStyleInfo.dart';
import 'package:property_launch/ui/agent/AgentAddPropertyPage.dart';
import 'package:property_launch/ui/agent/AgentEditPropertyPage.dart';
import 'package:property_launch/ui/agent/AgentProfilePage.dart';
import 'package:property_launch/ui/login.dart';
import 'package:property_launch/ui/owner/OwnerEditPropertyPage.dart';
import 'package:property_launch/ui/owner/OwnerAddPropertyPage.dart';
import 'package:property_launch/ui/owner/AreaSpendPage.dart';
import 'package:property_launch/ui/owner/ContactAdminPage.dart';
import 'package:property_launch/ui/owner/InfoAboutYourVideoPage.dart';
import 'package:property_launch/ui/owner/OwnerProfilePage.dart';
import 'package:property_launch/ui/owner/PropertyDetailPage.dart';
import 'package:property_launch/ui/owner/SignUpPage.dart';
import 'package:property_launch/ui/owner/VideoStyleListPage.dart';
import 'package:property_launch/ui/owner/home.dart';
import 'package:property_launch/ui/splash.dart';
import 'package:property_launch/utils/AppConstants.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  final routes = <String, WidgetBuilder>{
    SplashPage.tag: (context) => SplashPage(),
    LoginPage.tag: (context) => LoginPage(),
    HomePage.tag: (context) => HomePage(),
    SignUp.tag: (context) => SignUp(),
    PropertyList.tag:(context) => PropertyList(),
    PropertyDetailPage.tag:(context) => PropertyDetailPage(),
    AddEditPropertyPage.tag:(context) => AddEditPropertyPage(),
    OrderHistory.tag:(context) => OrderHistory(),
    AgentList.tag:(context) => AgentList(),
    OwnerList.tag:(context) => OwnerList(),
    OwnerProfilePage.tag:(context) => OwnerProfilePage(),
    AddPropertyPage.tag:(context) => AddPropertyPage(),
    ContactAdminPage.tag:(context) => ContactAdminPage(),
    AgentProfilePage.tag:(context) => AgentProfilePage(),
    VideoStyleInfo.tag:(context) => VideoStyleInfo(),
    VideoStyleListPage.tag:(context) => VideoStyleListPage(),
    AreaSpendPage.tag:(context) => AreaSpendPage(),
    ThankYouPage.tag:(context) => ThankYouPage(),
    HistoryPropertyDetailPage.tag:(context) => HistoryPropertyDetailPage(),
    InfoAboutYourVideoPage.tag:(context) => InfoAboutYourVideoPage(),
    AgentAddPropertyPage.tag:(context) =>AgentAddPropertyPage(),
    AgentEditPropertyPage.tag:(context)=> AgentEditPropertyPage(),

  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AppConstants.APP_NAME,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.lightBlue, fontFamily: 'Roboto'),
      home:SplashPage(),
      routes: routes,
    );
  }
}
