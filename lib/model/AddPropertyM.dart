class AddPropertyP {
  String _propertyId;
  String _proName;
  String _proAddress;
  String _suburb;
  String _state;
  String _postCode;
  String _estimatedSalePrice;
  String _rentalPriceWeeklyPrice;
  String _propertyInfo;
  String _propertyType;
  String _lat;
  String _long;
  String _airCondition;
  String _gasCooking;
  String _nbnReady;
  String _shed;
  String _grannyFlat;
  String _secondLivingArea;
  String _pool;
  String _eletecticCooking;
  String _entertainingArea;
  String _securitySystem;
  String _study;
  String _userId;
  String _user1Id;
  String _userType;

  AddPropertyP(
      {String propertyId,
      String proName,
      String proAddress,
      String suburb,
      String state,
      String postCode,
      String estimatedSalePrice,
      String rentalPriceWeeklyPrice,
      String propertyInfo,
      String propertyType,
      String lat,
      String long,
      String airCondition,
      String gasCooking,
      String nbnReady,
      String shed,
      String grannyFlat,
      String secondLivingArea,
      String pool,
      String eletecticCooking,
      String entertainingArea,
      String securitySystem,
      String study,
      String userId,
      String user1Id,
      String userType}) {
    this._propertyId = propertyId;
    this._proName = proName;
    this._proAddress = proAddress;
    this._suburb = suburb;
    this._state = state;
    this._postCode = postCode;
    this._estimatedSalePrice = estimatedSalePrice;
    this._rentalPriceWeeklyPrice = rentalPriceWeeklyPrice;
    this._propertyInfo = propertyInfo;
    this._propertyType = propertyType;
    this._lat = lat;
    this._long = long;
    this._airCondition = airCondition;
    this._gasCooking = gasCooking;
    this._nbnReady = nbnReady;
    this._shed = shed;
    this._grannyFlat = grannyFlat;
    this._secondLivingArea = secondLivingArea;
    this._pool = pool;
    this._eletecticCooking = eletecticCooking;
    this._entertainingArea = entertainingArea;
    this._securitySystem = securitySystem;
    this._study = study;
    this._userId = userId;
    this._user1Id = user1Id;
    this._userType = userType;
  }

  String get propertyId => _propertyId;

  set propertyId(String propertyId) => _propertyId = propertyId;

  String get proName => _proName;

  set proName(String proName) => _proName = proName;

  String get proAddress => _proAddress;

  set proAddress(String proAddress) => _proAddress = proAddress;

  String get suburb => _suburb;

  set suburb(String suburb) => _suburb = suburb;

  String get state => _state;

  set state(String state) => _state = state;

  String get postCode => _postCode;

  set postCode(String postCode) => _postCode = postCode;

  String get estimatedSalePrice => _estimatedSalePrice;

  set estimatedSalePrice(String estimatedSalePrice) =>
      _estimatedSalePrice = estimatedSalePrice;

  String get rentalPriceWeeklyPrice => _rentalPriceWeeklyPrice;

  set rentalPriceWeeklyPrice(String rentalPriceWeeklyPrice) =>
      _rentalPriceWeeklyPrice = rentalPriceWeeklyPrice;

  String get propertyInfo => _propertyInfo;

  set propertyInfo(String propertyInfo) => _propertyInfo = propertyInfo;

  String get propertyType => _propertyType;

  set propertyType(String propertyType) => _propertyType = propertyType;

  String get lat => _lat;

  set lat(String lat) => _lat = lat;

  String get long => _long;

  set long(String long) => _long = long;

  String get airCondition => _airCondition;

  set airCondition(String airCondition) => _airCondition = airCondition;

  String get gasCooking => _gasCooking;

  set gasCooking(String gasCooking) => _gasCooking = gasCooking;

  String get nbnReady => _nbnReady;

  set nbnReady(String nbnReady) => _nbnReady = nbnReady;

  String get shed => _shed;

  set shed(String shed) => _shed = shed;

  String get grannyFlat => _grannyFlat;

  set grannyFlat(String grannyFlat) => _grannyFlat = grannyFlat;

  String get secondLivingArea => _secondLivingArea;

  set secondLivingArea(String secondLivingArea) =>
      _secondLivingArea = secondLivingArea;

  String get pool => _pool;

  set pool(String pool) => _pool = pool;

  String get eletecticCooking => _eletecticCooking;

  set eletecticCooking(String eletecticCooking) =>
      _eletecticCooking = eletecticCooking;

  String get entertainingArea => _entertainingArea;

  set entertainingArea(String entertainingArea) =>
      _entertainingArea = entertainingArea;

  String get securitySystem => _securitySystem;

  set securitySystem(String securitySystem) => _securitySystem = securitySystem;

  String get study => _study;

  set study(String study) => _study = study;

  String get userId => _userId;

  set userId(String userId) => _userId = userId;

  String get user1Id => _user1Id;

  set user1Id(String user1Id) => _user1Id = user1Id;

  String get userType => _userType;

  set userType(String userType) => _userType = userType;

  AddPropertyP.fromJson(Map<String, dynamic> json) {
    _propertyId = json['propertyId'];
    _proName = json['proName'];
    _proAddress = json['proAddress'];
    _suburb = json['suburb'];
    _state = json['state'];
    _postCode = json['postCode'];
    _estimatedSalePrice = json['estimatedSalePrice'];
    _rentalPriceWeeklyPrice = json['rentalPriceWeeklyPrice'];
    _propertyInfo = json['propertyInfo'];
    _propertyType = json['propertyType'];
    _lat = json['lat'];
    _long = json['long'];
    _airCondition = json['airCondition'];
    _gasCooking = json['gasCooking'];
    _nbnReady = json['nbnReady'];
    _shed = json['shed'];
    _grannyFlat = json['grannyFlat'];
    _secondLivingArea = json['secondLivingArea'];
    _pool = json['pool'];
    _eletecticCooking = json['eletecticCooking'];
    _entertainingArea = json['entertainingArea'];
    _securitySystem = json['securitySystem'];
    _study = json['study'];
    _userId = json['userId'];
    _user1Id = json['user1Id'];
    _userType = json['userType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.propertyId != null) {
      data['propertyId'] = this._propertyId;
    }
    data['proName'] = this._proName;
    data['proAddress'] = this._proAddress;
    data['suburb'] = this._suburb;
    data['state'] = this._state;
    data['postCode'] = this._postCode;
    data['estimatedSalePrice'] = this._estimatedSalePrice;
    data['rentalPriceWeeklyPrice'] = this._rentalPriceWeeklyPrice;
    data['propertyInfo'] = this._propertyInfo;
    data['propertyType'] = this._propertyType;
    data['lat'] = this._lat;
    data['long'] = this._long;
    data['airCondition'] = this._airCondition;
    data['gasCooking'] = this._gasCooking;
    data['nbnReady'] = this._nbnReady;
    data['shed'] = this._shed;
    data['grannyFlat'] = this._grannyFlat;
    data['secondLivingArea'] = this._secondLivingArea;
    data['pool'] = this._pool;
    data['eletecticCooking'] = this._eletecticCooking;
    data['entertainingArea'] = this._entertainingArea;
    data['securitySystem'] = this._securitySystem;
    data['study'] = this._study;
    data['userId'] = this._userId;
    data['user1Id'] = this._user1Id;
    data['userType'] = this._userType;
    return data;
  }
}
