


import 'package:property_launch/model/owner/OwnerDetails.dart';

class OwnerListmaster {
  String message;
  int success;
  List<OwnerDetails> result;

  OwnerListmaster({this.message, this.success, this.result});

  OwnerListmaster.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    success = json['success'];
    if (json['result'] != null) {
      result = new List<OwnerDetails>();
      json['result'].forEach((v) {
        result.add(new OwnerDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
