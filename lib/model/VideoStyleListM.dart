

class VideoStyleListM {
  String _videoUrl;
  bool _isSelected;


  VideoStyleListM(this._videoUrl, this._isSelected);

  String get videoUrl => _videoUrl;

  set videoUrl(String value) {
    _videoUrl = value;
  }

  bool get isSelected => _isSelected;

  set isSelected(bool value) {
    _isSelected = value;
  }


}