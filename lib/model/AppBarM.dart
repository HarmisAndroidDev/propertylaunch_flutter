class AppBarM {
  bool _isDrawerVisible = false,
      _isBackVisible = false,
      _IsNotificationVisible = false,
      _isProfilePicvisible = false,
      _isReportVisible = false,
      _isLogoutVisible = false;
  String _appBarTitle;

  AppBarM();

  String get appBarTitle => _appBarTitle;

  set appBarTitle(String value) {
    _appBarTitle = value;
  }

  get isLogoutVisible => _isLogoutVisible;

  set isLogoutVisible(value) {
    _isLogoutVisible = value;
  }

  get isReportVisible => _isReportVisible;

  set isReportVisible(value) {
    _isReportVisible = value;
  }

  get isProfilePicvisible => _isProfilePicvisible;

  set isProfilePicvisible(value) {
    _isProfilePicvisible = value;
  }

  get IsNotificationVisible => _IsNotificationVisible;

  set IsNotificationVisible(value) {
    _IsNotificationVisible = value;
  }

  get isBackVisible => _isBackVisible;

  set isBackVisible(value) {
    _isBackVisible = value;
  }

  bool get isDrawerVisible => _isDrawerVisible;

  set isDrawerVisible(bool value) {
    _isDrawerVisible = value;
  }


}
