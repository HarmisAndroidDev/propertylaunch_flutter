

class DrawerItemsM {
  int _index;
  String _image,_name;
  bool _isSelected = false;

  DrawerItemsM(this._index, this._image, this._name, this._isSelected);


  int get index => _index;

  set index(int value) {
    _index = value;
  }

  bool get isSelected => _isSelected;

  set isSelected(bool value) {
    _isSelected = value;
  }

  get name => _name;

  set name(value) {
    _name = value;
  }

  String get image => _image;

  set image(String value) {
    _image = value;
  }


}