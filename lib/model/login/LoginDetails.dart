class LoginDetails {
  String _userId;
  String _userType;
  String _name;
  String _username;
  String _email;
  String _phone;
  String _profileImage;
  String _gender;
  String _dob;
  String _rememberToken;

  LoginDetails(
      {String userId,
        String userType,
        String name,
        String username,
        String email,
        String phone,
        String profileImage,
        String gender,
        String dob,
        String rememberToken}) {
    this._userId = userId;
    this._userType = userType;
    this._name = name;
    this._username = username;
    this._email = email;
    this._phone = phone;
    this._profileImage = profileImage;
    this._gender = gender;
    this._dob = dob;
    this._rememberToken = rememberToken;
  }

  String get userId => _userId;
  set userId(String userId) => _userId = userId;
  String get userType => _userType;
  set userType(String userType) => _userType = userType;
  String get name => _name;
  set name(String name) => _name = name;
  String get username => _username;
  set username(String username) => _username = username;
  String get email => _email;
  set email(String email) => _email = email;
  String get phone => _phone;
  set phone(String phone) => _phone = phone;
  String get profileImage => _profileImage;
  set profileImage(String profileImage) => _profileImage = profileImage;
  String get gender => _gender;
  set gender(String gender) => _gender = gender;
  String get dob => _dob;
  set dob(String dob) => _dob = dob;
  String get rememberToken => _rememberToken;
  set rememberToken(String rememberToken) => _rememberToken = rememberToken;

  LoginDetails.fromJson(Map<String, dynamic> json) {
    _userId = json['userId'].toString();
    _userType = json['userType'];
    _name = json['name'];
    _username = json['username'];
    _email = json['email'];
    _phone = json['phone'];
    _profileImage = json['profileImage'];
    _gender = json['gender'];
    _dob = json['dob'];
    _rememberToken = json['remember_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this._userId;
    data['userType'] = this._userType;
    data['name'] = this._name;
    data['username'] = this._username;
    data['email'] = this._email;
    data['phone'] = this._phone;
    data['profileImage'] = this._profileImage;
    data['gender'] = this._gender;
    data['dob'] = this._dob;
    data['remember_token'] = this._rememberToken;
    return data;
  }
}
