

import 'package:property_launch/model/login/LoginDetails.dart';

class LoginMaster {
  int _success;
  String _message;
  LoginDetails _result;

  LoginMaster({int success, String message, LoginDetails result}) {
    this._success = success;
    this._message = message;
    this._result = result;
  }

  int get success => _success;

  set success(int success) => _success = success;

  String get message => _message;

  set message(String value) {
    _message = value;
  }


  LoginDetails get result => _result;

  set result(LoginDetails value) {
    _result = value;
  }

  LoginMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    _result = json['result'] != null
        ? new LoginDetails.fromJson(json['result'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    if (this._result != null) {
      data['result'] = this._result.toJson();
    }
    return data;
  }
}
