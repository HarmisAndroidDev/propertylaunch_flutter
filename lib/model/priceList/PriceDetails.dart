
class PriceDetails {
  String price;
  String name;

  PriceDetails({this.price, this.name});

  PriceDetails.fromJson(Map<String, dynamic> json) {
    price = json['price'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['name'] = this.name;
    return data;
  }
}