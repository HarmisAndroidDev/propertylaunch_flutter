import 'package:property_launch/model/priceList/PriceDetails.dart';

class PriceMaster {
  String message;
  int success;
  List<PriceDetails> result;

  PriceMaster({this.message, this.success, this.result});

  PriceMaster.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    success = json['success'];
    if (json['result'] != null) {
      result = new List<PriceDetails>();
      json['result'].forEach((v) {
        result.add(new PriceDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

