class PropertyFeaturesM {
  String _featureName;
  bool _isSelected;

  PropertyFeaturesM(this._featureName, this._isSelected);

  String get featureName => _featureName;

  set featureName(String value) {
    _featureName = value;
  }

  bool get isSelected => _isSelected;

  set isSelected(bool value) {
    if (value != null) {
      _isSelected = value;
    } else {
      _isSelected = false;
    }
  }
}
