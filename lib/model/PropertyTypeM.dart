

class PropertyTypeM {

  String _propertyName;
  bool _isSelected;

  PropertyTypeM(this._propertyName, this._isSelected);

  bool get isSelected => _isSelected;

  set isSelected(bool value) {
    _isSelected = value;
  }

  String get propertyName => _propertyName;

  set propertyName(String value) {
    _propertyName = value;
  }


}