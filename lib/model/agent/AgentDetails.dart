

class AgentDetails {
  int userId;
  String userName;
  String email;
  String address;
  String profileImage;
  String userType;

  AgentDetails(
      {this.userId,
        this.userName,
        this.email,
        this.address,
        this.profileImage,
        this.userType});

  AgentDetails.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    userName = json['userName'];
    email = json['email'];
    address = json['address'];
    profileImage = json['profileImage'];
    userType = json['userType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['userName'] = this.userName;
    data['email'] = this.email;
    data['address'] = this.address;
    data['profileImage'] = this.profileImage;
    data['userType'] = this.userType;
    return data;
  }
}
