
import 'package:property_launch/model/agent/AgentDetails.dart';

class AgentListMaster {
  String message;
  int success;
  List<AgentDetails> result;

  AgentListMaster({this.message, this.success, this.result});

  AgentListMaster.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    success = json['success'];
    if (json['result'] != null) {
      result = new List<AgentDetails>();
      json['result'].forEach((v) {
        result.add(new AgentDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

