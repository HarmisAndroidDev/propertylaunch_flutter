import 'package:property_launch/model/ownerPropertyList/ImageDetails.dart';

class OwnerPropertyDetails {
  String _airCondition;
  String _gasCooking;
  String _nbnReady;
  String _shed;
  String _propertyId;
  String _proName;
  String _proAddress;
  String _suburb;
  String _state;
  String _postCode;
  String _estimatedSalePrice;
  String _rentalPriceWeeklyPrice;
  String _propertyInfo;
  String _propertyType;
  String _proTypeName;
  String _propFeature;
  String _bedrooms;
  String _bathrooms;
  String _parking;
  String _ownerId;
  String _agentId;
  String _agentImage;
  String _googleLocation;
  String _lat;
  String _long;
  String _addBy;
  String _agentName;
  String _ownerName;
  String _ownerEmail;
  String _ownerImage;
  String _userName;
  String _userEmail;
  String _profileImage;
  String _userAddress;
  String _userType;
  String _proImg;
  List<String> _feName;
  List<ImageDetails> _propertyImage;

  Result(
      {String airCondition,
        String gasCooking,
        String nbnReady,
        String shed,
        String propertyId,
        String proName,
        String proAddress,
        String suburb,
        String state,
        String postCode,
        String estimatedSalePrice,
        String rentalPriceWeeklyPrice,
        String propertyInfo,
        String propertyType,
        String proTypeName,
        String propFeature,
        String bedrooms,
        String bathrooms,
        String parking,
        String ownerId,
        String agentId,
        String agentImage,
        String googleLocation,
        String lat,
        String long,
        String addBy,
        String agentName,
        String ownerName,
        String ownerEmail,
        String ownerImage,
        String userName,
        String userEmail,
        String profileImage,
        String userAddress,
        String userType,
        String proImg,
        List<String> feName,
        List<ImageDetails> propertyImage}) {
    this._airCondition = airCondition;
    this._gasCooking = gasCooking;
    this._nbnReady = nbnReady;
    this._shed = shed;
    this._propertyId = propertyId;
    this._proName = proName;
    this._proAddress = proAddress;
    this._suburb = suburb;
    this._state = state;
    this._postCode = postCode;
    this._estimatedSalePrice = estimatedSalePrice;
    this._rentalPriceWeeklyPrice = rentalPriceWeeklyPrice;
    this._propertyInfo = propertyInfo;
    this._propertyType = propertyType;
    this._proTypeName = proTypeName;
    this._propFeature = propFeature;
    this._bedrooms = bedrooms;
    this._bathrooms = bathrooms;
    this._parking = parking;
    this._ownerId = ownerId;
    this._agentId = agentId;
    this._agentImage=agentImage;
    this._googleLocation = googleLocation;
    this._lat = lat;
    this._long = long;
    this._addBy=addBy;
    this._agentName = agentName;
    this._ownerName = ownerName;
    this._ownerEmail = ownerEmail;
    this._ownerImage = ownerImage;
    this._userName = userName;
    this._userEmail = userEmail;
    this._profileImage = profileImage;
    this._userAddress = userAddress;
    this._userType = userType;
    this._proImg = proImg;
    this._feName = feName;
    this._propertyImage = propertyImage;
  }


  String get addBy => _addBy;

  set addBy(String value) {
    _addBy = value;
  }

  String get airCondition => _airCondition;
  set airCondition(String airCondition) => _airCondition = airCondition;
  String get gasCooking => _gasCooking;
  set gasCooking(String gasCooking) => _gasCooking = gasCooking;
  String get nbnReady => _nbnReady;
  set nbnReady(String nbnReady) => _nbnReady = nbnReady;
  String get shed => _shed;
  set shed(String shed) => _shed = shed;
  String get propertyId => _propertyId;
  set propertyId(String propertyId) => _propertyId = propertyId;
  String get proName => _proName;
  set proName(String proName) => _proName = proName;
  String get proAddress => _proAddress;
  set proAddress(String proAddress) => _proAddress = proAddress;
  String get suburb => _suburb;
  set suburb(String suburb) => _suburb = suburb;
  String get state => _state;
  set state(String state) => _state = state;
  String get postCode => _postCode;
  set postCode(String postCode) => _postCode = postCode;
  String get estimatedSalePrice => _estimatedSalePrice;
  set estimatedSalePrice(String estimatedSalePrice) =>
      _estimatedSalePrice = estimatedSalePrice;
  String get rentalPriceWeeklyPrice => _rentalPriceWeeklyPrice;
  set rentalPriceWeeklyPrice(String rentalPriceWeeklyPrice) =>
      _rentalPriceWeeklyPrice = rentalPriceWeeklyPrice;
  String get propertyInfo => _propertyInfo;
  set propertyInfo(String propertyInfo) => _propertyInfo = propertyInfo;
  String get propertyType => _propertyType;
  set propertyType(String propertyType) => _propertyType = propertyType;
  String get proTypeName => _proTypeName;
  set proTypeName(String proTypeName) => _proTypeName = proTypeName;
  String get propFeature => _propFeature;
  set propFeature(String propFeature) => _propFeature = propFeature;
  String get bedrooms => _bedrooms;
  set bedrooms(String bedrooms) => _bedrooms = bedrooms;
  String get bathrooms => _bathrooms;
  set bathrooms(String bathrooms) => _bathrooms = bathrooms;
  String get parking => _parking;
  set parking(String parking) => _parking = parking;
  String get ownerId => _ownerId;
  set ownerId(String ownerId) => _ownerId = ownerId;
  String get agentId => _agentId;
  set agentId(String agentId) => _agentId = agentId;
  String get googleLocation => _googleLocation;
  set googleLocation(String googleLocation) => _googleLocation = googleLocation;
  String get lat => _lat;
  set lat(String lat) => _lat = lat;
  String get long => _long;
  set long(String long) => _long = long;
  String get agentName => _agentName;
  set agentName(String agentName) => _agentName = agentName;
  String get ownerName => _ownerName;
  set ownerName(String ownerName) => _ownerName = ownerName;
  String get ownerEmail => _ownerEmail;
  set ownerEmail(String ownerEmail) => _ownerEmail = ownerEmail;
  String get ownerImage => _ownerImage;
  set ownerImage(String ownerImage) => _ownerImage = ownerImage;
  String get userName => _userName;
  set userName(String userName) => _userName = userName;
  String get userEmail => _userEmail;
  set userEmail(String userEmail) => _userEmail = userEmail;
  String get profileImage => _profileImage;
  set profileImage(String profileImage) => _profileImage = profileImage;
  String get userAddress => _userAddress;
  set userAddress(String userAddress) => _userAddress = userAddress;
  String get userType => _userType;
  set userType(String userType) => _userType = userType;
  String get proImg => _proImg;
  set proImg(String proImg) => _proImg = proImg;
  List<String> get feName => _feName;
  set feName(List<String> feName) => _feName = feName;
  List<ImageDetails> get propertyImage => _propertyImage;
  set propertyImage(List<ImageDetails> propertyImage) =>
      _propertyImage = propertyImage;


  String get agentImage => _agentImage;

  set agentImage(String value) {
    _agentImage = value;
  }

  OwnerPropertyDetails.fromJson(Map<String, dynamic> json) {
    _airCondition = json['airCondition'];
    _gasCooking = json['gasCooking'];
    _nbnReady = json['nbnReady'];
    _shed = json['shed'];
    _propertyId = json['propertyId'];
    _proName = json['proName'];
    _proAddress = json['proAddress'];
    _suburb = json['suburb'];
    _state = json['state'];
    _postCode = json['postCode'];
    _estimatedSalePrice = json['estimatedSalePrice'];
    _rentalPriceWeeklyPrice = json['rentalPriceWeeklyPrice'];
    _propertyInfo = json['propertyInfo'];
    _propertyType = json['propertyType'];
    _proTypeName = json['proTypeName'];
    _propFeature = json['propFeature'];
    _bedrooms = json['bedrooms'];
    _bathrooms = json['bathrooms'];
    _parking = json['parking'];
    _ownerId = json['ownerId'];
    _agentId = json['agentId'];
    _agentImage = json['agentImage'];
    _googleLocation = json['googleLocation'];
    _lat = json['lat'];
    _long = json['long'];
    _addBy = json['addby'];
    _agentName = json['agentName'];
    _ownerName = json['ownerName'];
    _ownerEmail = json['ownerEmail'];
    _ownerImage = json['ownerImage'];
    _userName = json['userName'];
    _userEmail = json['userEmail'];
    _profileImage = json['profileImage'];
    _userAddress = json['userAddress'];
    _userType = json['userType'];
    _proImg = json['proImg'];
    _feName = json['feName'].cast<String>();
    if (json['propertyImage'] != null) {
      _propertyImage = new List<ImageDetails>();
      json['propertyImage'].forEach((v) {
        _propertyImage.add(new ImageDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['airCondition'] = this._airCondition;
    data['gasCooking'] = this._gasCooking;
    data['nbnReady'] = this._nbnReady;
    data['shed'] = this._shed;
    data['propertyId'] = this._propertyId;
    data['proName'] = this._proName;
    data['proAddress'] = this._proAddress;
    data['suburb'] = this._suburb;
    data['state'] = this._state;
    data['postCode'] = this._postCode;
    data['estimatedSalePrice'] = this._estimatedSalePrice;
    data['rentalPriceWeeklyPrice'] = this._rentalPriceWeeklyPrice;
    data['propertyInfo'] = this._propertyInfo;
    data['propertyType'] = this._propertyType;
    data['proTypeName'] = this._proTypeName;
    data['propFeature'] = this._propFeature;
    data['bedrooms'] = this._bedrooms;
    data['bathrooms'] = this._bathrooms;
    data['parking'] = this._parking;
    data['ownerId'] = this._ownerId;
    data['agentId'] = this._agentId;
    data['agentImage'] = this._agentImage;
    data['googleLocation'] = this._googleLocation;
    data['lat'] = this._lat;
    data['long'] = this._long;
    data['addby'] = this._addBy;
    data['agentName'] = this._agentName;
    data['ownerName'] = this._ownerName;
    data['ownerEmail'] = this._ownerEmail;
    data['ownerImage'] = this._ownerImage;
    data['userName'] = this._userName;
    data['userEmail'] = this._userEmail;
    data['profileImage'] = this._profileImage;
    data['userAddress'] = this._userAddress;
    data['userType'] = this._userType;
    data['proImg'] = this._proImg;
    data['feName'] = this._feName;
    if (this._propertyImage != null) {
      data['propertyImage'] =
          this._propertyImage.map((v) => v.toJson()).toList();
    }
    return data;
  }
}