class ImageDetails {
  int _id;
  String _image;
  bool _isFile = false;

  bool get isFile => _isFile;

  set isFile(bool value) {
    if(value != null){
      _isFile = value;
    }else{
      _isFile=false;
    }
  }

  ImageDetails({int id, String image}) {
    this._id = id;
    this._image = image;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get image => _image;
  set image(String image) => _image = image;

  ImageDetails.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['image'] = this._image;
    return data;
  }
}