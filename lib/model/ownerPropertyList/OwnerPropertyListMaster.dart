import 'OwnerPropertyDetails.dart';

class OwnerPropertyListMaster {
  String _message;
  int _success;
  List<OwnerPropertyDetails> _result;
  int _totalCount;

  int get totalCount => _totalCount;

  set totalCount(int value) {
    _totalCount = value;
  }

  OwnerPropertyListMaster(
      {String message, int success, int totalCount, List<OwnerPropertyDetails> result}) {
    this._message = message;
    this._success = success;
    this._totalCount = totalCount;
    this._result = result;
  }




  String get message => _message;

  set message(String message) => _message = message;

  int get success => _success;

  set success(int success) => _success = success;

  List<OwnerPropertyDetails> get result => _result;

  set result(List<OwnerPropertyDetails> result) => _result = result;

  OwnerPropertyListMaster.fromJson(Map<String, dynamic> json) {
    _message = json['message'];
    _success = json['success'];
    _totalCount = json['totalCount'];
    if (json['result'] != null) {
      _result = new List<OwnerPropertyDetails>();
      json['result'].forEach((v) {
        _result.add(new OwnerPropertyDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this._message;
    data['success'] = this._success;
    data['totalCount'] = this._totalCount;
    if (this._result != null) {
      data['result'] = this._result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
