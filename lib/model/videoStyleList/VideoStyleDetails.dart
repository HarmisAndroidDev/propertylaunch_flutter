class VideoStyleListDetails {
  int _templateId;
  String _templateName;
  String _sampleVideo;
  String _description;
  bool _isSelected = false;


  bool get isSelected => _isSelected;

  set isSelected(bool value) {
    if(value != null){
      _isSelected = value;
    }else{
      _isSelected = false;
    }
  }

  VideoStyleListDetails(
      {int templateId,
      String templateName,
      String sampleVideo,
      String description,bool isSelected}) {
    this._templateId = templateId;
    this._templateName = templateName;
    this._sampleVideo = sampleVideo;
    this._description = description;
    this._isSelected = isSelected;
  }

  int get templateId => _templateId;

  set templateId(int templateId) => _templateId = templateId;

  String get templateName => _templateName;

  set templateName(String templateName) => _templateName = templateName;

  String get sampleVideo => _sampleVideo;

  set sampleVideo(String sampleVideo) => _sampleVideo = sampleVideo;

  String get description => _description;

  set description(String description) => _description = description;

  VideoStyleListDetails.fromJson(Map<String, dynamic> json) {
    _templateId = json['templateId'];
    _templateName = json['templateName'];
    _sampleVideo = json['sampleVideo'];
    _description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['templateId'] = this._templateId;
    data['templateName'] = this._templateName;
    data['sampleVideo'] = this._sampleVideo;
    data['description'] = this._description;
    return data;
  }
}
