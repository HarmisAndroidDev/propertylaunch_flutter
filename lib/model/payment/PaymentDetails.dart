class PaymentDetails {
  int _propertyid;
  String _amount;
  String _name;
  String _image;

  PaymentDetails({int propertyid, String amount, String name, String image}) {
    this._propertyid = propertyid;
    this._amount = amount;
    this._name = name;
    this._image = image;
  }

  int get propertyid => _propertyid;
  set propertyid(int propertyid) => _propertyid = propertyid;
  String get amount => _amount;
  set amount(String amount) => _amount = amount;
  String get name => _name;
  set name(String name) => _name = name;
  String get image => _image;
  set image(String image) => _image = image;

  PaymentDetails.fromJson(Map<String, dynamic> json) {
    _propertyid = json['propertyid'];
    _amount = json['amount'];
    _name = json['name'];
    _image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['propertyid'] = this._propertyid;
    data['amount'] = this._amount;
    data['name'] = this._name;
    data['image'] = this._image;
    return data;
  }
}