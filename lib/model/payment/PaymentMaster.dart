import 'package:property_launch/model/payment/PaymentDetails.dart';

class PaymentMaster {
  int _success;
  String _message;
  List<PaymentDetails> _result;

  PaymentMaster({int success, String message, List<PaymentDetails> result}) {
    this._success = success;
    this._message = message;
    this._result = result;
  }

  int get success => _success;
  set success(int success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
  List<PaymentDetails> get result => _result;
  set result(List<PaymentDetails> result) => _result = result;

  PaymentMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    if (json['result'] != null) {
      _result = new List<PaymentDetails>();
      json['result'].forEach((v) {
        _result.add(new PaymentDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    if (this._result != null) {
      data['result'] = this._result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

