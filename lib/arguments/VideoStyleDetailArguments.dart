

import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/model/videoStyleList/VideoStyleDetails.dart';

class VideoStyleDetailArguments{

  VideoStyleListDetails videoStyleListDetails;
  OwnerPropertyDetails ownerPropertyDetails;

  VideoStyleDetailArguments(this.videoStyleListDetails,this.ownerPropertyDetails);

}