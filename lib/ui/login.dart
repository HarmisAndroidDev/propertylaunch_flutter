import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:property_launch/model/MessageMaster.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:property_launch/model/login/LoginMaster.dart';
import 'package:property_launch/ui/customUi/SolidButton.dart';
import 'package:property_launch/ui/owner/SignUpPage.dart';
import 'package:property_launch/ui/owner/home.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:property_launch/utils/ImagePath.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';

import 'customUi/BlueBorderButton.dart';
import 'customUi/CTextview.dart';
import 'customUi/RoundCornerdTextField.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final mUsernameController = TextEditingController();
  final mPasswordController = TextEditingController();
  BuildContext mContext;
  String _deviceToken = "";

  @override
  void initState() {
    super.initState();
    new AppSettings().getDeviceToken().then((token) {
      _deviceToken = token.toString();
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    mUsernameController.dispose();
    mPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final image = new Container(
        margin: EdgeInsets.only(top: 80.0),
        width: double.infinity,
        height: 150,
        child: new Image.asset("images/img_logo.png"));

    final email = RoundCornerTextField(
      margin: EdgeInsets.only(top: 40.0),
      borderColor: CommonColors.edt_border_color,
      hintText: AppConstants.enter_email,
      hintTextColor: CommonColors.hint_text_color,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.enter_username;
        } else if (!CommonUtils.isvalidEmail(value)) {
          return AppConstants.enter_valid_email;
        }
      },
      controller: mUsernameController,
      inputType: TextInputType.emailAddress,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final password = RoundCornerTextField(
      margin: EdgeInsets.only(top: 20.0),
      borderColor: CommonColors.edt_border_color,
      controller: mPasswordController,
      hintText: AppConstants.enter_password,
      hintTextColor: CommonColors.hint_text_color,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.enter_password;
        }
      },
      autoFocus: false,
      isPassword: true,
      fontWeight: FontWeight.w400,
    );

    final loginButton = new BlueBorderButton(
      margin: EdgeInsets.only(top: 20.0),
      height: AppDimens.btn_height,
      buttonText: AppConstants.login,
      width: double.infinity,
      radious: 5.0,
      backgroundColor: CommonColors.blue_btn_color,
      onPressed: () {
        if (_formKey.currentState.validate()) {

          CommonUtils.isInternetConnected().then((isConnected){
            if(isConnected){
              loginApiCall();
            }else{
              CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
            }
          });
        }
      },
    );

    final forgotLabel = FlatButton(
      child: Text(AppConstants.forgotPassword,
          textAlign: TextAlign.right,
          style: TextStyle(
              fontFamily: AppConstants.FONT_FAMILY,
              fontSize: 15,
              fontWeight: FontWeight.w500,
              color: CommonColors.primaryColor)),
      onPressed: () {
        displayDialog();
      },
    );

    final tvAlready = new CTextView(
      text: AppConstants.dont_have_account,
      textSize: 16.0,
      textColor: CommonColors.color_a9a9a9,
      fontWeight: FontWeight.w700,
    );
    final tvLogin = new CTextView(
      text: " " + AppConstants.signup,
      textColor: CommonColors.primaryColor,
      textSize: 16.0,
      fontWeight: FontWeight.w700,
    );

    final tvRow = new InkWell(
        onTap: () {
          Navigator.pushReplacementNamed(mContext, SignUp.tag);
        },
        child: new Container(
            margin: EdgeInsets.only(top: 20.0),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[tvAlready, tvLogin],
            )));

    final formListView = new Container(
        padding: EdgeInsets.only(left: 30.0, right: 30.0, top: 30.0),
        child: new ListView(children: <Widget>[
          image,
          email,
          password,
          new Align(
            alignment: Alignment.centerRight,
            child: forgotLabel,
          ),
          loginButton,
          tvRow
        ]));

    final form = new Form(key: _formKey, child: formListView);
    final mainContainer = new Container(
      height: double.infinity,
      width: double.infinity,
      child: new Stack(
        children: <Widget>[
          new Align(
            alignment: Alignment.bottomCenter,
            child: form,
          )
        ],
      ),
    );

    final scaffold = Scaffold(
        backgroundColor: Colors.white,
        body: new Builder(builder: ((BuildContext context) {
          mContext = context;
          return isLoading
              ? CommonUtils.showProgressBar(mainContainer)
              : mainContainer;
        })));

    return scaffold;
  }

  loginApiCall() async {
    CommonUtils.showProgressDialog(mContext);

    final response = await http.post(ApiUrls.login, body: getLoginParams());
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      CommonUtils.hideProgressDialog(mContext);
      print("Response: " + response.body);
      LoginMaster loginMaster =
          LoginMaster.fromJson(json.decode(response.body));
      if (loginMaster != null) {
        if (loginMaster.success == 1) {
          AppSettings appSettings = new AppSettings();
          appSettings.setLoginDetails(json.encode(loginMaster.result.toJson()));
          if (loginMaster.result.userType == AppConstants.USER_TYPE_OWNER) {
            Navigator.pushReplacementNamed(context, HomePage.tag);
          } else if (loginMaster.result.userType ==
              AppConstants.USER_TYPE_AGENT) {
            Navigator.pushReplacementNamed(context, HomePage.tag);
          }
        } else {
          if (mContext == null) {
            print("mContext receiving null");
          }
          CommonUtils.showErrorMessage(mContext, loginMaster.message);
        }
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);

      CommonUtils.showErrorMessage(mContext, "" + AppConstants.oops);
      throw Exception('Exception');
    }
  }

  String getLoginParams() {
    var map = new Map<String, dynamic>();
    map[ApiParams.email] = mUsernameController.text;
    map[ApiParams.password] = mPasswordController.text;
    if (Platform.isAndroid) {
      map[ApiParams.device] = AppConstants.DEVICE_ACCESS_ANDROID;
    } else {
      map[ApiParams.device] = AppConstants.DEVICE_ACCESS_IOS;
    }
    map[ApiParams.deviceToken] = "123456";
    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }

  void displayDialog() {
    showDialog(
        context: mContext,
        builder: (BuildContext context) {
          return new MyDialog();
        });
  }

  @override
  void makePeopleLaugh() {}
}

class MyDialog extends StatefulWidget {
  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  final mRecEmailController = TextEditingController();
  final _forForm = GlobalKey<FormState>();
  bool forgotApiLoading = false;
  BuildContext mContext;

  bool isMessegeVisible = false;
  String message = "";

  @override
  Widget build(BuildContext context) {
    final tvForgot = new CTextView(
      text: AppConstants.forgotPassword,
      textSize: 16.0,
      textColor: CommonColors.black,
      fontWeight: FontWeight.w700,
      textAlign: TextAlign.center,
    );

    final recEmail = RoundCornerTextField(
      margin: EdgeInsets.only(top: 20.0),
      borderColor: CommonColors.edt_border_color,
      hintText: AppConstants.enter_email,
      hintTextColor: CommonColors.hint_text_color,
      controller: mRecEmailController,
      inputType: TextInputType.emailAddress,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final tvMessage = Visibility(
      child: new CTextView(
        margin: EdgeInsets.only(top: 10.0),
        text: message,
        textSize: 14.0,
        textColor: Colors.red,
        fontWeight: FontWeight.w500,
        textAlign: TextAlign.center,
      ),
      visible: isMessegeVisible,
    );

    final btnSubmit = new BlueBorderButton(
      margin: EdgeInsets.only(top: 20.0),
      height: AppDimens.btn_height,
      buttonText: AppConstants.submit,
      width: double.infinity,
      radious: 5.0,
      backgroundColor: CommonColors.blue_btn_color,
      onPressed: () {
        if (mRecEmailController.text.isEmpty) {
          setState(() {
            message = AppConstants.enter_email;
          });
        } else if (!CommonUtils.isvalidEmail(mRecEmailController.text)) {
          setState(() {
            message = AppConstants.enter_valid_email;
          });
        } else {
          CommonUtils.isInternetConnected().then((isConnected){
            if(isConnected){
              forgotApiCall();
            }else{
              CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
            }
          });
        }
      },
    );

    final changeColumn = new Container(
        child: new ListView(
      shrinkWrap: true,
      children: <Widget>[tvForgot, recEmail, tvMessage, btnSubmit],
    ));

    Form form = new Form(child: changeColumn, key: _forForm);

    Dialog changePasswordDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      //this right here
      child: Container(
        padding: EdgeInsets.all(20.0),
        alignment: Alignment.center,
        width: 400.0,
        height: 220,
        child: new Center(
          child: forgotApiLoading ? new CircularProgressIndicator() : form,
        ),
      ),
    );

    return changePasswordDialog;
  }

  forgotApiCall() async {
    setState(() {
      forgotApiLoading = true;
      //0
      isMessegeVisible = false;
    });
    final response =
        await http.post(ApiUrls.changePassword, body: getForgotParams());
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      setState(() {
        forgotApiLoading = false;
      });
      print("Response: " + response.body);
      MessageMaster loginMaster =
          MessageMaster.fromJson(json.decode(response.body));
      if (loginMaster != null) {
        if (loginMaster.success == 1) {
          Navigator.of(context, rootNavigator: true).pop('dialog');
          CommonUtils.showErrorMessage(mContext, loginMaster.message);
        } else {
          setState(() {
            message = loginMaster.message;
            isMessegeVisible = true;
          });
          CommonUtils.showErrorMessage(mContext, loginMaster.message);
        }
      }
    } else {
      setState(() {
        message = AppConstants.oops;
        forgotApiLoading = false;
        isMessegeVisible = true;
      });
      CommonUtils.showErrorMessage(mContext, "" + AppConstants.oops);
      throw Exception('Exception');
    }
  }

  String getForgotParams() {
    var map = new Map<String, dynamic>();
    map[ApiParams.email] = mRecEmailController.text;
    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }
}
