import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:property_launch/ui/VideoStyleInfo.dart';
import 'package:property_launch/ui/customUi/BlueBorderButton.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/ui/customUi/ProgressImageView.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CustomBoxShadow.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';

class AgentPropertyDetailPage extends StatefulWidget {
  static String tag = "agent-property-detail-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AgentPropertyDetailPageState();
  }
}

class _AgentPropertyDetailPageState extends State<AgentPropertyDetailPage> {
  BuildContext mContext;
  Set<Marker> markerSet = new Set();
  static CameraPosition _initialCamera = CameraPosition(
    target: LatLng(23.023854, 72.538471),
    zoom: 16.0,
  );

  static LatLng propertyLatLong = new LatLng(23.023854, 72.538471);
  static String markerId = "102";

  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _mapController;
  GoogleMap googleMap;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Marker propertyMarker = new Marker(
      markerId: new MarkerId(markerId),
      position: propertyLatLong,
      infoWindow: InfoWindow(title: "Driver Location", snippet: ""),
      icon: BitmapDescriptor.fromAsset("images/ic_marker.png"));

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    markers[new MarkerId(markerId)] = propertyMarker;
    //  requestPermission();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _initViews();
  }

  Widget _initViews() {
    final scaffold = new Scaffold(
      appBar: AppBar(
        title: CTextView(
          text: AppConstants.video_style,
          textSize: AppDimens.appBarTitleSize,
          textColor: Colors.white,
          backgroundColor: Colors.transparent,
        ),
        automaticallyImplyLeading: true,
        backgroundColor: CommonColors.primaryColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios,color: Colors.white,),
          onPressed: () => Navigator.pop(context, false),
          color: Colors.white,
        ),
      ),
      backgroundColor: Colors.white,
      body: new Builder(builder: (BuildContext context) {
        mContext = context;
        return buildItems();
      }),
    );

    return scaffold;
  }

  Widget buildItems() {
    final ivPropertyImage = new Align(
        child: new ProgressImageView(
          isCircule: false,
          borderRadius: 20.0,
          imageurl:
          "https://cdn.pixabay.com/photo/2017/07/08/02/16/house-2483336__340.jpg",
          height: 200,
          width: double.infinity,
        ));
    final propertyThumbnailList = new ListView.builder(
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: 5,
        itemBuilder: (context, index) {
          return propertyThumb();
        });
    final tvPropertyName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: "Astha Bunglows",
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final tvPropertyAddress = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 2,
          margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
          text:
          "Address : B/102,Acharya Nagar scoety, Gopal Palace,Ambavadi Road,Nehrunagar, Ahmedabad - 387001",
          textSize: 16.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w500,
        ));
    final tvSelectHome = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: "Select Home",
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final btnVideoAd = new BlueBorderButton(
      margin: EdgeInsets.only(top: 20.0),
      height: AppDimens.btn_height,
      buttonText: AppConstants.want_to_do,
      width: double.infinity,
      radious: 5.0,
      backgroundColor: CommonColors.blue_btn_color,
      onPressed: () {
        Navigator.pushNamed(context, VideoStyleInfo.tag);
      },
    );
    final agentIconBox = new Container(
      height: 50,
      width: 50,
      margin: EdgeInsets.only(left: 5.0),
      padding: EdgeInsets.all(10.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5)),
      child: new Center(
        child: Image.asset(
          "images/ic_agent.png",
          color: CommonColors.primaryColor,
        ),
      ),
    );
    final lblAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: "Agent name",
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final tvAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 10.0),
          maxLines: 1,
          textAlign: TextAlign.start,
          text: "Rohit kumar Chauhan",
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final agentDetails = new Container(
        margin: EdgeInsets.only(left: 10.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[lblAgentName, tvAgentName],
        ));
    final layoutAgent = new Container(
      height: 60,
      margin: const EdgeInsets.only(top: 20.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[agentIconBox, agentDetails],
      ),
    );
    final lblPropertyDetails = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_detail,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final tvPropertyDetail = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 20.0, bottom: 5.0),
          text:
          "Address : B/102,Acharya Nagar scoety, Gopal Palace,Ambavadi Road,Nehrunagar, Ahmedabad - 387001",
          textSize: 16.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final lblPropertyFeatures = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_features,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final featuresList = new ListView.builder(
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: 5,
        itemBuilder: (context, index) {
          return propertyFeatures();
        });
    final divider = new Container(
      width: double.infinity,
      color: CommonColors.text_color,
      height: 1,
      margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
    );
    googleMap = GoogleMap(
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
        ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
                () => VerticalDragGestureRecognizer())),
      mapType: MapType.normal,
      zoomGesturesEnabled: true,
      myLocationButtonEnabled: true,
      myLocationEnabled: false,
      markers: Set<Marker>.of(markers.values),
      minMaxZoomPreference: MinMaxZoomPreference.unbounded,
      initialCameraPosition: _initialCamera,
      onMapCreated: (GoogleMapController googleMapController) {
        _mapController = googleMapController;
        _controller.complete(googleMapController);
      },
    );
    final mapView = new Container(
        height: 200,
        width: double.infinity,
        margin: EdgeInsets.only(top: 20.0),
        child: new ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            child: googleMap));
    final detailListview = ListView(
      shrinkWrap: true,
      padding:
      EdgeInsets.only(top: 10.0, bottom: 10.0, right: 15.0, left: 15.0),
      children: <Widget>[
        ivPropertyImage,
        new Container(height: 80, child: propertyThumbnailList),
        tvPropertyName,
        tvPropertyAddress,
        tvSelectHome,
        btnVideoAd,
        layoutAgent,
        lblPropertyDetails,
        tvPropertyDetail,
        divider,
        lblPropertyFeatures,
        featuresList,
        mapView
      ],
    );
    return detailListview;
  }

  Future requestPermission() async {
    Map<PermissionGroup, PermissionStatus> permissions =
    await PermissionHandler()
        .requestPermissions([PermissionGroup.location]);
  }

  Future<int> checkPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    return permission.value;
  }

  Widget propertyThumb() {
    final ivPropertyImage = new Align(
        alignment: Alignment.centerLeft,
        child: new ProgressImageView(
          margin: EdgeInsets.only(right: 10.0),
          isCircule: false,
          borderRadius: 10.0,
          imageurl:
          "https://cdn.pixabay.com/photo/2017/07/08/02/16/house-2483336__340.jpg",
          height: 55,
          width: 80,
        ));

    return ivPropertyImage;
  }

  Widget propertyFeatures() {
    final tvFeatureName = new Container(
        width: 150,
        margin: EdgeInsets.only(top: 5.0),
        child: new CTextView(
          text: "gas cooking",
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w600,
        ));

    final tvFeatureDetail = new Container(
        width: 150,
        margin: EdgeInsets.only(top: 5.0),
        child: new CTextView(
          text: "Electric gas cooking",
          textSize: 15.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w600,
        ));

    final detailsRow = new Container(
        width: double.infinity,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[tvFeatureName, tvFeatureDetail],
        ));
    return detailsRow;
  }
}
