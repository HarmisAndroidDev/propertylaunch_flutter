import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:property_launch/model/AddPropertyM.dart';
import 'package:property_launch/model/PropertyFeaturesM.dart';
import 'package:property_launch/model/PropertyTypeM.dart';
import 'package:property_launch/model/agent/AgentDetails.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:property_launch/model/login/LoginMaster.dart';
import 'package:property_launch/model/owner/OwnerDetails.dart';
import 'package:property_launch/model/ownerPropertyList/AddpropertyMaster.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyListMaster.dart';
import 'package:property_launch/ui/AgentList.dart';
import 'package:property_launch/ui/OwnerList.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/ui/customUi/CustomImage.dart';
import 'package:property_launch/ui/customUi/ProgressImageView.dart';
import 'package:property_launch/ui/customUi/RoundCornerdTextField.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:property_launch/utils/CustomBoxShadow.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:flutter_google_places/flutter_google_places.dart';

import 'package:http/http.dart' as http;
import 'package:property_launch/utils/ImagePath.dart';

//var kGoogleApiKey = AppConstants.googleApiKey;

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: AppConstants.googleApiKey);

class AgentAddPropertyPage extends StatefulWidget {
  static String tag = "agent-add-property-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AgentAddPropertyPageState();
  }
}

class _AgentAddPropertyPageState extends State<AgentAddPropertyPage> {
  List<PropertyTypeM> propertyTypeList = new List();
  List<PropertyFeaturesM> propertyFeatureList = new List();
  final mPropertyNameController = TextEditingController();
  final mPropertyAddressController = TextEditingController();
  final mPrpertyPriceController = TextEditingController();
  final mPrpertyInfoController = TextEditingController();
  final mPrpertyStateController = TextEditingController();
  final mPrpertySuberbController = TextEditingController();
  final mPrpertyPostCodeController = TextEditingController();
  final mPrpertyEstPriceController = TextEditingController();
  final mPrpertyEstRentalPriceController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  bool isMapLoaded = false;
  Set<Marker> markerSet = new Set();
  BuildContext mContext;
  static CameraPosition _initialCamera = CameraPosition(
    target: LatLng(23.023854, 72.538471),
    zoom: 16.0,
  );

  static LatLng propertyLatLong = new LatLng(23.023854, 72.538471);
  static String markerId = "102";
  List<String> thumbList = new List();
  String propertyLat = "0.0", propertyLong = "0.0";

  Completer<GoogleMapController> _gController = Completer();
  GoogleMapController _mapController;
  GoogleMap googleMap;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Marker propertyMarker = new Marker(
      markerId: new MarkerId(markerId),
      position: propertyLatLong,
      infoWindow: InfoWindow(title: "Driver Location", snippet: ""),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));

  int selectedPropertyTypeIndex = 0;
  LoginDetails loginDetails;
  OwnerDetails _ownerDetails = new OwnerDetails();

  void addPropertyTypeList() {
    propertyTypeList.add(new PropertyTypeM("House", true));
    propertyTypeList.add(new PropertyTypeM("Townhouse", false));
    propertyTypeList.add(new PropertyTypeM("Commercial", false));
    propertyTypeList.add(new PropertyTypeM("Apartment", false));
    propertyTypeList.add(new PropertyTypeM("Land", false));
  }

  void addPropertyFeaures() {
    propertyFeatureList.add(new PropertyFeaturesM("Air Conditioning", true));
    propertyFeatureList.add(new PropertyFeaturesM("Gas cooking", false));
    propertyFeatureList.add(new PropertyFeaturesM("Nbn Ready", false));
    propertyFeatureList.add(new PropertyFeaturesM("Shed", false));
    propertyFeatureList.add(new PropertyFeaturesM("Granny Flat", false));
    propertyFeatureList.add(new PropertyFeaturesM("2nd Living Area", false));
    propertyFeatureList.add(new PropertyFeaturesM("Pool", false));
    propertyFeatureList.add(new PropertyFeaturesM("Electric cooking", false));
    propertyFeatureList.add(new PropertyFeaturesM("Entertaining area ", false));
    propertyFeatureList.add(new PropertyFeaturesM("Security system", false));
    propertyFeatureList.add(new PropertyFeaturesM("Study", false));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    AppSettings appSetings = new AppSettings();
    appSetings.getLoginDetails().then((onValue) {
      loginDetails = LoginDetails.fromJson(json.decode(onValue));
    });

    markers[new MarkerId(markerId)] = propertyMarker;
    addPropertyFeaures();
    addPropertyTypeList();
    requestPermission();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print("build method called");

    isMapLoaded = true;

    return initViews();
  }

  void _selectImageOption(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.camera),
                    title: new Text(AppConstants.text_camera),
                    onTap: () {
                      _getImageFromDevice(false);
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.attach_file),
                  title: new Text(AppConstants.text_gallery),
                  onTap: () {
                    _getImageFromDevice(true);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  Future _getImageFromDevice(bool isGallery) async {
    var galleryFile = await ImagePicker.pickImage(
        source: isGallery ? ImageSource.gallery : ImageSource.camera);

    if (galleryFile != null) {
      setState(() {
        thumbList.add(galleryFile.path);
        print("Image path: " + galleryFile.path);
      });
    } else {
      CommonUtils.showErrorMessage(mContext, "unable to get image path");
    }
  }

  Widget initViews() {
    final tvAddImage = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 2.0),
          textAlign: TextAlign.left,
          text: AppConstants.add_image,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w500,
        ));

    final footerAddImage = new Align(
        alignment: Alignment.centerLeft,
        child: new InkWell(
          onTap: () {
            _selectImageOption(mContext);
          },
          child: new Container(
            width: 80,
            height: 55,
            padding: EdgeInsets.all(15.0),
            decoration: new BoxDecoration(
                boxShadow: [
                  CustomBoxShadow(
                    color: Colors.white,

                    blurRadius: 1.0, // has the effect of softening the shadow
                    // has the effect of extending the shadow
                    blurStyle: BlurStyle.outer,

                    offset: Offset(
                      0.0, // horizontal, move right 10
                      0.0, // vertical, move down 10
                    ),
                  )
                ],
                border: new Border.all(color: CommonColors.primaryColor),
                borderRadius: BorderRadius.circular(10)),
            child: new Center(
              child: Image.asset(
                LocalImages.ic_add_photo,
                color: CommonColors.primaryColor,
              ),
            ),
          ),
        ));

    final propertyThumbnailList = new Container(
        height: 60,
        margin: EdgeInsets.only(top: 10.0),
        child: new ListView.builder(
            physics: AlwaysScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: thumbList.length + 1,
            itemBuilder: (context, index) {
              if (index == thumbList.length) {
                return footerAddImage;
              } else {
                return propertyThumb(index);
              }
            }));
    final tvPropertyAddress = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_address,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w500,
        ));

    final tvPropertyName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_name,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w500,
        ));

    final edPropertyName = RoundCornerTextField(
      margin: EdgeInsets.only(top: 10.0),
      borderColor: CommonColors.edt_border_color,
      hintText: AppConstants.property_name,
      hintTextColor: CommonColors.hint_text_color,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.enter_property_name;
        }
      },
      controller: mPropertyNameController,
      inputType: TextInputType.text,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final edPropertyAddress = RoundCornerTextField(
      margin: EdgeInsets.only(top: 10.0),
      borderColor: CommonColors.edt_border_color,
      hintText: AppConstants.property_address,
      hintTextColor: CommonColors.hint_text_color,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.enter_address;
        }
      },
      controller: mPropertyAddressController,
      inputType: TextInputType.text,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final tvSuberb = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: AppConstants.suberb,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w500,
        ));

    final edPropertySuberb = RoundCornerTextField(
      margin: EdgeInsets.only(top: 10.0),
      borderColor: CommonColors.edt_border_color,
      hintText: AppConstants.suberb,
      hintTextColor: CommonColors.hint_text_color,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.enter_suberb;
        }
      },
      controller: mPrpertySuberbController,
      inputType: TextInputType.text,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final tvState = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: AppConstants.state,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w500,
        ));

    final edState = RoundCornerTextField(
      margin: EdgeInsets.only(top: 10.0),
      borderColor: CommonColors.edt_border_color,
      hintText: AppConstants.state,
      hintTextColor: CommonColors.hint_text_color,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.enter_state;
        }
      },
      controller: mPrpertyStateController,
      inputType: TextInputType.text,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final tvPostcode = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: AppConstants.postcode,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w500,
        ));

    final edPropertyPostcode = RoundCornerTextField(
      margin: EdgeInsets.only(top: 10.0),
      borderColor: CommonColors.edt_border_color,
      hintTextColor: CommonColors.hint_text_color,
      hintText: AppConstants.postcode,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.please_enter_postcode;
        }
      },
      controller: mPrpertyPostCodeController,
      inputType: TextInputType.text,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final tvSalesPrice = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: AppConstants.estimated_slaes_price,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w500,
        ));

    final edPropertySalesPrice = RoundCornerTextField(
      margin: EdgeInsets.only(top: 10.0),
      borderColor: CommonColors.edt_border_color,
      hintTextColor: CommonColors.hint_text_color,
      hintText: AppConstants.price_hint,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.please_enter_sales_price;
        }
      },
      controller: mPrpertyPriceController,
      inputType: TextInputType.number,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final tvRentalPrice = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: AppConstants.rental_estaimated,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w500,
        ));

    final edTRental = RoundCornerTextField(
      margin: EdgeInsets.only(top: 10.0),
      borderColor: CommonColors.edt_border_color,
      hintTextColor: CommonColors.hint_text_color,
      hintText: AppConstants.price_hint,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.enter_rental_price;
        }
      },
      controller: mPrpertyEstRentalPriceController,
      inputType: TextInputType.number,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final tvPropertyInfo = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_info,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w500,
        ));

    final edPropertyInfo = new Container(
        height: 100,
        child: RoundCornerTextField(
          margin: EdgeInsets.only(top: 10.0),
          borderColor: CommonColors.edt_border_color,
          hintText: AppConstants.property_info,
          hintTextColor: CommonColors.hint_text_color,
          maxLines: 500,
          validator: (value) {
            if (value.isEmpty) {
              return AppConstants.enter_property_info;
            }
          },
          controller: mPrpertyInfoController,
          inputType: TextInputType.text,
          autoFocus: false,
          fontWeight: FontWeight.w400,
        ));

    final tvSelect = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.select,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));

    final itemGridDelegate = new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 5.0,
        mainAxisSpacing: 5.0,
        crossAxisCount: 2,
        childAspectRatio: 5.0);

    final propertyTypeGridview = new Container(
      child: new Container(
        padding: EdgeInsets.only(top: 20.0),
        color: Colors.white,
        child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          gridDelegate: itemGridDelegate,
          shrinkWrap: true,
          itemCount: propertyTypeList.length,
          itemBuilder: (BuildContext context, int index) {
            return new GestureDetector(
              child: _buildPropertyTypeItems(index, context),
              onTap: () {
                setState(() {
                  propertyTypeList[index].isSelected = true;
                  propertyTypeList[selectedPropertyTypeIndex].isSelected =
                      false;
                  selectedPropertyTypeIndex = index;
                });
              },
            );
          },
        ),
      ),
    );

    final tvPropertyFeatures = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_features,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final tvActionSave = new Align(
        alignment: Alignment.centerRight,
        child: new InkWell(
          child: new CTextView(
            margin: EdgeInsets.only(right: 20.0),
            backgroundColor: Colors.transparent,
            maxLines: 1,
            textAlign: TextAlign.center,
            text: AppConstants.done,
            textSize: 16.0,
            textColor: Colors.white,
            fontWeight: FontWeight.w700,
          ),
          onTap: () {
            if (checkValidation()) {
              CommonUtils.isInternetConnected().then((isConnected){
                if(isConnected){
                  addPropertyApi();
                }else{
                  CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
                }
              });
            }
          },
        ));

    final faeturesGridItems = new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 5.0,
        mainAxisSpacing: 5.0,
        crossAxisCount: 2,
        childAspectRatio: 5.0);

    final propertyFeaturesGrid = new Container(
      child: new Container(
        padding: EdgeInsets.only(top: 20.0),
        color: Colors.white,
        child: GridView.builder(
          gridDelegate: faeturesGridItems,
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: propertyFeatureList.length,
          itemBuilder: (BuildContext context, int index) {
            return new GestureDetector(
                onTap: () {
                  setState(() {
                    propertyFeatureList[index].isSelected =
                        !propertyFeatureList[index].isSelected;
                  });
                },
                child: _buildFeaturesItems(index, context));
          },
        ),
      ),
    );

    final lblSelectAgent = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: AppConstants.select_owner,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final agentIconBox = new Container(
      height: 50,
      width: 50,
      margin: EdgeInsets.only(left: 5.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5)),
      child: new Center(
        child: ProgressImageView(
          isCircule: false,
          height: 50,
          width: 50,
          borderRadius: 5.0,
          imageurl: _ownerDetails.profileImage,
        ),
      ),
    );
    final lblAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: AppConstants.owner_name,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final tvAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 10.0),
          maxLines: 1,
          textAlign: TextAlign.start,
          text: _ownerDetails.userName == null ? "" : _ownerDetails.userName,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final agentDetails = new Container(
        margin: EdgeInsets.only(left: 10.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[lblAgentName, tvAgentName],
        ));
    final layoutAgent = new Container(
      height: 60,
      margin: const EdgeInsets.only(top: 20.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5.0)),
      child: new InkWell(
          onTap: () {
            redirectToOwnerList();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[agentIconBox, agentDetails],
          )),
    );

    googleMap = GoogleMap(
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
        ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer())),
      mapType: MapType.normal,
      onTap: (LatLng latLng) {
        OpenPlaceAutoComplete();
      },
      zoomGesturesEnabled: true,
      markers: Set<Marker>.of(markers.values),
      minMaxZoomPreference: MinMaxZoomPreference.unbounded,
      initialCameraPosition: _initialCamera,
      onMapCreated: (GoogleMapController googleMapController) {
        print("Map Created");
        _mapController = googleMapController;
        _gController.complete(googleMapController);
      },
    );

    final mapView = new Container(
        height: 200,
        width: double.infinity,
        margin: EdgeInsets.only(top: 20.0),
        child: new ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            child: googleMap));

    final mainListview = new ListView(
      physics: AlwaysScrollableScrollPhysics(),
      padding: EdgeInsets.all(15.0),
      children: <Widget>[
        tvAddImage,
        propertyThumbnailList,
        tvPropertyName,
        edPropertyName,
        tvPropertyAddress,
        edPropertyAddress,
        tvSuberb,
        edPropertySuberb,
        tvState,
        edState,
        tvPostcode,
        edPropertyPostcode,
        tvSalesPrice,
        edPropertySalesPrice,
        tvRentalPrice,
        edTRental,
        tvPropertyInfo,
        edPropertyInfo,
        tvSelect,
        propertyTypeGridview,
        tvPropertyFeatures,
        propertyFeaturesGrid,
        lblSelectAgent,
        layoutAgent,
        mapView
      ],
    );
    final form = new Form(key: _formKey, child: mainListview);

    final scaffold = new Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: CTextView(
          text: AppConstants.text_add_property,
          textSize: AppDimens.appBarTitleSize,
          backgroundColor: Colors.transparent,
          textColor: Colors.white,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(mContext, false),
          color: Colors.white,
        ),
        backgroundColor: CommonColors.primaryColor,
        automaticallyImplyLeading: true,
        actions: <Widget>[tvActionSave],
      ),
      body: Builder(builder: (BuildContext context) {
        mContext = context;
        return form;
      }),
    );

    return scaffold;
  }

  Future requestPermission() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler()
            .requestPermissions([PermissionGroup.location]);
  }

  Future<int> checkPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    return permission.value;
  }

  Widget propertyThumb(int index) {
    final ivPropertyImage = new Align(
        alignment: Alignment.centerLeft,
        child: new CustomImage(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          imageUrl: thumbList[index],
          height: 55,
          width: 80,
        ));

    final imageContainer = new Container(
      height: 65,
      width: 85,
      child: new Center(child: ivPropertyImage),
    );

    final btnCross = new InkWell(
      child: new Container(
          height: 15,
          width: 15,
          child: new Container(
            height: 15,
            width: 15,
            padding: EdgeInsets.all(4.0),
            decoration: new BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(30.0))),
            child: Image.asset(
              "images/ic_delete.png",
              color: CommonColors.primaryColor,
            ),
          )),
      onTap: () {
        setState(() {
          thumbList.removeAt(index);
        });
      },
    );

    final stackImage = new Container(
        margin: EdgeInsets.only(right: 5.0),
        child: new Stack(children: <Widget>[
          imageContainer,
          Positioned(top: 0, right: 0, child: btnCross),
        ]));

    return stackImage;
  }

  Widget _buildPropertyTypeItems(int index, BuildContext context) {
    final icCheckbox = new Container(
      height: 20,
      width: 20,
      child: Icon(
        propertyTypeList[index].isSelected
            ? Icons.check_box
            : Icons.check_box_outline_blank,
        color: CommonColors.primaryColor,
      ),
    );

    final tvPropertyType = new Container(
        width: 120,
        margin: EdgeInsets.only(left: 10.0),
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: propertyTypeList[index].propertyName,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    final rowContainer = new Container(
      width: 150,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[icCheckbox, tvPropertyType],
      ),
    );

    return rowContainer;
  }

  Widget _buildFeaturesItems(int index, BuildContext context) {
    final icCheckbox = new Container(
      height: 20,
      width: 20,
      child: Icon(
        propertyFeatureList[index].isSelected
            ? Icons.check_box
            : Icons.check_box_outline_blank,
        color: CommonColors.primaryColor,
      ),
    );

    final tvPropertyFeature = new Container(
        width: 120,
        margin: EdgeInsets.only(left: 10.0),
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: propertyFeatureList[index].featureName,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    final rowContainer = new Container(
      width: 150,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[icCheckbox, tvPropertyFeature],
      ),
    );

    return rowContainer;
  }

  Future OpenPlaceAutoComplete() async {
    /*enable language en and country us to load country wise*/
    Prediction p = await PlacesAutocomplete.show(
      context: mContext,
      apiKey: AppConstants.googleApiKey,
      mode: Mode.fullscreen,
      // Mode.fullscreen
      /*language: "en",
        components: [new Component(Component.country, "US")]*/
    );
    displayPrediction(p);
  }

  Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;
      print(lat.toString() + "//" + lng.toString());
      propertyLong = lng.toString();
      propertyLat = lat.toString();

      setState(() {
        propertyLatLong = new LatLng(lat, lng);
        _initialCamera = new CameraPosition(target: propertyLatLong, zoom: 16);
        propertyMarker = new Marker(
            markerId: new MarkerId(markerId),
            position: propertyLatLong,
            infoWindow:
                InfoWindow(title: detail.result.formattedAddress, snippet: ""),
            icon:
                BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));
        markers[propertyMarker.markerId] = propertyMarker;
        _mapController
            .animateCamera(CameraUpdate.newCameraPosition(_initialCamera));
      });
    }
  }

  addPropertyApi() async {
    CommonUtils.showProgressDialog(mContext);
    final request = await http.MultipartRequest(
        AppConstants.METHOD_POST, Uri.parse(ApiUrls.addProperty));
    request.fields[ApiParams.json_content] = getJsonParams();

    /*add images*/
    for (String imagePath in thumbList) {
      try {
        File imageFile = new File(imagePath);
        var stream =
            new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
        var length = await imageFile.length();
        var multipartFile = new http.MultipartFile(
            ApiParams.images, stream, length,
            filename: basename(imageFile.path));
        request.files.add(multipartFile);
        print("added to field");
      } catch (e) {
        print("error: " + e);
      }
    }
    /* add images to end*/

    request.send().then((response) {
      print("URL: " + response.request.url.toString());
      try {
        CommonUtils.hideProgressDialog(mContext);
        response.stream.transform(utf8.decoder).listen((value) {
          print("Response: " + value);
          if (response.statusCode == 200) {
            AddPropertyMaster addPropertyMaster =
                AddPropertyMaster.fromJson(json.decode(value));
            if (addPropertyMaster.success == 1) {
              CommonUtils.showErrorMessage(mContext, addPropertyMaster.message);
              CommonUtils.isPropertyAdded = true;
              CommonUtils.ownerPropertyDetails = addPropertyMaster.result;
              CommonUtils.showErrorMessage(
                  mContext, AppConstants.property_added);
              backToDetail(addPropertyMaster.result, mContext);
            } else {
              CommonUtils.showErrorMessage(mContext, addPropertyMaster.message);
            }
          }
        });
      } catch (e) {
        CommonUtils.hideProgressDialog(mContext);
        CommonUtils.showErrorMessage(mContext, e);
        print("Error: " + e);
      }
    });
  }

  String getJsonParams() {
    AddPropertyP addPropertyM = new AddPropertyP();
    addPropertyM.proName = mPropertyNameController.text;
    addPropertyM.proAddress = mPropertyAddressController.text;
    addPropertyM.suburb = mPrpertySuberbController.text;
    addPropertyM.state = mPrpertyStateController.text;
    addPropertyM.postCode = mPrpertyPostCodeController.text;
    addPropertyM.estimatedSalePrice = mPrpertyPriceController.text;
    addPropertyM.rentalPriceWeeklyPrice = mPrpertyEstRentalPriceController.text;
    addPropertyM.propertyInfo = mPrpertyInfoController.text;
    addPropertyM.propertyType = (selectedPropertyTypeIndex + 1).toString();
    addPropertyM.airCondition =
        propertyFeatureList[0].isSelected ? "true" : "false";
    addPropertyM.gasCooking =
        propertyFeatureList[1].isSelected ? "true" : "false";
    addPropertyM.nbnReady =
        propertyFeatureList[2].isSelected ? "true" : "false";
    addPropertyM.shed = propertyFeatureList[3].isSelected ? "true" : "false";
    addPropertyM.grannyFlat =
        propertyFeatureList[4].isSelected ? "true" : "false";
    addPropertyM.secondLivingArea =
        propertyFeatureList[5].isSelected ? "true" : "false";
    addPropertyM.pool = propertyFeatureList[6].isSelected ? "true" : "false";
    addPropertyM.eletecticCooking =
        propertyFeatureList[7].isSelected ? "true" : "false";
    addPropertyM.entertainingArea =
        propertyFeatureList[8].isSelected ? "true" : "false";
    addPropertyM.securitySystem =
        propertyFeatureList[9].isSelected ? "true" : "false";
    addPropertyM.study = propertyFeatureList[10].isSelected ? "true" : "false";
    addPropertyM.userId = loginDetails.userId;
    addPropertyM.user1Id = _ownerDetails.userId.toString();
    addPropertyM.userType = AppConstants.userTypeAgent;
    addPropertyM.lat = propertyLat;
    addPropertyM.long = propertyLong;
    String jsonStr = jsonEncode(addPropertyM);

    print("params == =" + jsonStr);

    return jsonStr;
  }

  bool checkValidation() {
    if (thumbList.length == 0) {
      CommonUtils.showErrorMessage(
          mContext, AppConstants.property_image_required);
      return false;
    } else if (!_formKey.currentState.validate()) {
      return false;
    } else if (_ownerDetails.userId == null) {
      CommonUtils.showErrorMessage(mContext, AppConstants.select_agent);

      return false;
    } else if (propertyLat == "0.0" || propertyLong == "0.0") {
      CommonUtils.showErrorMessage(
          mContext, AppConstants.property_location_required);
      return false;
    }
    return true;
  }

  redirectToOwnerList() {
    Navigator.pushNamed(mContext, OwnerList.tag).then((value) {
      if (value != null) {
        var result = value as Map;
        if (result.containsKey("agentDetails")) {
          setState(() {
            _ownerDetails = result['agentDetails'];
          });
        }
      }
    });
  }

  Future redirectToAgentList(BuildContext context) async {
    var results =
        await Navigator.of(context).push(new MaterialPageRoute<dynamic>(
      builder: (BuildContext context) {
        return new OwnerList();
      },
    ));

    if (results != null && results.containsKey('agentDetails')) {
      setState(() {
        _ownerDetails = results['agentDetails'];
      });
      print("agentDetails" + _ownerDetails.userName);
    }
  }

  void backToDetail(
      OwnerPropertyDetails ownerPropertyDetails, BuildContext context) {
    Navigator.of(context).pop(
        {'propertyDetails': ownerPropertyDetails, AppConstants.isAdded: false});
  }
}
