import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:property_launch/arguments/PaymentHistoryArguments.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:property_launch/model/payment/PaymentDetails.dart';
import 'package:property_launch/model/payment/PaymentMaster.dart';
import 'package:property_launch/ui/HistoryPropertyDetailPage.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';

import 'customUi/CTextview.dart';
import 'customUi/ProgressImageView.dart';
import 'package:http/http.dart' as http;

class OrderHistory extends StatefulWidget {
  static String tag = "order-history-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OrderHistoryState();
  }
}

class _OrderHistoryState extends State<OrderHistory> {
  bool isLoading = false, isListAvailable = true;
  BuildContext mContext;
  List<PaymentDetails> paymentHistoryList = new List();
  String mErrorMessage = "";
  LoginDetails loginDetails;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppSettings appSettings = new AppSettings();
    appSettings.getLoginDetails().then((onValue) {
      setState(() {
        loginDetails = LoginDetails.fromJson(json.decode(onValue));
        CommonUtils.isInternetConnected().then((isConnected){
          if(isConnected){
            getPropertyLisApiCall();
          }else{
            CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
          }
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return _initViews(context);
  }

  Widget _initViews(BuildContext context) {
    final listVIew = new ListView.builder(
      padding: EdgeInsets.all(15.0),
      shrinkWrap: true,
      itemCount: paymentHistoryList.length,
      itemBuilder: (BuildContext context, int index) {
        return new GestureDetector(
          child: buildItems(index),
          onTap: () {
            redirectToDetailPage(paymentHistoryList[index]);
          //  Navigator.pushNamed(context, HistoryPropertyDetailPage.tag);
          },
        );
      },
    );

    final noDataContainer = new Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child: new Center(
        child: new CTextView(
          text: mErrorMessage,
          textAlign: TextAlign.center,
          fontWeight: FontWeight.w500,
          fontFamily: AppConstants.FONT_FAMILY,
          textColor: Colors.black,
          textSize: 16,
        ),
      ),
    );

    final scaffold = new Scaffold(
      body: new Builder(builder: (BuildContext context) {
        mContext = context;
        return isListAvailable ? listVIew : noDataContainer;
      }),
    );

    return scaffold;
  }

  Widget buildItems(int index) {
    final ivPropertyImage = new Align(
        alignment: Alignment.centerLeft,
        child: new ProgressImageView(
          isCircule: false,
          borderRadius: 10.0,
          imageurl: paymentHistoryList[index].image,
          height: 110,
          width: 110,
        ));

    final tvPropertyName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 2.0),
          textAlign: TextAlign.left,
          text: paymentHistoryList[index].name,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));

    final tvPropertyAddress = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 2,
          margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
          text: "Ad Video style",
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    final tvAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: "\$" + paymentHistoryList[index].amount,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    final propertyColumn = new Container(
        height: 110,
        margin: EdgeInsets.only(left: 85.0),
        child: new Card(
            child: new Container(
                margin: EdgeInsets.only(left: 10.0),
                padding: EdgeInsets.all(5.0),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    tvPropertyName,
                    tvPropertyAddress,
                    tvAgentName
                  ],
                ))));

    final propertyStack = new Container(
        height: 120,
        child: new Align(
            alignment: Alignment.center,
            child: new Stack(
              children: <Widget>[
                ivPropertyImage,
                new Align(
                  child: propertyColumn,
                  alignment: Alignment.center,
                ),
              ],
            )));
    return propertyStack;
  }

  getPropertyLisApiCall() async {
    setState(() {
      isListAvailable = true;
    });
    CommonUtils.showProgressDialog(mContext);
    final response =
        await http.post(ApiUrls.getPaymentHistory, body: getPropertyLisParams());
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      CommonUtils.hideProgressDialog(mContext);
      print("Response: " + response.body);
      PaymentMaster master = PaymentMaster.fromJson(json.decode(response.body));
      if (master != null) {
        if (master.success == 1) {
          CommonUtils.isPropertyAdded = false;
          if (master.result != null && master.result.length > 0) {
            setState(() {
              paymentHistoryList.addAll(master.result);
            });
          } else {
            print("_showEmptyLayout");
            _showEmptyLayout();
          }
        } else {
          if (!CommonUtils.isEmpty(master.message)) {
            setState(() {
              mErrorMessage = master.message;
            });
          }
          _showEmptyLayout();
        }
      } else {
        CommonUtils.hideProgressDialog(mContext);
        _showEmptyLayout();
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);
      _showEmptyLayout();
      throw Exception('Exception');
    }
  }

  void _showEmptyLayout() {
    setState(() {
      isListAvailable = false;
    });
  }

  String getPropertyLisParams() {
    var map = new Map<String, dynamic>();
    map[ApiParams.ownerId] = "30";
    map[ApiParams.propertyId] = "272";

    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }

  redirectToDetailPage(PaymentDetails paymentDetails) {
    Navigator.pushNamed(mContext, HistoryPropertyDetailPage.tag,
        arguments: PaymentHistoryArguments(paymentDetails));
  }

}
