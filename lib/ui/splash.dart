import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'dart:async';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:property_launch/utils/ImagePath.dart';

import 'PropertList.dart';
import 'login.dart';
import 'owner/SignUpPage.dart';
import 'owner/home.dart';



class SplashPage extends StatefulWidget {
  static String tag = 'splash-page';
  final String title;
  static const platform = const MethodChannel('com.harmis.cloudsys.channel');

  SplashPage({Key key, this.title}) : super(key: key) {
    platform.setMethodCallHandler(_handleMethod);
  }

  Future<dynamic> _handleMethod(MethodCall call) async {
    switch (call.method) {
      case "notification":
        print("Notification from android " + call.arguments);
        return new Future.value("");
    }
  }

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  AppSettings appSettings = new AppSettings();

  void navigationToNextPage() {
    /*get statusbar */
    /*SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.white
    ));*/
    appSettings.getLoginDetails().then((value) {
      if (CommonUtils.isEmpty(value)) {
        Navigator.pushReplacementNamed(context, LoginPage.tag);
      } else {
        LoginDetails loginDetails = LoginDetails.fromJson(json.decode(value));
        if (loginDetails != null) {
          if (loginDetails.userType == AppConstants.USER_TYPE_OWNER) {
            Navigator.pushReplacementNamed(context, HomePage.tag);
          } else if (loginDetails.userType == AppConstants.USER_TYPE_AGENT) {
            Navigator.pushReplacementNamed(context, HomePage.tag);
          }
        }
      }
    });
  }

  startSplashScreenTimer() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationToNextPage);
  }

  @override
  void initState() {
    super.initState();
    print("Splash Initializing");
    startSplash();
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(value)));
  }

  void startSplash() {
    CommonUtils.isInternetConnected().then((IsConnected) {
      if (IsConnected) {
        startSplashScreenTimer();
      } else {
        showInSnackBar("No internet connected. Please try to reconnect");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final String args = ModalRoute.of(context).settings.arguments;
    print(args.toString());
    CommonUtils.setPotraitModeOnly();
    //CommonUtils.hideStatusBar();
    return new Scaffold(
      key: _scaffoldKey,
      body: new Builder(builder: (BuildContext context){
        return new Center(
          child: Container(
              color: Colors.white,
              height: double.infinity,
              width: double.infinity,
              child: new Image.asset(LocalImages.ic_logo)),
        );

      }),
    );
  }
}
