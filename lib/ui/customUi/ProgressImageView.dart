import 'dart:io';

import 'package:flutter/material.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/ImagePath.dart';

class ProgressImageView extends StatelessWidget {
  String imageurl;
  final double height;
  final double width;
  final EdgeInsets margin;
  final double borderWidth;
  final Color borderColor;
  final BoxFit scaleType;
  double borderRadius;
  bool isCircule;
  final String imagePath;
  final String placeHolderImage;

  ProgressImageView(
      {this.imageurl,
      this.height,
      this.width,
      this.margin,
      this.borderWidth,
      this.borderColor,
      this.scaleType,
      this.isCircule,
      this.borderRadius,
      this.imagePath,this.placeHolderImage});

  @override
  Widget build(BuildContext context) {
    if (imageurl == null) {
      imageurl = ApiUrls.dummyImage;
    }

    if (isCircule == null) {
      isCircule = false;
    }
    if (isCircule) {
      borderRadius = 60;
    } else {
      borderRadius = borderRadius;
    }

    final fileImage = FileImage(new File(imageurl == null ? "" : imageurl));

    final fadeInImage = new FadeInImage.assetNetwork(
        fit: scaleType == null ? BoxFit.cover : scaleType,
        placeholder: placeHolderImage == null ? LocalImages.img_default_image : placeHolderImage,
        image: imageurl == null ? ApiUrls.dummyImage : imageurl);

    final imageContainer = new Container(
      child: new ClipRRect(
        child: imageurl.startsWith("http") ? fadeInImage : fileImage,
        borderRadius: new BorderRadius.circular(borderRadius),
      ),
      height: height == null ? 0 : height,
      width: width == null ? 0 : width,
      margin: margin == null ? EdgeInsets.all(0.0) : margin,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(isCircule ? 60.0 : borderRadius),
          border: Border.all(
              color:
                  borderColor == null ? CommonColors.primaryColor : borderColor,
              width: borderWidth == null ? 0.0 : borderWidth)),
    );
    return imageContainer;
  }
}
