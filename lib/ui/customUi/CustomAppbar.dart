import 'package:flutter/material.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/ImagePath.dart';
import 'package:property_launch/model/AppBarM.dart';


class MyAppbar extends StatelessWidget {
  final String title;
  final bool IsautomaticallyImplyLeading;
  final String profileImageUrl;
  final Function onNotificationTaped,
      onLogoutClicked,
      onReportClicked,
      onProfileClicked,
      onDrawerClicked,
      onBackPressed;
  final AppBarM appBarM;

  MyAppbar(
      {@required this.appBarM,
      this.title,
      this.IsautomaticallyImplyLeading,
      this.onNotificationTaped,
      this.onLogoutClicked,
      this.onReportClicked,
      this.onProfileClicked,
      this.onDrawerClicked,
      this.onBackPressed,
      this.profileImageUrl});

  @override
  Widget build(BuildContext context) {


    return new AppBar(
      title: new Text("Home"),
      automaticallyImplyLeading: false,
      backgroundColor: CommonColors.appBarColor,
      actions: <Widget>[],
    );
  }
}
