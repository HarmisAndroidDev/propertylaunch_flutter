import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/CommonColors.dart';

class RoundCornerTextField extends StatelessWidget {
  final Color backgroundColor;
  final String hintText;
  final Color textColor;
  final Color borderColor;
  final Function onChanged;
  bool isAllcaps = false;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final double width;
  final double height;
  final double radious;
  final double borderWidth;
  final int maxLines;
  final TextAlign textAlign;
  final TextEditingController controller;
  final TextInputType inputType;
  final FormFieldValidator<String> validator;
  final bool autoFocus;
  bool isPassword;
  FontWeight fontWeight;
  final Color hintTextColor;
  final int maxLength;
   bool isEnabled;

  RoundCornerTextField(
      {this.backgroundColor,
      this.hintText,
      this.textColor,
      this.borderColor,
      this.onChanged,
      this.isAllcaps,
      this.margin,
      this.padding,
      this.width,
      this.height,
      this.radious,
      this.borderWidth,
      this.maxLines,
      this.textAlign,
      this.controller,
      this.inputType,
      this.validator,
      this.autoFocus,
      this.isPassword,
      this.fontWeight,
      this.hintTextColor,this.maxLength,this.isEnabled});

  @override
  Widget build(BuildContext context) {
    if (isPassword == null) {
      isPassword = false;
    }
    final textField = new TextFormField(

      enabled: isEnabled == null ? true : isEnabled,
      maxLines: maxLines != null ? maxLines : 1,
      controller: controller,
      maxLength: maxLength,
      validator: validator,
      obscureText: isPassword ? true : false,
      textAlign: textAlign != null ? textAlign : TextAlign.left,
      autofocus: autoFocus == null ? false : autoFocus,
      keyboardType: inputType == null ? TextInputType.text : inputType,
      style: TextStyle(

          fontFamily: AppConstants.FONT_FAMILY,
          fontSize: 15.0,
          fontWeight: fontWeight == null ? FontWeight.w300 : fontWeight,
          color: CommonColors.color_363636),
      decoration: new InputDecoration(

        hintText: hintText != null ? hintText : "",
        counterText: "",
        hintStyle: TextStyle(
            color: hintTextColor == null ? CommonColors.black : hintTextColor),
        border: OutlineInputBorder(
            borderSide: BorderSide(
                color: borderColor == null
                    ? CommonColors.underlineColor
                    : borderColor)),
        contentPadding: EdgeInsets.all(10.0),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: borderColor == null
                    ? CommonColors.underlineColor
                    : borderColor)),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: borderColor == null
                    ? CommonColors.underlineColor
                    : borderColor)),
      ),
    );

    final textFieldContainer = new Container(
      margin: margin,
      //alignment: Alignment.center,
      // height: height != null ? height : 50,
      width: width != null ? width : MediaQuery.of(context).size.width,
      //decoration: boxDecoration,
      child: textField,
    );
    return textFieldContainer;
  }
}
