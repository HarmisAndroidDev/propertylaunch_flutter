import 'dart:io';

import 'package:flutter/material.dart';
import 'package:property_launch/utils/CommonColors.dart';

class CustomImage extends StatelessWidget {
  final String imageUrl;
  final EdgeInsets margin;
  final double borderWidth;
  final Color borderColor;
  final double height;
  final double width;
  final BorderRadius borderRadius;
  final BoxFit scaleType;

  CustomImage(
      {@required this.imageUrl,
      this.margin,
      this.borderWidth,
      this.borderColor,
      this.height,
      this.width,
      this.borderRadius,this.scaleType});

  @override
  Widget build(BuildContext context) {
    final networkImage = NetworkImage(imageUrl);
    final fileImage = FileImage(new File(imageUrl));

    // TODO: implement build
    return new Container(
        height: height == null ? 70 : height,
        width: width == null ? 70 : width,
        margin: margin == null ? EdgeInsets.all(0.0) : margin,
        decoration: new BoxDecoration(
          color: CommonColors.hint_text_color,
          shape: BoxShape.rectangle,
          border: Border.all(
              color: borderColor == null ? Colors.transparent : borderColor,
              width: borderWidth == null ? 1 : borderWidth),
          borderRadius: borderRadius == null
              ? BorderRadius.all(Radius.circular(0))
              : borderRadius,
          image: new DecorationImage(
              fit: scaleType == null ? BoxFit.cover : scaleType,
              image: imageUrl.startsWith("http") ? networkImage : fileImage),
        ));
  }
}
