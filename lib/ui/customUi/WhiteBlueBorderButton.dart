import 'package:flutter/material.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/CommonColors.dart';

class WhiteBlueBorderButton extends StatelessWidget {
  final Color backgroundColor;
  final String buttonText;
  final Color textColor;
  final Function onPressed;
  bool isAllcaps = false;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final double width;
  final double height;

  WhiteBlueBorderButton(
      {this.backgroundColor,
      this.buttonText,
      this.textColor,
      this.onPressed,
      this.isAllcaps,
      this.margin,
      this.padding,
      this.width,
      this.height});

  @override
  Widget build(BuildContext context) {
    if (isAllcaps == null) {
      isAllcaps = false;
    }
    return Container(
      width: width == null ? 80 : width,
      height: height == null ? 40 : height,
      margin: margin,
      padding: padding,
      child: OutlineButton(
        color: CommonColors.primaryColor,
        borderSide: BorderSide(color: CommonColors.primaryColor, width: 2),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: EdgeInsets.only(left: 8, right: 8, top: 0, bottom: 0),
          child: Text(isAllcaps ? buttonText.toUpperCase() : buttonText,
              style: TextStyle(
                  color: CommonColors.primaryColor,
                  fontFamily: AppConstants.FONT_FAMILY,
                  fontSize: 12,
                  fontWeight: FontWeight.w800)),
        ),
        onPressed: onPressed,
      ),
    );
    /* return new Container(
        width: width == null ? 200 : width,
        height: height == null ? 50 : height,
        margin: margin,
        padding: padding,
        child: Material(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          color: CommonColors.primaryColor,
          clipBehavior: Clip.antiAlias,
          child: MaterialButton(
            minWidth: 200,
            height: 50,
            color: CommonColors.primaryColor,
            child: Text(isAllcaps ? buttonText.toUpperCase() : buttonText,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: AppConstants.FONT_FAMILY,
                    fontSize: 16,
                    fontWeight: FontWeight.w700)),
            onPressed: onPressed,
          ),
        ));*/
  }
}
