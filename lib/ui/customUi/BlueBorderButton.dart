import 'package:flutter/material.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/CommonColors.dart';

class BlueBorderButton extends StatelessWidget {
  final Color backgroundColor;
  final String buttonText;
  final Color textColor;
  final Function onPressed;
  bool isAllcaps = false;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final double width;
  final double height;
  final FontWeight fontWeight;
  final double fontsize;
  final Color fontColor;
  final double radious;
  final double borderWidth;
  final Color borderColor;

  BlueBorderButton(
      {this.backgroundColor,
      this.buttonText,
      this.textColor,
      this.onPressed,
      this.isAllcaps,
      this.margin,
      this.padding,
      this.width,
      this.height,
      this.fontWeight,
      this.fontsize,
      this.fontColor,
      this.radious,
      this.borderColor,
      this.borderWidth});

  @override
  Widget build(BuildContext context) {
    if (isAllcaps == null) {
      isAllcaps = false;
    }
    return new InkWell(
      onTap: onPressed,
      child: new Container(
        width: width == null ? 200 : width,
        height: height == null ? 20 : height,
        margin: margin,
        decoration: BoxDecoration(
          color: backgroundColor == null ? Colors.white : backgroundColor,
          border: Border.all(
            color:
                borderColor == null ? CommonColors.primaryColor : borderColor,
            width: borderWidth == null ? 1.0 : borderWidth,
          ),
          borderRadius: BorderRadius.circular(radious == null ? 30 : radious),
        ),
        child: new Center(
          child: new Text(
            buttonText,
            style: new TextStyle(
                fontSize: fontsize == null ? 16 : fontsize,
                fontWeight: fontWeight == null ? FontWeight.w700 : fontWeight,
                fontFamily: AppConstants.FONT_FAMILY,
                color: fontColor == null ? Colors.white : fontColor),
          ),
        ),
      ),
    );
  }
}
