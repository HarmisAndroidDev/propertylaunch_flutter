import 'package:flutter/material.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/CommonColors.dart';

class SimpleRoundButton extends StatelessWidget {
  final Color backgroundColor;
  final String buttonText;
  final Color textColor;
  final Function onPressed;
  bool isAllcaps = false;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final double width;
  final double height;
  final FontWeight fontWeight;
  final double fontSize;
  final Color fontColor;
  final Color borderColor;
  final double radious;

  SimpleRoundButton(
      {this.backgroundColor,
      this.buttonText,
      this.textColor,
      this.onPressed,
      this.isAllcaps,
      this.margin,
      this.padding,
      this.width,
      this.height,this.fontWeight,this.fontSize,this.fontColor,this.borderColor,this.radious});

  @override
  Widget build(BuildContext context) {
    if (isAllcaps == null) {
      isAllcaps = false;
    }
    return new Container(
        width: width == null ? 200 : width,
        height: height == null ? 50 : height,
        margin: margin,
        padding: padding,
        /*decoration: new BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          gradient: LinearGradient(
              colors: [Colors.blue,Colors.lightBlueAccent, Colors.black12],
              begin: FractionalOffset(0.20, 0.0),
              end: FractionalOffset(0.5, 0.0),
              stops: [3.0,2.5,5.0],
              tileMode: TileMode.clamp),
        ),*/
        child: Material(
          shape:
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(radious == null ? 30.0 : radious)),
         color: borderColor == null ? Colors.transparent : borderColor,
          clipBehavior: Clip.antiAlias,
          child: MaterialButton(
            minWidth: 200,
            height: 50,
            color: CommonColors.primaryColor,
            child: Text(isAllcaps ? buttonText.toUpperCase() : buttonText,
                style: TextStyle(
                    color: fontColor == null ? Colors.white  : fontColor,
                    fontFamily: AppConstants.FONT_FAMILY,
                    fontSize: fontSize == null ? 16 : fontSize,
                    fontWeight: fontWeight  == null ? FontWeight.w700 : fontWeight)),
            onPressed: onPressed,
          ),
        ));
  }
}
