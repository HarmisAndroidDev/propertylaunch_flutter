import 'dart:io';

import 'package:flutter/material.dart';
import 'package:property_launch/utils/CommonColors.dart';

class CircleImageView extends StatelessWidget {
  final String imageUrl;
  final EdgeInsets margin;
  final double borderWidth;
  final Color borderColor;
  final double height;
  final double width;

  CircleImageView(
      {this.imageUrl,
      this.margin,
      this.borderWidth,
      this.borderColor,
      this.height,
      this.width});

  @override
  Widget build(BuildContext context) {
    final networkImage = NetworkImage(imageUrl);

    final fileImage = FileImage(new File(imageUrl));

    // TODO: implement build
    return new Container(
        height: height == null ? 70 : height,
        width: width == null ? 70 : width,
        margin: margin == null ? EdgeInsets.all(0.0) : margin,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(60.0),
            border: Border.all(
                color: borderColor == null
                    ? CommonColors.primaryColor
                    : borderColor,
                width: borderWidth == null ? 2.0 : borderWidth)),
//        child: imageUrl.startsWith("http") ? networkImage : localImage);
        child: CircleAvatar(
          radius: 30.0,
          backgroundImage:
              imageUrl.startsWith("http") ? networkImage : fileImage,
          backgroundColor: Colors.transparent,
        ));
  }
}
