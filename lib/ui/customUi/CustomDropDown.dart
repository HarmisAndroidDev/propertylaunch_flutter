import 'package:flutter/material.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CustomBoxShadow.dart';
import 'package:property_launch/utils/ImagePath.dart';

class CustomDropDown extends StatelessWidget {
  final String selectedValue;
  final Function onChanged;
  final String hint;
  final List<String> items;
  final double height;
  final double width;
  final Color textColor;
  final EdgeInsets margin;

  CustomDropDown(
      {this.selectedValue,
      this.onChanged,
      this.hint,
      this.items,
      this.height,
      this.width,
      this.textColor,this.margin});

  @override
  Widget build(BuildContext context) {
    final boxDecoration = new BoxDecoration(
        boxShadow: [
          CustomBoxShadow(
            color: Colors.grey,
            blurRadius: 1.0,
            blurStyle: BlurStyle.outer,
            offset: Offset(
              0.0,
              0.0,
            ),
          )
        ],
        border: new Border.all(color: CommonColors.primaryColor),
        borderRadius: BorderRadius.circular(3));
    final btnDropDown = new Container(
        child: DropdownButton<String>(
      value: selectedValue,
      icon: new Container(),
      underline: new DropdownButtonHideUnderline(child: new Container()),
      onChanged: onChanged,
      hint: Text(hint),
      items: items.map<DropdownMenuItem<String>>((Object value) {
        return DropdownMenuItem<String>(
          value: value,
          child: new Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 5.0, right: 5.0),
              child: new Text(
                value,
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontSize: 16.0,
                    fontFamily: AppConstants.FONT_FAMILY,
                    color: textColor == null
                        ? CommonColors.primaryColor
                        : textColor),
              )),
        );
      }).toList(),
    ));
    final dropIcon = new Container(
        width: width != null ? width : 100,
        height: height != null ? height : 40,
        child: new Stack(
          alignment: Alignment.centerRight,
          children: <Widget>[
            new Align(
              child: btnDropDown,
              alignment: Alignment.center,
            ),
            Image.asset(LocalImages.ic_down,
                color: CommonColors.primaryColor, height: 15, width: 15)
          ],
        ));
    final classDropDown = new Container(
      margin: margin == null ? EdgeInsets.all(0.0) : margin,
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      decoration: boxDecoration,
      child: dropIcon,
    );

    return classDropDown;
  }
}
