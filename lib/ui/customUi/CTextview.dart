import 'package:flutter/material.dart';
import 'package:property_launch/utils/AppConstants.dart';

class CTextView extends StatelessWidget {
  final EdgeInsets margin;
  final EdgeInsets padding;
  final String text;
  final TextAlign textAlign;
  final FontWeight fontWeight;
  final double textSize;
  final Color backgroundColor;
  final Color textColor;
  final String fontFamily;
  final int maxLines;
  final TextOverflow textOverflow;

  CTextView(
      {this.margin,
      this.padding,
      this.text,
      this.textAlign,
      this.fontWeight,
      this.textSize,
      this.backgroundColor,
      this.textColor,
      this.fontFamily,
      this.maxLines,
      this.textOverflow});

  @override
  Widget build(BuildContext context) {
    return new Container(
      color: backgroundColor == null ? Colors.white : backgroundColor,
      margin: margin == null ? EdgeInsets.all(0.0) : margin,
      padding: padding == null ? EdgeInsets.all(0.0) : padding,
      child: new Text(
        text == null ? "NULL" : text,
        textAlign: textAlign == null ? TextAlign.start : textAlign,
        maxLines: maxLines == null ? 100 : maxLines,
        overflow: textOverflow == null ? TextOverflow.ellipsis : textOverflow,
        style: TextStyle(
            color: textColor == null ? Colors.black : textColor,
            fontFamily:
                fontFamily == null ? AppConstants.FONT_FAMILY : fontFamily,
            fontSize: textSize == null ? 10.0 : textSize,
            fontWeight: fontWeight == null ? FontWeight.normal : fontWeight),
      ),
    );
    ;
  }
}
