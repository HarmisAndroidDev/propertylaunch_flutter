import 'dart:async';

import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:property_launch/arguments/PropertyDetailArguments.dart';
import 'package:property_launch/arguments/VideoStyleDetailArguments.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/model/videoStyleList/VideoStyleDetails.dart';
import 'package:property_launch/ui/owner/VideoStyleListPage.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:video_player/video_player.dart';

import 'customUi/BlueBorderButton.dart';
import 'customUi/CTextview.dart';
import 'package:http/http.dart' as http;


class VideoStyleInfo extends StatefulWidget {
  static String tag = "video-style-page";


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return _VideoStyleInfoState();
  }
}

class _VideoStyleInfoState extends State<VideoStyleInfo> {
  Timer timer;
  bool isLoading = false;
  OwnerPropertyDetails _ownerPropertyDetails;
  VideoStyleListDetails  videoStyleListDetails;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
  receiveArguements(context);
    return initViews();
  }

  void receiveArguements(BuildContext context) {
    final PropertyDetailsArguments propertyDetailsArguments =
        ModalRoute.of(context).settings.arguments;
    _ownerPropertyDetails = propertyDetailsArguments.ownerPropertyDetails;
  }

  VideoPlayerController videoPlayerController;
  Widget initViews() {
     videoPlayerController =
        VideoPlayerController.network(ApiUrls.dummyVideoUrl);

    final chewieController = ChewieController(
        videoPlayerController: videoPlayerController,
        aspectRatio: 3 / 2,
        autoPlay: true,
        looping: false,
        showControls: true);

    final playerWidget = Chewie(
      controller: chewieController,
    );

    final videoView = new Container(
        height: 200,
        margin: EdgeInsets.only(top: 10.0),
        child: new ClipRRect(
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            child: new Container(color: Colors.black, child: playerWidget)));

    final btnVideoAd = new Align(
        alignment: Alignment.centerRight,
        child:new Container(
      width: 200,
        child: new InkWell(
            onTap: () {
              Navigator.pushNamed(context, VideoStyleListPage.tag,
                  arguments: PropertyDetailsArguments(_ownerPropertyDetails,false));                 },
            child: new BlueBorderButton(
              margin: EdgeInsets.only(top: 20.0),
              height: AppDimens.btn_height,
              buttonText: AppConstants.view_video_styles,
              width: double.infinity,
              radious: 5.0,
              backgroundColor: CommonColors.blue_btn_color,
            ))));

    final tvVideoDetails = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.justify,
          text: AppConstants.dummyParagraph,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    final mainListview = new ListView(
      padding: EdgeInsets.all(10.0),
      children: <Widget>[videoView, btnVideoAd, tvVideoDetails],
    );

    return new Scaffold(
      appBar: AppBar(
        title: CTextView(text: AppConstants.video_style,
        textSize: AppDimens.appBarTitleSize,textColor: Colors.white,backgroundColor: Colors.transparent,),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context, false),
          color: Colors.white,
        ),
        backgroundColor: CommonColors.primaryColor,
        automaticallyImplyLeading: true,
      ),
      body: mainListview,
    );
  }

  @override
  void dispose() {
    super.dispose();
    videoPlayerController.dispose();
  }


  Future<http.Response> getVideoStyleInfoApi() {
    setState(() {
      isLoading =true;
    });
    return http.get(ApiUrls.getVideoInfo).then((http.Response reposnse) {
      String res = reposnse.body;
      if(reposnse.statusCode == 200){

      }

    });
  }



}





