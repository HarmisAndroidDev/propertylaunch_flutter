import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:property_launch/arguments/PropertyDetailArguments.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyListMaster.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';

import 'customUi/CTextview.dart';
import 'customUi/ProgressImageView.dart';
import 'owner/PropertyDetailPage.dart';
import 'package:http/http.dart' as http;

class PropertyList extends StatefulWidget {
  static String tag = "property-list-page";



  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PropertyListState();
  }
}

class _PropertyListState extends State<PropertyList> {
  // bool isLoading = false, isApiLoading = false, isListAvailable = true;
  int startLimit = 0, totalCount = 0, perPage = 10;
  BuildContext mContext;
  ScrollController controller;
  List<OwnerPropertyDetails> propertyList = new List();
  LoginDetails loginDetails;
  AppSettings appSettings;
  String mErrorMessage = AppConstants.text_no_data_found;
  bool isLoading = false, isListAvailable = true;
  int clickedIndex = 0;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("init state called");
    appSettings = new AppSettings();
    appSettings.getLoginDetails().then((value) {
      loginDetails = LoginDetails.fromJson(json.decode(value));
      controller = new ScrollController()..addListener(_scrollListener);
    });

    Future.delayed(Duration.zero, () {
      CommonUtils.isInternetConnected().then((isConnected){
        if(isConnected){
          getPropertyLisApiCall();
        }else{
          CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
        }
      });

    });

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print("Build method  called  Times");
    if(CommonUtils.isPropertyAdded){
      CommonUtils.isPropertyAdded =false;
      setState(() {
        propertyList.insert(0, CommonUtils.ownerPropertyDetails);
      });
    }


    return _initViews(context);
  }

  void _scrollListener() {
    if (controller.position.extentAfter < 100) {
      /* if (startLimit < totalCount && !isApiLoading) {
        startLimit += perPage;
      }*/
    }
  }

  void updateList(){
    Future.delayed(Duration.zero, () {
      setState(() {
        print("List updated");
        getPropertyLisApiCall();
      });
    });
  }



  Widget _initViews(BuildContext context) {
    final listVIew = new ListView.builder(
      padding: EdgeInsets.all(15.0),
      shrinkWrap: true,
      itemCount: propertyList.length,
      itemBuilder: (BuildContext context, int index) {
        return new GestureDetector(
          child: buildItems(index),
          onTap: () {
          clickedIndex = index;
            redirectToAddPropertyPage(propertyList[index]);
          },
        );
      },
    );

    final noDataContainer = new Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child: new Center(
        child: new CTextView(
          text: mErrorMessage,
          textAlign: TextAlign.center,
          fontWeight: FontWeight.w500,
          fontFamily: AppConstants.FONT_FAMILY,
          textColor: Colors.black,
          textSize: 16,
        ),
      ),
    );

    final scaffold = new Scaffold(
      body: new Builder(builder: (BuildContext context) {
        mContext = context;
        return isLoading
            ? CommonUtils.showProgressBar(listVIew)
            : (isListAvailable ? listVIew : noDataContainer);
      }),
    );

    return scaffold;
  }

  Widget buildItems(int index) {
    final ivPropertyImage = new Align(
        alignment: Alignment.centerLeft,
        child: new ProgressImageView(
          isCircule: false,
          borderRadius: 10.0,
          imageurl: propertyList[index].proImg,
          height: 110,
          width: 110,
        ));

    final tvPropertyName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 2.0),
          textAlign: TextAlign.left,
          text: propertyList[index].proName,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));

    final tvPropertyAddress = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 2,
          margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
          text: propertyList[index].proAddress,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    final tvAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: propertyList[index].agentName,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    final propertyColumn = new Container(
        height: 110,
        margin: EdgeInsets.only(left: 85.0),
        child: new Card(
            child: new Container(
                margin: EdgeInsets.only(left: 10.0),
                padding: EdgeInsets.all(5.0),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    tvPropertyName,
                    tvPropertyAddress,
                    tvAgentName
                  ],
                ))));

    final propertyStack = new Container(
        height: 120,
        child: new Align(
            alignment: Alignment.center,
            child: new Stack(
              children: <Widget>[
                ivPropertyImage,
                new Align(
                  child: propertyColumn,
                  alignment: Alignment.center,
                ),
              ],
            )));

/*
    final fotterProgress = new Container(
      child: new Center(child: new CircularProgressIndicator(),),
      width: double.infinity,
      height: 40,
      decoration: BoxDecoration(color: CommonColors.text_color,borderRadius: BorderRadius.all(Radius.circular(10.0))),
    );
*/
    return propertyStack;
  }

  /* property list api call */

  getPropertyLisApiCall() async {
    setState(() {
      propertyList.clear();

      isListAvailable = true;
    });
    CommonUtils.showProgressDialog(mContext);
    final response =
        await http.post(ApiUrls.getProperyList, body: getPropertyLisParams());
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      CommonUtils.hideProgressDialog(mContext);
      print("Response: " + response.body);
      OwnerPropertyListMaster master =
          OwnerPropertyListMaster.fromJson(json.decode(response.body));
      if (master != null) {
        if (master.success == 1) {
          CommonUtils.isPropertyAdded = false;
          if (master.result != null && master.result.length > 0) {
            setState(() {
              propertyList.addAll(master.result);
            });
          } else {
            print("_showEmptyLayout");
            _showEmptyLayout();
          }
        } else {
          if (!CommonUtils.isEmpty(master.message)) {
            setState(() {
              mErrorMessage = master.message;
            });
          }
          _showEmptyLayout();
        }
      } else {
        CommonUtils.hideProgressDialog(mContext);
        _showEmptyLayout();
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);
      _showEmptyLayout();
      throw Exception('Exception');
    }
  }

  void _showEmptyLayout() {
    if (startLimit == 0) {
      setState(() {
        isListAvailable = false;
      });
    }
  }

  String getPropertyLisParams() {
    var map = new Map<String, dynamic>();
    map[ApiParams.ownerid] = loginDetails.userId;
    if (loginDetails.userType == AppConstants.userTypeAgent) {
      map[ApiParams.userType] = AppConstants.userTypeAgent;
    } else {
      map[ApiParams.userType] = AppConstants.userTypeOwner;
    }
    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }

  redirectToAddPropertyPage(OwnerPropertyDetails ownerPorpertyDetails) {
    Navigator.pushNamed(mContext, PropertyDetailPage.tag,
            arguments: PropertyDetailsArguments(ownerPorpertyDetails, true))
        .then((value) {
      if (value != null) {
        var result = value as Map;
        if (result.containsKey("propertyDetails") &&
            result.containsKey(AppConstants.isAdded)) {
          print("Object Received proprty Lisyt" +
              result[AppConstants.isAdded].toString());
          setState(() {
            if (!result[AppConstants.isAdded]) {
              propertyList[clickedIndex] = result["propertyDetails"];
             }

          });
        }
      }
    });
  }
}
