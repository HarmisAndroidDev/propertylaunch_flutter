import 'dart:async';
import 'dart:convert';

import 'package:chewie/chewie.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:property_launch/arguments/PaymentHistoryArguments.dart';
import 'package:property_launch/arguments/PropertyDetailArguments.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyListMaster.dart';
import 'package:property_launch/model/payment/PaymentDetails.dart';
import 'package:property_launch/ui/customUi/BlueBorderButton.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/ui/customUi/ProgressImageView.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:property_launch/utils/CustomBoxShadow.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:property_launch/utils/ImagePath.dart';
import 'package:video_player/video_player.dart';
import 'package:http/http.dart' as http;

import 'customUi/MyCustomDialog.dart';

class HistoryPropertyDetailPage extends StatefulWidget {
  static String tag = "history-property-detail-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HistoryPropertyDetailPageState();
  }
}

class _HistoryPropertyDetailPageState extends State<HistoryPropertyDetailPage> {
  BuildContext mContext;
  Set<Marker> markerSet = new Set();
  static CameraPosition _initialCamera = CameraPosition(
    target: LatLng(23.023854, 72.538471),
    zoom: 16.0,
  );

  static LatLng propertyLatLong = new LatLng(23.023854, 72.538471);
  static String markerId = "102";

  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _mapController;
  GoogleMap googleMap;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Marker propertyMarker = new Marker(
      markerId: new MarkerId(markerId),
      position: propertyLatLong,
      infoWindow: InfoWindow(title: "", snippet: ""),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));

  OwnerPropertyDetails _ownerPropertyDetails;

  PaymentDetails _paymentDetails;

  LoginDetails _loginDetails;
  bool isFirstTime = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    markers[new MarkerId(markerId)] = propertyMarker;
    AppSettings appSettings = new AppSettings();
    appSettings.getLoginDetails().then((onValue) {
      setState(() {
        _loginDetails = LoginDetails.fromJson(json.decode(onValue));
      });
    });

    Future.delayed(Duration.zero,(){
      CommonUtils.isInternetConnected().then((isConnected){
        if(isConnected){
          getPropertyDetail();
        }else{
          CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
        }
      });
    });
  }

  void receiveArguements(BuildContext context) {
    final PaymentHistoryArguments propertyDetailsArguments =
        ModalRoute.of(context).settings.arguments;
    _paymentDetails = propertyDetailsArguments.paymentDetails;
    print("arguments received"+_paymentDetails.propertyid.toString());

  }

  void setmapvalues() {
    if (_ownerPropertyDetails != null) {
      double lat = _ownerPropertyDetails.lat == ""
          ? 0.0
          : double.parse(_ownerPropertyDetails.lat);
      double long = _ownerPropertyDetails.long == ""
          ? 0.0
          : double.parse(_ownerPropertyDetails.long);

      LatLng propertyLatLong = new LatLng(lat, long);
      _initialCamera = CameraPosition(
        target: propertyLatLong,
        zoom: 16.0,
      );
      propertyMarker = new Marker(
          markerId: new MarkerId(markerId),
          position: propertyLatLong,
          infoWindow: InfoWindow(
              title: _ownerPropertyDetails.proName,
              snippet: _ownerPropertyDetails.propertyInfo),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));
      markers[new MarkerId(markerId)] = propertyMarker;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (isFirstTime) {
      isFirstTime = false;
      receiveArguements(context);
    }
    return _initViews();
  }

  Widget _initViews() {
    final scaffold = new Scaffold(
      appBar: AppBar(
        title: CTextView(
          backgroundColor: Colors.transparent,
          textSize: 20.0,
          text: AppConstants.order_history_details,
          textColor: Colors.white,
        ),
        automaticallyImplyLeading: true,
        backgroundColor: CommonColors.primaryColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context, false),
          color: Colors.white,
        ),
      ),
      backgroundColor: Colors.white,
      body: new Builder(builder: (BuildContext context) {
        mContext = context;
        return buildItems();
      }),
    );

    return scaffold;
  }

  Widget buildItems() {
    final ivPropertyImage = new Align(
        child: new ProgressImageView(
          isCircule: false,
          borderRadius: 20.0,
          imageurl: _ownerPropertyDetails == null
              ? ApiUrls.dummyImage
              : (_ownerPropertyDetails.proImg != null ? _ownerPropertyDetails.proImg : ApiUrls.dummyImage),
          height: 200,
          width: double.infinity,
        ));
    final propertyThumbnailList = new ListView.builder(
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: _ownerPropertyDetails == null
            ? 0
            : _ownerPropertyDetails.propertyImage.length,
        itemBuilder: (context, index) {
          return new GestureDetector(
            onTap: (){
              setState(() {
                _ownerPropertyDetails.proImg = _ownerPropertyDetails.propertyImage[index].image;
              });
            },
            child: _ownerPropertyDetails == null ? new Container() : propertyThumb(index)
          ,

          );
        });
    final tvPropertyName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 2.0),
          textAlign: TextAlign.left,
          text: _ownerPropertyDetails == null
              ? ""
              : _ownerPropertyDetails.proName,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final tvPropertyAddress = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 2,
          margin: EdgeInsets.only(top: 5.0),
          text: _ownerPropertyDetails == null
              ? ""
              : _ownerPropertyDetails.proAddress,
          textSize: 16.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w500,
        ));
    final tvSelectHome = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: "Select Home",
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));

    final tvVideoStyle = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.video_style,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));

    final imageView = new Container(
        foregroundDecoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            color: Colors.black54),
        child: new ProgressImageView(
          isCircule: false,
          borderRadius: 20.0,
          height: 160,
          width: double.infinity,
          imageurl: ApiUrls.dummyImage,
        ));
    final playButton = new Container(
        height: 160,
        width: double.infinity,
        padding: EdgeInsets.all(10.0),
        child: new InkWell(
            onTap: (){
              showVideoDialog(ApiUrls.dummyVideoUrl);
            },
            child:new Align(
          alignment: Alignment.center,
          child: new Icon(
            Icons.play_arrow,
            color: Colors.white,
            size: 80,
          ),
        )));

    final stackImage = new Container(
        margin: EdgeInsets.only(top: 20.0),
        child: new Stack(
          children: <Widget>[imageView, playButton],
        ));

    final agentIconBox = new Container(
      height: 50,
      width: 50,
      margin: EdgeInsets.only(left: 5.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5)),
      child: new Center(
        child: ProgressImageView(
            isCircule: false,
            height: 50,
            width: 50,
            borderRadius: 5.0,
            imageurl: _ownerPropertyDetails == null ? "" : ApiUrls.dummyImage),
      ),
    );
    final lblAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: AppConstants.agency_name,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final tvAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 10.0),
          maxLines: 1,
          textAlign: TextAlign.start,
          text: _ownerPropertyDetails ==  null ? "" : (_ownerPropertyDetails.agentName == null ? "" :_ownerPropertyDetails.agentName),
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final agentDetails = new Container(
        margin: EdgeInsets.only(left: 10.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[lblAgentName, tvAgentName],
        ));
    final layoutAgent = new Container(
      height: 60,
      margin: const EdgeInsets.only(top: 20.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[agentIconBox, agentDetails],
      ),
    );
    final lblPropertyDetails = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_detail,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final tvPropertyDetail = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 20.0),
          text: _ownerPropertyDetails == null
              ? ""
              : _ownerPropertyDetails.propertyInfo,
          textSize: 16.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final lblPropertyFeatures = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0, bottom: 10.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_features,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final itemGridDelegate = new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 5.0,
        mainAxisSpacing: 5.0,
        crossAxisCount: 2,
        childAspectRatio: 6.0);
    final featuresList = new GridView.builder(
        gridDelegate: itemGridDelegate,
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: _ownerPropertyDetails == null
            ? 0
            : _ownerPropertyDetails.feName.length,
        itemBuilder: (context, index) {
          if (_ownerPropertyDetails == null) {
            return new Container();
          } else {
            return propertyFeatures(index, _ownerPropertyDetails.feName);
          }
        });
    final divider = new Container(
      width: double.infinity,
      color: CommonColors.text_color,
      height: 1,
      margin: EdgeInsets.only(top: 20.0),
    );
    googleMap = GoogleMap(
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
        ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
                () => VerticalDragGestureRecognizer())),
      mapType: MapType.normal,
      zoomGesturesEnabled: true,
      myLocationButtonEnabled: true,
      myLocationEnabled: false,
      markers: Set<Marker>.of(markers.values),
      minMaxZoomPreference: MinMaxZoomPreference.unbounded,
      initialCameraPosition: _initialCamera,
      onMapCreated: (GoogleMapController googleMapController) {
        _mapController = googleMapController;
        _controller.complete(googleMapController);
      },
    );
    final mapView = new Container(
        height: 200,
        width: double.infinity,
        margin: EdgeInsets.only(top: 20.0),
        child: new ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            child: googleMap));
    final detailListview = ListView(
      shrinkWrap: true,
      padding:
      EdgeInsets.only(top: 10.0, bottom: 10.0, right: 15.0, left: 15.0),
      children: <Widget>[
        ivPropertyImage,
        new Container(height: 80, child: propertyThumbnailList),
        tvPropertyName,
        tvPropertyAddress,
        tvSelectHome,
        tvVideoStyle,
        stackImage,
        layoutAgent,
        lblPropertyDetails,
        tvPropertyDetail,
        divider,
        lblPropertyFeatures,
        featuresList,
        mapView
      ],
    );
    return detailListview;
  }

  Future requestPermission() async {
    Map<PermissionGroup, PermissionStatus> permissions =
    await PermissionHandler()
        .requestPermissions([PermissionGroup.location]);
  }

  Future<int> checkPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    return permission.value;
  }

  Widget propertyThumb(int index) {
    final ivPropertyImage = new Align(
        alignment: Alignment.centerLeft,
        child: new ProgressImageView(
          margin: EdgeInsets.only(right: 10.0),
          isCircule: false,
          borderRadius: 10.0,
          imageurl: _ownerPropertyDetails.propertyImage[index].image,
          height: 55,
          width: 80,
        ));

    return ivPropertyImage;
  }

  getPropertyDetail() async {
    CommonUtils.showProgressDialog(mContext);
    final response = await http.post(ApiUrls.getPropertyDetail,
        body: getPropertyDetailparams());
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      CommonUtils.hideProgressDialog(mContext);
      print("Response: " + response.body);
      OwnerPropertyListMaster master =
      OwnerPropertyListMaster.fromJson(json.decode(response.body));
      if (master != null) {
        if (master.success == 1) {
          setState(() {
            _ownerPropertyDetails = master.result[0];
            setmapvalues();

          });
        } else {
          if (mContext == null) {
            print("mContext receiving null");
          }
          CommonUtils.showErrorMessage(mContext, master.message);
        }
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);

      CommonUtils.showErrorMessage(mContext, "" + AppConstants.oops);
      throw Exception('Exception');
    }
  }

  String getPropertyDetailparams() {
    var map = new Map<String, dynamic>();
    map[ApiParams.propertyId] = "326";
    map[ApiParams.userId] = "3";
    map[ApiParams.userType] = "agent";
    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }

  Widget propertyFeatures(int index, List<String> featuresList) {
    final tvFeatureName = new Container(
        width: 150,
        margin: EdgeInsets.only(top: 0.0),
        child: new CTextView(
          text: featuresList[index],
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w600,
        ));

    return tvFeatureName;
  }

  void showVideoDialog(String videoUrl) {
    final videoPlayerController = VideoPlayerController.network("http:\/\/propertylaunch.pro\/public\/templateVideo\/20190516.180301_10_A_GardenHill_16.mp4");

    final chewieController = ChewieController(
        videoPlayerController: videoPlayerController,
        aspectRatio: 3 / 2,
        autoPlay: true,
        looping: false,
        showControls: true);

    final playerWidget = Chewie(
      controller: chewieController,
    );

    final videoView = new Container(
        height: 250,
        child: new ClipRRect(
          clipBehavior: Clip.hardEdge,
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          child: new FittedBox(
            child: new Container(
                padding: EdgeInsets.all(5.0),
                color: Colors.black,
                child: playerWidget),
            fit: BoxFit.cover,
          ),
        ));

    MyCustomDialog changePasswordDialog = MyCustomDialog(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      //this right here
      child: new Container(
          margin: EdgeInsets.only(left: 20, right: 20), child: videoView),
    );

    /*showDialog(
        context: context,
        builder: (BuildContext context) {
          return changePasswordDialog;
        });*/

    showMyDialog(
        context: mContext,
        builder: (BuildContext context) {
          return changePasswordDialog;
        });
  }

}
