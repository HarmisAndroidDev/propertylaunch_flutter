import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:property_launch/model/agent/AgentDetails.dart';
import 'package:property_launch/model/agent/AgentListMaster.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:property_launch/utils/CustomBoxShadow.dart';

import 'customUi/CTextview.dart';
import 'customUi/ProgressImageView.dart';
import 'package:http/http.dart' as http;

class AgentList extends StatefulWidget {
  static String tag = "agent-list-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AgentListState();
  }
}

class _AgentListState extends State<AgentList> {
  bool isLoading = false, isListAvailable = true;
  List<AgentDetails> agentList = new List();
  BuildContext _mContext;

  @override
  void initState() {
    super.initState();



    Future.delayed(Duration.zero, () {

      CommonUtils.isInternetConnected().then((isConnected){
        if(isConnected){
          getAgentLisApiCall();
        }else{
          CommonUtils.showErrorMessage(_mContext, AppConstants.no_internet);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return _initViews(context);
  }

  Widget _initViews(BuildContext context) {
    final listVIew = new ListView.builder(
      padding: EdgeInsets.all(15.0),
      shrinkWrap: true,
      itemCount: agentList.length,
      itemBuilder: (BuildContext context, int index) {
        return new GestureDetector(
          child: buildItems(index),
          onTap: () {
            backToAddProperty(agentList[index], _mContext);
          },
        );
      },
    );

    final noDataContainer = new Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child: new Center(
        child: new CTextView(
          text: AppConstants.text_no_data_found,
          textAlign: TextAlign.center,
          fontWeight: FontWeight.w500,
          fontFamily: AppConstants.FONT_FAMILY,
          textColor: Colors.black,
          textSize: 16,
        ),
      ),
    );

    final scaffold = new Scaffold(
      appBar: AppBar(
        title: CTextView(
          text: AppConstants.agentList,
          textSize: AppDimens.appBarTitleSize,
          backgroundColor: Colors.transparent,
          textColor: Colors.white,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context, false),
          color: Colors.white,
        ),
        backgroundColor: CommonColors.primaryColor,
        automaticallyImplyLeading: true,
      ),
      body: new Builder(builder: (BuildContext context) {
        _mContext = context;
        return isListAvailable ? listVIew : noDataContainer;
      }),
    );

    return scaffold;
  }

  Widget buildItems(int index) {
    final agentIconBox = new Container(
      height: 70,
      width: 70,
      margin: EdgeInsets.only(left: 5.0),
      padding: EdgeInsets.all(10.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5)),
      child: new Center(
        child: ProgressImageView(
          height: 70,
          width: 70,
          borderRadius: 5,
          isCircule: false,
          imageurl: agentList[index].profileImage,
        ),
      ),
    );
    final lblAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: "Agent name",
          textSize: 16.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final tvAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 10.0),
          maxLines: 1,
          textAlign: TextAlign.start,
          text: agentList[index].userName,
          textSize: 16.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final agentDetails = new Container(
        margin: EdgeInsets.only(left: 10.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[lblAgentName, tvAgentName],
        ));
    final layoutAgent = new Container(
      height: 80,
      margin: const EdgeInsets.only(top: 10.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[agentIconBox, agentDetails],
      ),
    );
    return layoutAgent;
  }

  getAgentLisApiCall() async {
    /* setState(() {
      isListAvailable = true;
      isLoading = true;
    });
   */
    CommonUtils.showProgressDialog(_mContext);
    final response =
        await http.post(ApiUrls.getAgentList, body: getAgentLisParams());
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      /* setState(() {
        isLoading = false;
      });
     */
      CommonUtils.hideProgressDialog(_mContext);
      print("Response: " + response.body);
      AgentListMaster master =
          AgentListMaster.fromJson(json.decode(response.body));
      if (master != null) {
        if (master.success == 1) {
          // totalCount = master.totalCount;
          if (master.result != null && master.result.length > 0) {
            agentList.addAll(master.result);
            setState(() {
              isListAvailable = true;
            });
          } else {
            setState(() {
              isListAvailable = false;
            });

            print("_showEmptyLayout");
          }
        } else {
          if (!CommonUtils.isEmpty(master.message)) {
            setState(() {});
          }
        }
      } else {
        CommonUtils.hideProgressDialog(_mContext);
        setState(() {
          isListAvailable = false;
        });
      }
    } else {
      CommonUtils.hideProgressDialog(_mContext);
      setState(() {
        isListAvailable = false;
      });
      throw Exception('Exception');
    }
  }

  String getAgentLisParams() {
    var map = new Map<String, dynamic>();
    map[ApiParams.userType] = AppConstants.userTypeOwner;
    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }

  void backToAddProperty(AgentDetails agentDetails, BuildContext context) {
    Navigator.of(context).pop({'agentDetails': agentDetails});
  }

}
