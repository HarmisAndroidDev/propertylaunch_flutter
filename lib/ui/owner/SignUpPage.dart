import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:property_launch/model/login/LoginMaster.dart';
import 'package:property_launch/ui/customUi/BlueBorderButton.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/ui/customUi/RoundCornerdTextField.dart';
import 'package:property_launch/ui/customUi/SolidButton.dart';
import 'package:property_launch/ui/owner/home.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:http/http.dart' as http;

import '../login.dart';

class SignUp extends StatefulWidget {
  static final String tag = "signup-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SignUpState();
  }
}

class _SignUpState extends State<SignUp> {
  BuildContext mContext;
  final _formKey = GlobalKey<FormState>();
  final mPhoneController = TextEditingController();
  final mPasswordController = TextEditingController();
  final mCPasswordController = TextEditingController();
  final mEmailController = TextEditingController();
  final mNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    CommonUtils.enableStatusBar(CommonColors.primaryColor);
    final scaffold = initViews();
    return scaffold;
  }

  Scaffold initViews() {
    final image = new Container(
        margin: EdgeInsets.all(30.0),
        width: double.infinity,
        height: 150,
        child: new Image.asset("images/img_logo.png"));

    final edtFullName = new RoundCornerTextField(
        margin: EdgeInsets.only(top: 40.0),
        width: double.infinity,
        hintText: AppConstants.fullName,
        borderColor: CommonColors.edt_border_color,
        borderWidth: 1.0,
        inputType: TextInputType.text,
        autoFocus: false,
        maxLines: 1,
        fontWeight: FontWeight.w400,
        hintTextColor: CommonColors.hint_text_color,
        controller: mNameController,
        validator: (value) {
          if (value.isEmpty) {
            return AppConstants.enter_name;
          }
        });

    final edtPhoneNumber = new RoundCornerTextField(
        margin: EdgeInsets.only(top: 20.0),
        width: double.infinity,
        hintText: AppConstants.enter_phone,
        borderColor: CommonColors.edt_border_color,
        borderWidth: 1.0,
        maxLines: 1,
        maxLength: 10,
        inputType: TextInputType.phone,
        autoFocus: false,
        fontWeight: FontWeight.w400,
        hintTextColor: CommonColors.hint_text_color,
        controller: mPhoneController,
        validator: (value) {
          if (value.isEmpty) {
            return AppConstants.enter_phone;
          } else if (!CommonUtils.isValidMobile(value)) {
            return AppConstants.enter_valid_mobile;
          }
        });

    final edtEmail = new RoundCornerTextField(
        margin: EdgeInsets.only(top: 20.0),
        controller: mEmailController,
        width: double.infinity,
        hintText: AppConstants.enter_email,
        borderColor: CommonColors.edt_border_color,
        borderWidth: 1.0,
        maxLines: 1,
        inputType: TextInputType.emailAddress,
        autoFocus: false,
        fontWeight: FontWeight.w400,
        hintTextColor: CommonColors.hint_text_color,
        validator: (value) {
          if (value.isEmpty) {
            return AppConstants.enter_username;
          } else if (!CommonUtils.isvalidEmail(value)) {
            return AppConstants.enter_valid_email;
          }
        });

    final edtPassword = new RoundCornerTextField(
        margin: EdgeInsets.only(top: 20.0),
        width: double.infinity,
        controller: mPasswordController,
        hintText: AppConstants.enter_password,
        borderColor: CommonColors.edt_border_color,
        borderWidth: 1.0,
        inputType: TextInputType.text,
        autoFocus: false,
        isPassword: true,
        maxLines: 1,
        fontWeight: FontWeight.w400,
        hintTextColor: CommonColors.hint_text_color,
        validator: (value) {
          if (value.isEmpty) {
            return AppConstants.enter_password;
          }
        });

    final edtConirmPassword = new RoundCornerTextField(
        margin: EdgeInsets.only(top: 20.0),
        width: double.infinity,
        hintText: AppConstants.confirm_password,
        borderColor: CommonColors.edt_border_color,
        borderWidth: 1.0,
        controller: mCPasswordController,
        inputType: TextInputType.text,
        autoFocus: false,
        isPassword: true,
        maxLines: 1,
        fontWeight: FontWeight.w400,
        hintTextColor: CommonColors.hint_text_color,
        validator: (value) {
          if (value.isEmpty) {
            return AppConstants.enter_confirm_username;
          }
        });

    final btnRegister = new BlueBorderButton(
      margin: EdgeInsets.only(top: 20.0),
      height: AppDimens.btn_height,
      buttonText: AppConstants.register,
      width: double.infinity,
      radious: 5.0,
      backgroundColor: CommonColors.blue_btn_color,
      onPressed: () {
        if (_formKey.currentState.validate()) {

          CommonUtils.isInternetConnected().then((isConnected){
            if(isConnected){
              regApiCall();
            }else{
              CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
            }
          });
        }
      },
    );

    final tvAlready = new CTextView(
      text: AppConstants.already_have_account,
      textSize: 16.0,
      textColor: CommonColors.color_a9a9a9,
      fontWeight: FontWeight.w700,
    );
    final tvLogin = new CTextView(
      text: " " + AppConstants.login,
      textColor: CommonColors.primaryColor,
      textSize: 16.0,
      fontWeight: FontWeight.w700,
    );

    final tvRow = new InkWell(
        onTap: () {
          Navigator.pushReplacementNamed(mContext, LoginPage.tag);
        },
        child: new Container(
            margin: EdgeInsets.only(top: 20.0),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[tvAlready, tvLogin],
            )));

    final formListView = new Container(
        padding: EdgeInsets.only(left: 30.0, right: 30.0, top: 40.0),
        child: new ListView(children: <Widget>[
          image,
          edtFullName,
          edtPhoneNumber,
          edtEmail,
          edtPassword,
          edtConirmPassword,
          btnRegister,
          tvRow
        ]));

    final form = new Form(key: _formKey, child: formListView);

    final scaffold =
        new Scaffold(
            backgroundColor: Colors.white,
            body: new Builder(builder: ((BuildContext context) {
      mContext = context;
      return form;
    })));

    return scaffold;
  }

  regApiCall() async {
    CommonUtils.showProgressDialog(mContext);
    final response =
        await http.post(ApiUrls.registration, body: getRegParams());
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      CommonUtils.hideProgressDialog(mContext);
      print("Response: " + response.body);
      LoginMaster loginMaster =
          LoginMaster.fromJson(json.decode(response.body));
      if (loginMaster != null) {
        if (loginMaster.success == 1) {
          AppSettings appSettings = new AppSettings();
          appSettings.setLoginDetails(json.encode(loginMaster.result.toJson()));
          Navigator.pushNamed(mContext, HomePage.tag);
        //  Navigator.pop(mContext);
        } else {

          CommonUtils.showErrorMessage(mContext, loginMaster.message);
          Navigator.pushNamed(mContext, HomePage.tag);
        //  Navigator.pop(mContext);
        }
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showErrorMessage(mContext, "" + AppConstants.oops);
      throw Exception('Exception');
    }
  }

  String getRegParams() {
    var map = new Map<String, dynamic>();
    map[ApiParams.email] = mEmailController.text;
    map[ApiParams.fullName] = mNameController.text;
    map[ApiParams.password] = mPasswordController.text;
    map[ApiParams.phone] = mPhoneController.text;
    map[ApiParams.uType] = "2";
    if (Platform.isAndroid) {
      map[ApiParams.deviceAccess] = AppConstants.DEVICE_ACCESS_ANDROID;
    } else {
      map[ApiParams.deviceAccess] = AppConstants.DEVICE_ACCESS_IOS;
    }
    map[ApiParams.deviceToken] = "123456";
    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }
}
