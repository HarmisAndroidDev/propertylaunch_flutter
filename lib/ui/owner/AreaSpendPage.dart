import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:property_launch/arguments/PropertyDetailArguments.dart';
import 'package:property_launch/arguments/VideoStyleDetailArguments.dart';
import 'package:property_launch/model/MessageMaster.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/model/priceList/PriceDetails.dart';
import 'package:property_launch/model/priceList/PriceMaster.dart';
import 'package:property_launch/model/videoStyleList/VideoStyleDetails.dart';
import 'package:property_launch/ui/ThankYouPage.dart';
import 'package:property_launch/ui/customUi/BlueBorderButton.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:video_player/video_player.dart';
import 'package:http/http.dart' as http;

import 'home.dart';

class AreaSpendPage extends StatefulWidget {
  static String tag = "area-spend-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return _AreaSpendPageState();
  }
}

class _AreaSpendPageState extends State<AreaSpendPage> {
  Timer timer;
  Set<Marker> markerSet = new Set();
  static CameraPosition _initialCamera = CameraPosition(
    target: LatLng(23.023854, 72.538471),
    zoom: 16.0,
  );

  static LatLng propertyLatLong = new LatLng(23.023854, 72.538471);
  static String markerId = "102";

  Completer<GoogleMapController> _gController = Completer();
  GoogleMapController _mapController;
  GoogleMap googleMap;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Marker propertyMarker = new Marker(
      markerId: new MarkerId(markerId),
      position: propertyLatLong,
      infoWindow: InfoWindow(title: "Property Location", snippet: ""),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));

  PriceDetails dropDownvalue;
  BuildContext mContext;
  List<PriceDetails> priceDetailsList = <PriceDetails>[];
  bool isListAvailable = true;
  String selectedPrice = "\$100";
  String currencySymbol = "";
  OwnerPropertyDetails ownerPropertyDetails;
  VideoStyleListDetails videoStyleListDetails;

  LoginDetails loginDetails;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppSettings appSetting = new AppSettings();
    appSetting.getCurrencySymbol().then((value) {
      currencySymbol = value;
    });

    appSetting.getLoginDetails().then((value) {
      loginDetails = LoginDetails.fromJson(json.decode(value));;
    });

    Future.delayed(Duration.zero, () {
      StripeSource.setPublishableKey(AppConstants.PUBLISH_KEY_STRIPE);

      CommonUtils.isInternetConnected().then((isConnected){
        if(isConnected){
          getPriceLisApiCall();
        }else{
          CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    receiveArguements(context);
    return initViews();
  }

  void receiveArguements(BuildContext context) {
    final VideoStyleDetailArguments propertyDetailsArguments =
        ModalRoute.of(context).settings.arguments;
    ownerPropertyDetails = propertyDetailsArguments.ownerPropertyDetails;
    videoStyleListDetails=propertyDetailsArguments.videoStyleListDetails;

    setMapValues();
  }

  void setMapValues(){
    double lat = ownerPropertyDetails.lat == ""
        ? 0.0
        : double.parse(ownerPropertyDetails.lat);
    double long = ownerPropertyDetails.long == ""
        ? 0.0
        : double.parse(ownerPropertyDetails.long);

    LatLng propertyLatLong = new LatLng(lat, long);
    _initialCamera = CameraPosition(
      target: propertyLatLong,
      zoom: 16.0,
    );
    propertyMarker = new Marker(
        markerId: new MarkerId(markerId),
        position: propertyLatLong,
        infoWindow: InfoWindow(
            title: ownerPropertyDetails.proName,
            snippet: ownerPropertyDetails.propertyInfo),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));
    markers[new MarkerId(markerId)] = propertyMarker;
  }


  Widget initViews() {
    final line = new Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0),
      width: 60,
      height: 1,
      color: CommonColors.primaryColor,
    );

    final tvChooseSpend = new CTextView(
      textAlign: TextAlign.center,
      text: AppConstants.choose_spend,
      textSize: 18.0,
      textColor: CommonColors.primaryColor,
      fontWeight: FontWeight.w500,
    );

    final topRow = new Align(
        alignment: Alignment.center,
        child: new Container(
            margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[line, tvChooseSpend, line],
            )));

    final tvAmount = new Align(
        alignment: Alignment.center,
        child: new Material(
          elevation: 2.0,
          child: new Container(
              height: 40,
              decoration: BoxDecoration(
                  color: CommonColors.btnBlueLight,
                  borderRadius: BorderRadius.all(Radius.circular(2.0)),
                  border: Border.all(
                      style: BorderStyle.solid,
                      color: CommonColors.white_80,
                      width: 1)),
              width: 155,
              alignment: Alignment.center,
              child: (new CTextView(
                backgroundColor: Colors.transparent,
                text: currencySymbol +
                    (dropDownvalue == null ? "0" : dropDownvalue.price),
                textColor: CommonColors.primaryColor,
                textSize: 16,
              ))),
        ));

    final dropDown = new Container(
        width: double.infinity,
        child: DropdownButton<PriceDetails>(
          hint: CTextView(
            text: AppConstants.select_price,
            textSize: 16,
            textColor: CommonColors.text_color,
            fontWeight: FontWeight.w500,
            backgroundColor: Colors.white,
          ),
          value: dropDownvalue,
          icon: new Container(),
          onChanged: (PriceDetails newValue) {
            setState(() {
              dropDownvalue = newValue;
            });
          },
          items: priceDetailsList
              .map<DropdownMenuItem<PriceDetails>>((PriceDetails value) {
            return DropdownMenuItem<PriceDetails>(
              value: value,
              child: CTextView(
                text: value.name,
                textSize: 16,
                textColor: CommonColors.text_color,
                fontWeight: FontWeight.w500,
                backgroundColor: Colors.transparent,
              ),
            );
          }).toList(),
        ));

    final dropIcon = new Align(
        alignment: Alignment.centerRight,
        child: RotatedBox(
          quarterTurns: 1,
          child: new Icon(
            Icons.keyboard_arrow_right,
            color: CommonColors.text_color,
          ),
        ));

    final stackDrop = new Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 20.0),
        child: new Stack(
          alignment: Alignment.center,
          children: <Widget>[dropDown, dropIcon],
        ));

    final btnNext = new Align(
        alignment: Alignment.topRight,
        child: new Container(
            margin: EdgeInsets.only(top: 20.0),
            width: 100,
            height: AppDimens.btn_height,
            child: new BlueBorderButton(
              height: AppDimens.btn_height,
              buttonText: AppConstants.pay,
              width: double.infinity,
              radious: 2.0,
              backgroundColor: CommonColors.blue_btn_color,
              onPressed: () {
                _getStripeToken();
              },
            )));

    final tvVideoDetails = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.justify,
          text: videoStyleListDetails.description,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    googleMap = GoogleMap(
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
        ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer())),
      mapType: MapType.normal,
      zoomGesturesEnabled: true,
      markers: Set<Marker>.of(markers.values),
      minMaxZoomPreference: MinMaxZoomPreference.unbounded,
      initialCameraPosition: _initialCamera,
      onMapCreated: (GoogleMapController googleMapController) {
        print("Map Created");
        _mapController = googleMapController;
        _gController.complete(googleMapController);
      },
    );

    final mapView = new Container(
        height: 200,
        width: double.infinity,
        margin: EdgeInsets.only(top: 20.0),
        child: new ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            child: googleMap));

    final mainListview = new Container(
        color: Colors.white,
        child: new ListView(
          padding: EdgeInsets.all(15.0),
          children: <Widget>[
            topRow,
            tvAmount,
            stackDrop,
            btnNext,
            tvVideoDetails,
            mapView
          ],
        ));

    return new Scaffold(
      appBar: AppBar(
        title: CTextView(
          text: AppConstants.video_style,
          textSize: AppDimens.appBarTitleSize,
          backgroundColor: Colors.transparent,
          textColor: Colors.white,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context, false),
          color: Colors.white,
        ),
        backgroundColor: CommonColors.primaryColor,
        automaticallyImplyLeading: true,
      ),
      body: Builder(builder: (BuildContext context) {
        mContext = context;
        return mainListview;
      }),
    );
  }

  _getStripeToken() {
    StripeSource.addSource().then((String token) {
      print(" Stripe Token"+token);
      if (token != null) {
        CommonUtils.isInternetConnected().then((isConnected){
          if(isConnected){
            paymentApiCall(token);
          }else{
            CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
          }
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  getPriceLisApiCall() async {
    CommonUtils.showProgressDialog(mContext);
    final response = await http.get(ApiUrls.getPriceList);
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      CommonUtils.hideProgressDialog(mContext);
      print("Response: " + response.body);
      PriceMaster master = PriceMaster.fromJson(json.decode(response.body));
      if (master != null) {
        if (master.result.length > 0) {
          setState(() {
            priceDetailsList.addAll(master.result);
            dropDownvalue = priceDetailsList[0];
          });
        } else {
          _showEmptyLayout();
        }
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showErrorMessage(mContext, AppConstants.oops);
      _showEmptyLayout();
      throw Exception('Exception');
    }
  }

  void _showEmptyLayout() {
    setState(() {
      isListAvailable = false;
    });
  }



  paymentApiCall(String token) async {
    CommonUtils.showProgressDialog(mContext);
    final response =
        await http.post(ApiUrls.makePayment, body: getPaymentParams(token));
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      CommonUtils.hideProgressDialog(mContext);
      print("Response: " + response.body);
      MessageMaster loginMaster =
          MessageMaster.fromJson(json.decode(response.body));
      if (loginMaster != null) {
        if (loginMaster.success == 1) {
       //   Navigator.of(context, rootNavigator: true).pop('dialog');
        //  Navigator.pushReplacementNamed(mContext, HomePage.tag);
          Navigator.of(context)
              .pushNamedAndRemoveUntil(ThankYouPage.tag, (Route<dynamic> route) => false);
          CommonUtils.showErrorMessage(mContext, loginMaster.message);
        } else {

          CommonUtils.showErrorMessage(mContext, loginMaster.message);
        }
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showErrorMessage(mContext, "" + AppConstants.oops);
      throw Exception('Exception');
    }
  }

  String getPaymentParams(String token) {
    var map = new Map<String, dynamic>();
    map[ApiParams.token] = token;
    map[ApiParams.ownerid] = ownerPropertyDetails.ownerId;
    map[ApiParams.amount] = dropDownvalue.price;
    map[ApiParams.propertyid] = ownerPropertyDetails.propertyId;
    map[ApiParams.email] = loginDetails.email;

    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }
}
