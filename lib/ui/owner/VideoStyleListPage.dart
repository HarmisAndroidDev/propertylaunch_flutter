import 'dart:convert';

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:property_launch/arguments/PropertyDetailArguments.dart';
import 'package:property_launch/arguments/VideoStyleDetailArguments.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/model/videoStyleList/VideoStyleDetails.dart';
import 'package:property_launch/model/videoStyleList/VideoStyleListMaster.dart';
import 'package:property_launch/ui/VideoStyleInfo.dart';
import 'package:property_launch/ui/customUi/BlueBorderButton.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/ui/customUi/MyCustomDialog.dart';
import 'package:property_launch/ui/customUi/ProgressImageView.dart';
import 'package:property_launch/ui/owner/AreaSpendPage.dart';
import 'package:property_launch/ui/owner/InfoAboutYourVideoPage.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:http/http.dart' as http;
import 'package:video_player/video_player.dart';

class VideoStyleListPage extends StatefulWidget {
  static String tag = "video-style-list-stage";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _VideoStyleListPageState();
  }
}

class _VideoStyleListPageState extends State<VideoStyleListPage> {
  List<VideoStyleListDetails> videoList = new List();
  int preselected = -1;
  bool isListAvailable = true;
  BuildContext mContext;
  OwnerPropertyDetails ownerPropertyDetails;
  bool isVideoselected= false;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      CommonUtils.isInternetConnected().then((isConnected){
        if(isConnected){
          getPropertyLisApiCall();
        }else{
          CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    receiveArguements(context);
    return initViews();
  }

  void receiveArguements(BuildContext context) {
    final PropertyDetailsArguments propertyDetailsArguments =
        ModalRoute.of(context).settings.arguments;
    ownerPropertyDetails = propertyDetailsArguments.ownerPropertyDetails;
  }

  Widget initViews() {
    final itemGridDelegate = new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 0.0,
        mainAxisSpacing: 5.0,
        crossAxisCount: 2,
        childAspectRatio: 1.0);

    final videoStyleGridView = new Container(
      child: new Container(
        color: Colors.white,
        child: GridView.builder(
          physics: ClampingScrollPhysics(),
          gridDelegate: itemGridDelegate,
          shrinkWrap: true,
          itemCount: videoList.length,
          itemBuilder: (BuildContext context, int index) {
            return new GestureDetector(
              child: videoBuildItems(index, context),
              onTap: () {
                // print("play video");
                showVideoDialog(videoList[index].sampleVideo);
              },
              onLongPress: () {
                setState(() {
                  if (preselected != -1) {
                    videoList[preselected].isSelected = false;
                  }
                  videoList[index].isSelected = true;
                  preselected = index;
                  isVideoselected=true;
                });
                print("Select video");
              },
            );
          },
        ),
      ),
    );

    final btnVideoAd = new Align(
        alignment: Alignment.center,
        child: new InkWell(
            onTap: () {
               if(isVideoselected){
                 Navigator.pushNamed(mContext, InfoAboutYourVideoPage.tag,
                     arguments: VideoStyleDetailArguments(videoList[preselected],ownerPropertyDetails));
               }else{
                 CommonUtils.showErrorMessage(mContext, AppConstants.please_video_style);
               }
            },
            child: new BlueBorderButton(
              margin: EdgeInsets.only(top: 5.0, right: 5.0),
              height: AppDimens.btn_height,
              buttonText: AppConstants.please_video_style,
              width: double.infinity,
              radious: 5.0,
              backgroundColor: CommonColors.blue_btn_color,
            )));

    final mainLisView = new ListView(
      shrinkWrap: true,
      padding: EdgeInsets.all(10.0),
      children: <Widget>[videoStyleGridView, btnVideoAd],
    );

    final scaffold = new Scaffold(
      appBar: AppBar(
        title: CTextView(
          backgroundColor: Colors.transparent,
          textSize: 20.0,
          text: AppConstants.video_style,
          textColor: Colors.white,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context, false),
          color: Colors.white,
        ),
        backgroundColor: CommonColors.primaryColor,
        automaticallyImplyLeading: true,
      ),
      body: Builder(builder: (BuildContext context) {
        mContext = context;
        return mainLisView;
      }),
    );

    return scaffold;
  }

  Widget videoBuildItems(int index, BuildContext context) {
    final imageView = new Container(
        foregroundDecoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            color: Colors.black54),
        child: new ProgressImageView(
          isCircule: false,
          borderRadius: 20.0,
          height: 160,
          width: 160,
          imageurl: ApiUrls.dummyPropertyImage,
        ));
    final playButton = new Container(
        height: 160,
        width: 160,
        padding: EdgeInsets.all(10.0),
        child: new Align(
          alignment: Alignment.center,
          child: new Icon(
            Icons.play_arrow,
            color: Colors.white,
            size: videoList[index].isSelected ? 0 : 80,
          ),
        ));
    final tickButton = new Container(
        height: 160,
        width: 160,
        padding: EdgeInsets.all(10.0),
        child: new Align(
          alignment: Alignment.center,
          child: new Icon(
            Icons.check_circle_outline,
            color: Colors.white,
            size: videoList[index].isSelected ? 80 : 0,
          ),
        ));

    final stackImage = new Stack(
      children: <Widget>[imageView, playButton, tickButton],
    );

    return stackImage;
  }

  void getPropertyLisApiCall() async {
    CommonUtils.showProgressDialog(mContext);
    final response = await http.get(ApiUrls.getVideoStyleList);
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      CommonUtils.hideProgressDialog(mContext);
      print("Response: " + response.body);
      VideoStyleListMaster master =
          VideoStyleListMaster.fromJson(json.decode(response.body));
      if (master != null) {
        if (master.result.length > 0) {
          setState(() {
            videoList.addAll(master.result);
          });
        } else {
          _showEmptyLayout();
        }
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showErrorMessage(mContext, AppConstants.oops);
      _showEmptyLayout();

      throw Exception('Exception');
    }
  }

  void _showEmptyLayout() {
    setState(() {
      isListAvailable = false;
    });
  }

  void showVideoDialog(String videoUrl) {
    final videoPlayerController = VideoPlayerController.network(videoUrl);

    final chewieController = ChewieController(
        videoPlayerController: videoPlayerController,
        aspectRatio: 3 / 2,
        autoPlay: true,
        looping: false,
        showControls: true);

    final playerWidget = Chewie(
      controller: chewieController,
    );

    final videoView = new Container(
        height: 250,
        child: new ClipRRect(
          clipBehavior: Clip.hardEdge,
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          child: new FittedBox(
            child: new Container(
                padding: EdgeInsets.all(5.0),
                color: Colors.black,
                child: playerWidget),
            fit: BoxFit.cover,
          ),
        ));

    MyCustomDialog changePasswordDialog = MyCustomDialog(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      //this right here
      child: new Container(
          margin: EdgeInsets.only(left: 20, right: 20), child: videoView),
    );

    /*showDialog(
        context: context,
        builder: (BuildContext context) {
          return changePasswordDialog;
        });*/

    showMyDialog(
        context: mContext,
        builder: (BuildContext context) {
          return changePasswordDialog;
        });
  }
}
