import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'dart:async' show Future;
import 'package:http/http.dart' as http;

import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:property_launch/model/login/LoginMaster.dart';
import 'package:property_launch/ui/customUi/BlueBorderButton.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/ui/customUi/CirculeImageView.dart';
import 'package:property_launch/ui/customUi/RoundCornerdTextField.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:property_launch/utils/ImagePath.dart';

class OwnerProfilePage extends StatefulWidget {
  static final String tag = 'owner-profile-page';

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OwnerProfilePageState();
  }
}

class _OwnerProfilePageState extends State<OwnerProfilePage> {
  BuildContext mContext;
  bool isLoading = false;

  final mOldPasswordController = TextEditingController();
  final mNewPasswordController = TextEditingController();
  final mNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  LoginDetails loginDetails;
  String imagePath = "";
  String username = "", email = "";
  bool isEdit = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    AppSettings appSettings = new AppSettings();
    appSettings.getLoginDetails().then((onValue) {
      setState(() {
        loginDetails = LoginDetails.fromJson(json.decode(onValue));
        imagePath = loginDetails.profileImage;
        mNameController.text = loginDetails.name;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (loginDetails != null) {
      print("name = = = = " + loginDetails.name);
    }
    return _initViews();
  }

  Widget _initViews() {
    final header = new Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Stack(
          fit: StackFit.passthrough,
          children: <Widget>[
            Material(
              elevation: 5.0,
              clipBehavior: Clip.antiAlias,
              shape: CircleBorder(
                  side: BorderSide(
                      color: Colors.white,
                      width: 10.0,
                      style: BorderStyle.solid)),
              child: Align(
                  alignment: Alignment.center,
                  child: new CircleImageView(
                    height: 150,
                    width: 150,
                    imageUrl: imagePath == "" ? "" : imagePath,
                    borderWidth: 0,
                    borderColor: CommonColors.white_80,
                  )),
            ),
            Align(
                alignment: Alignment.center,
                child: new InkWell(
                  onTap: () {
                    _selectImageOption(mContext);
                  },
                  child: Container(
                    height: 50,
                    width: 50,
                    margin: EdgeInsets.only(
                      left: 100,
                    ),
                    child: new Image.asset(LocalImages.ic_camera,
                        fit: BoxFit.fill),
                  ),
                )),
          ],
        ));

    final tvName = Container(
        padding: EdgeInsets.only(top: 20.0, left: 12, right: 12),
        alignment: Alignment.center,
        child: Text(
          loginDetails == null ? "" : loginDetails.name,
          style: TextStyle(
              fontFamily: AppConstants.FONT_FAMILY,
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: CommonColors.color_363636),
        ));

    final edtName = new RoundCornerTextField(
        margin: EdgeInsets.only(top: 20.0),
        width: double.infinity,
        hintText: AppConstants.enter_name,
        borderColor: CommonColors.edt_border_color,
        borderWidth: 1.0,
        controller: mNameController,
        inputType: TextInputType.text,
        autoFocus: false,
        isEnabled: true,
        fontWeight: FontWeight.w400,
        hintTextColor: CommonColors.hint_text_color,
        validator: (value) {
          if (value.isEmpty) {
            return AppConstants.enter_name;
          }
        });

    final btnEdit = new Align(
        alignment: Alignment.center,
        child: new Container(
            width: 100,
            child: new BlueBorderButton(
              margin: EdgeInsets.only(top: 20.0, bottom: 10.0),
              height: 30,
              buttonText: AppConstants.edit,
              width: double.infinity,
              radious: 5.0,
              fontsize: 14.0,
              fontWeight: FontWeight.w400,
              fontColor: isEdit ? Colors.white : CommonColors.text_color,
              borderColor: CommonColors.hint_text_color,
              backgroundColor:
                  isEdit ? CommonColors.primaryColor : CommonColors.white_80,
              onPressed: () {
                setState(() {
                  isEdit = !isEdit;
                });
              },
            )));

    final line = Container(
      height: 1,
      color: CommonColors.color_a9a9a9,
    );

    final layoutMobile = Container(
        margin: EdgeInsets.only(top: 20.0, bottom: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: Padding(
                  padding: EdgeInsets.all(0),
                  child: Text(AppConstants.mobile_number,
                      style: TextStyle(
                          fontFamily: AppConstants.FONT_FAMILY,
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: CommonColors.color_363636))),
              flex: 1,
            ),
            Flexible(
              child: Padding(
                padding: EdgeInsets.all(0),
                child: Text(
                    loginDetails == null
                        ? "NA"
                        : (loginDetails.phone == null
                            ? "NA"
                            : loginDetails.phone),
                    style: TextStyle(
                        fontFamily: AppConstants.FONT_FAMILY,
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: CommonColors.black)),
              ),
              flex: 1,
            ),
          ],
        ));

    final layoutEmail = new Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Flexible(
          child: Padding(
            padding: EdgeInsets.all(0),
            child: Text(AppConstants.email,
                style: TextStyle(
                    fontFamily: AppConstants.FONT_FAMILY,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: CommonColors.black)),
          ),
          flex: 1,
        ),
        Flexible(
          child: Padding(
            padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
            child: Text(loginDetails == null ? "" : loginDetails.email,
                style: TextStyle(
                    fontFamily: AppConstants.FONT_FAMILY,
                    fontSize: 16,
                    fontWeight: FontWeight.w300,
                    color: CommonColors.black)),
          ),
          flex: 1,
        ),
      ],
    ));
    final tvChangePassword = new Container(
        margin: EdgeInsets.only(top: 20.0),
        child: new Align(
            alignment: Alignment.centerLeft,
            child: new CTextView(
              maxLines: 1,
              textAlign: TextAlign.start,
              text: AppConstants.change_password,
              textSize: 16.0,
              textColor: CommonColors.black,
              fontWeight: FontWeight.w700,
            )));

    final edtOldPassword = new RoundCornerTextField(
        margin: EdgeInsets.only(top: 20.0),
        width: double.infinity,
        controller: mOldPasswordController,
        hintText: AppConstants.old_password,
        borderColor: CommonColors.edt_border_color,
        borderWidth: 1.0,
        inputType: TextInputType.text,
        autoFocus: false,
        isPassword: true,
        isEnabled: isEdit,
        fontWeight: FontWeight.w400,
        hintTextColor: CommonColors.hint_text_color,
        validator: (value) {
          if (value.isEmpty) {
            return AppConstants.old_password;
          }
        });

    final edtNewPassword = new RoundCornerTextField(
        margin: EdgeInsets.only(top: 20.0),
        width: double.infinity,
        hintText: AppConstants.new_password,
        borderColor: CommonColors.edt_border_color,
        borderWidth: 1.0,
        controller: mNewPasswordController,
        inputType: TextInputType.text,
        autoFocus: false,
        isPassword: true,
        isEnabled: isEdit,
        fontWeight: FontWeight.w400,
        hintTextColor: CommonColors.hint_text_color,
        validator: (value) {
          if (value.isEmpty) {
            return AppConstants.enter_confirm_username;
          }
        });

    final btnSubmit = new BlueBorderButton(
      margin: EdgeInsets.only(top: 20.0),
      height: AppDimens.btn_height,
      buttonText: AppConstants.submit,
      width: double.infinity,
      radious: 5.0,
      backgroundColor: CommonColors.blue_btn_color,
      onPressed: () {
        print(mOldPasswordController.text);
        if (mOldPasswordController.text != "" ||
            mNewPasswordController.text != "") {
          if (_formKey.currentState.validate()) {
            CommonUtils.isInternetConnected().then((isConnected) {
              if (isConnected) {
                editProfileImageApi(imagePath);
              } else {
                CommonUtils.showErrorMessage(
                    mContext, AppConstants.no_internet);
              }
            });
          }
        } else if (mNameController.text != "") {
          CommonUtils.isInternetConnected().then((isConnected) {
            if (isConnected) {
              editProfileImageApi(imagePath);
            } else {
              CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
            }
          });
        }
      },
    );

    final mainListView = ListView(
      shrinkWrap: true,
      padding:
          EdgeInsets.only(top: 10.0, bottom: 10.0, left: 15.0, right: 15.0),
      children: <Widget>[
        header,
        isEdit ? edtName : tvName,
        btnEdit,
        layoutMobile,
        line,
        layoutEmail,
        line,
        tvChangePassword,
        edtOldPassword,
        edtNewPassword,
        btnSubmit
      ],
    );

    Form form = new Form(
      child: mainListView,
      key: _formKey,
    );

    final scafold = Scaffold(
      body: Builder(builder: (BuildContext context) {
        mContext = context;
        return form;
      }),
    );

    final willPopScope = WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: scafold);

    return willPopScope;
  }

  void _selectImageOption(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.camera),
                    title: new Text(AppConstants.text_camera),
                    onTap: () {
                      _getImageFromDevice(false);
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.attach_file),
                  title: new Text(AppConstants.text_gallery),
                  onTap: () {
                    _getImageFromDevice(true);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  Future _getImageFromDevice(bool isGallery) async {
    var galleryFile = await ImagePicker.pickImage(
        source: isGallery ? ImageSource.gallery : ImageSource.camera);
    if (galleryFile != null) {
      setState(() {
        imagePath = galleryFile.path;
      });
      print("Image path: " + galleryFile.path);

      CommonUtils.isInternetConnected().then((isConnected) {
        if (isConnected) {
          editProfileImageApi(galleryFile.path);
          CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
        }
      });
    }
  }

  /*Edit Profile Image Api*/
  editProfileImageApi(String imagePath) async {
    CommonUtils.showProgressDialog(mContext);
    final request = await http.MultipartRequest(
        AppConstants.METHOD_POST, Uri.parse(ApiUrls.edit_owner_profile));
    request.fields[ApiParams.json_content] = getJsonParams();

    if (!imagePath.startsWith("http") && imagePath != "") {
      try {
        File imageFile = new File(imagePath);
        var stream =
            new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
        var length = await imageFile.length();
        var multipartFile = new http.MultipartFile(
            ApiParams.image, stream, length,
            filename: basename(imageFile.path));
        request.files.add(multipartFile);
      } catch (e) {
        print("error: " + e);
      }
    }

    request.send().then((response) {
      CommonUtils.hideProgressDialog(mContext);
      print("URL: " + response.request.url.toString());
      try {
        response.stream.transform(utf8.decoder).listen((value) {
          print("Response: " + value);
          if (response.statusCode == 200) {
            LoginMaster loginMaster = LoginMaster.fromJson(json.decode(value));
            if (loginMaster.success == 1) {
              AppSettings appSettings = new AppSettings();
              appSettings
                  .setLoginDetails(json.encode(loginMaster.result.toJson()));

              appSettings.getLoginDetails().then((onValue) {
                setState(() {
                  loginDetails = LoginDetails.fromJson(json.decode(onValue));
                  imagePath = loginDetails.profileImage;
                  isEdit = false;
                });
              });

              CommonUtils.showErrorMessage(
                  mContext, AppConstants.profile_updated);
            } else {
              CommonUtils.showErrorMessage(mContext, loginMaster.message);
            }
          } else {
            CommonUtils.showErrorMessage(mContext, AppConstants.oops);
          }
        });
      } catch (e) {
        CommonUtils.showErrorMessage(mContext, AppConstants.oops);
        print("Error: " + e);
      }
    });
  }

  String getJsonParams() {
    var map = new Map<String, dynamic>();
    map[ApiParams.userId] = loginDetails.userId;
    if (mNewPasswordController.text != "" &&
        mOldPasswordController.text != "") {
      map[ApiParams.password] = mNewPasswordController.text;
      map[ApiParams.oldPassword] = mOldPasswordController.text;
    }
    if (mNameController.text != "") {
      map[ApiParams.name] = mNameController.text;
    }
    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }
}
