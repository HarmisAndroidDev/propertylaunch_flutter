import 'package:flutter/material.dart';
import 'package:property_launch/utils/CommonColors.dart';

class HomeFragment extends StatefulWidget {
  @override
  _HomeFragmentState createState() => _HomeFragmentState();
}

class _HomeFragmentState extends State<HomeFragment> {
  @override
  Widget build(BuildContext context) {
    print('Home fragment');
    return new Center(
      child: new Container(child: new Text("Home Fragment", style: new TextStyle(color: CommonColors.primaryColor),)),
    );
  }
}
