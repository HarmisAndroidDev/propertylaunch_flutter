import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:property_launch/arguments/PropertyDetailArguments.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:property_launch/model/ownerPropertyList/ImageDetails.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/ui/VideoStyleInfo.dart';
import 'package:property_launch/ui/agent/AgentEditPropertyPage.dart';
import 'package:property_launch/ui/customUi/BlueBorderButton.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/ui/customUi/ProgressImageView.dart';
import 'package:property_launch/ui/owner/OwnerEditPropertyPage.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:property_launch/utils/CustomBoxShadow.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:property_launch/utils/ImagePath.dart';

class PropertyDetailPage extends StatefulWidget {
  static String tag = "property-detail-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PropertyDetailPageState();
  }
}

class _PropertyDetailPageState extends State<PropertyDetailPage> {
  BuildContext mContext;
  Set<Marker> markerSet = new Set();
  static CameraPosition _initialCamera;
  static String markerId = "102";
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _mapController;
  GoogleMap googleMap;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  OwnerPropertyDetails _ownerPropertyDetails;
  Marker propertyMarker;
  bool isFirstTime = true;
  LoginDetails _loginDetails;
  String currencySymbol = "";
  bool isAdded = false;
  String userType=AppConstants.userTypeOwner;
  bool isBtnAdVisible = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppSettings appSettings = new AppSettings();

    appSettings.getLoginDetails().then((onValue) {
      _loginDetails = LoginDetails.fromJson(json.decode(onValue));
      setState(() {
        userType=_loginDetails.userType;
        if(userType == AppConstants.userTypeAgent){
          isBtnAdVisible=false;
        }
      });
    });

    appSettings.getCurrencySymbol().then((onValue) {
      setState(() {
        currencySymbol = onValue;
      });
    });
    //  requestPermission();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (isFirstTime) {
      receiveArguements(context);
      isFirstTime = false;
    }
    return _initViews();
  }

  void receiveArguements(BuildContext context) {
    final PropertyDetailsArguments propertyDetailsArguments =
        ModalRoute
            .of(context)
            .settings
            .arguments;
    _ownerPropertyDetails = propertyDetailsArguments.ownerPropertyDetails;

    double lat = _ownerPropertyDetails.lat == ""
        ? 0.0
        : double.parse(_ownerPropertyDetails.lat);
    double long = _ownerPropertyDetails.long == ""
        ? 0.0
        : double.parse(_ownerPropertyDetails.long);

    LatLng propertyLatLong = new LatLng(lat, long);
    _initialCamera = CameraPosition(
      target: propertyLatLong,
      zoom: 16.0,
    );
    propertyMarker = new Marker(
        markerId: new MarkerId(markerId),
        position: propertyLatLong,
        infoWindow: InfoWindow(
            title: _ownerPropertyDetails.proName,
            snippet: _ownerPropertyDetails.propertyInfo),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));

    markers[new MarkerId(markerId)] = propertyMarker;
  }

  Widget _initViews() {
    final ivEdit = new InkWell(
        onTap: () {
         /* if(_loginDetails.userType == _ownerPropertyDetails.addBy){
            print("userType"+_loginDetails.userType+"=="+_ownerPropertyDetails.addBy);*/
            redirectToAgentList();
         /* }else{
            CommonUtils.showErrorMessage(mContext, AppConstants.you_cant_change);
          }*/
        },
        child: Container(
          alignment: Alignment.center,
          height: 30,
          width: 30,
          padding: EdgeInsets.all(3),
          margin: EdgeInsets.all(12),
          child: Image.asset(
            LocalImages.ic_edit,
            color: Colors.white,
          ),
        ));

    final scaffold = new Scaffold(
      appBar: AppBar(
        actions: <Widget>[ivEdit],
        title: new Align(
            alignment: Alignment.center,
            child: CTextView(
              text: _ownerPropertyDetails != null
                  ? currencySymbol + _ownerPropertyDetails.estimatedSalePrice
                  : currencySymbol + "000",
              textSize: AppDimens.appBarTitleSize,
              textColor: Colors.white,
              backgroundColor: Colors.transparent,
              fontWeight: FontWeight.w500,
            )),
        automaticallyImplyLeading: true,
        backgroundColor: CommonColors.primaryColor,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () {
            backToDetail(_ownerPropertyDetails, mContext);
          },
          color: Colors.white,
        ),
      ),
      backgroundColor: Colors.white,
      body: new Builder(builder: (BuildContext context) {
        mContext = context;
        return buildItems();
      }),
    );

    return scaffold;
  }

  Widget buildItems() {
    final ivPropertyImage = new Align(
        child: new ProgressImageView(
          isCircule: false,
          borderRadius: 20.0,
          imageurl: _ownerPropertyDetails.proImg,
          height: 200,
          width: double.infinity,
        ));
    final propertyThumbnailList = new ListView.builder(
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: _ownerPropertyDetails.propertyImage.length,
        itemBuilder: (context, index) {
          return new GestureDetector(
            child: propertyThumb(index, _ownerPropertyDetails.propertyImage),
            onTap: () {
              setState(() {
                _ownerPropertyDetails.proImg =
                    _ownerPropertyDetails.propertyImage[index].image;
              });
            },
          );
        });
    final tvPropertyName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: _ownerPropertyDetails.proName,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final tvPropertyAddress = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 2,
          margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
          text: _ownerPropertyDetails.proAddress,
          textSize: 16.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w500,
        ));

    final lblUnit = new Container(
        height: 20,
        child:new CTextView(
          text: "Unit",
          textColor: CommonColors.black,
          textSize: 16,
          fontWeight: FontWeight.w600,

        ));

    final ivBad = new Container(
      height: 20,
        width: 20,
        margin: EdgeInsets.only(left: 10.0),
        padding: EdgeInsets.all(2.0),
        child: new Image.asset(
          LocalImages.ic_bad,color: CommonColors.text_color,));

    final tvNoofbad = new Container(
      margin: EdgeInsets.only(left: 5.0),
        height: 20,
        width: 20,
        child: new CTextView(
      text: _ownerPropertyDetails == null ? "0" : _ownerPropertyDetails.bedrooms,
      textColor: CommonColors.text_color,
      textSize: 16,
      fontWeight: FontWeight.w400,

    ));
    final ivBath = new Container(
        height: 20,
        width: 20,
        margin: EdgeInsets.only(left: 10.0),
        padding: EdgeInsets.all(2.0),
        child: new Image.asset(
          LocalImages.ic_bathtub,color: CommonColors.text_color,
        ));

    final tvNoBath = new Container(
        margin: EdgeInsets.only(left: 5.0),
        height: 20,
        width: 20,
        child: new CTextView(
          text: _ownerPropertyDetails == null ? "0" : _ownerPropertyDetails.bathrooms,
          textColor: CommonColors.text_color,
          textSize: 16,
          fontWeight: FontWeight.w400,

        ));

    final ivParking = new Container(
      height: 20,
        width: 20,
        margin: EdgeInsets.only(left: 10.0),
        padding: EdgeInsets.all(2.0),
        child:new Image.asset(
          LocalImages.ic_car,color: CommonColors.text_color,
        ));

    final tvNoParking = new Container(
        margin: EdgeInsets.only(left: 5.0),
        height: 20,
        width: 20,
        child: new CTextView(
          text: _ownerPropertyDetails == null ? "0" : _ownerPropertyDetails.parking,
          textColor: CommonColors.text_color,
          textSize: 16,
          fontWeight: FontWeight.w400,

        ));

    final facilityRow = new Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[lblUnit,ivBad,tvNoofbad,ivBath,tvNoBath,ivParking,tvNoParking],
    );

    final tvSelectHome = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 10.0),
          textAlign: TextAlign.left,
          text: "",
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final btnVideoAd = new BlueBorderButton(
      margin: EdgeInsets.only(top: 20.0),
      height: AppDimens.btn_height,
      buttonText: AppConstants.want_to_do,
      width: double.infinity,
      radious: 5.0,
      backgroundColor: CommonColors.blue_btn_color,
      onPressed: () {
        Navigator.pushNamed(context, VideoStyleInfo.tag,
            arguments: PropertyDetailsArguments(_ownerPropertyDetails, false));
      },
    );
    final agentIconBox = new Container(
      height: 50,
      width: 50,
      margin: EdgeInsets.only(left: 5.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5)),
      child: new Center(
        child: ProgressImageView(
          isCircule: false,
          height: 50,
          width: 50,
          borderRadius: 5.0,
          imageurl: _loginDetails == null ? "" : (_loginDetails.userType ==
              AppConstants.userTypeOwner ? _ownerPropertyDetails.agentImage : _ownerPropertyDetails.ownerImage),
        ),
      ),
    );
    final lblAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: _loginDetails == null ? "" : (_loginDetails.userType ==
              AppConstants.userTypeOwner ? AppConstants.agency_name : AppConstants.owner_name),
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final tvAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 10.0),
          maxLines: 1,
          textAlign: TextAlign.start,
          text: _loginDetails == null ? "" : (_loginDetails.userType ==
              AppConstants.userTypeOwner ? _ownerPropertyDetails.agentName : _ownerPropertyDetails.ownerName),
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final agentDetails = new Container(
        margin: EdgeInsets.only(left: 10.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[lblAgentName, tvAgentName],
        ));
    final layoutAgent = new Container(
      height: 60,
      margin: const EdgeInsets.only(top: 20.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[agentIconBox, agentDetails],
      ),
    );
    final lblPropertyDetails = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_detail,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final tvPropertyDetail = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 20.0, bottom: 5.0),
          text: _ownerPropertyDetails.propertyInfo,
          textSize: 16.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final lblPropertyFeatures = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_features,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));
    final itemGridDelegate = new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 5.0,
        mainAxisSpacing: 5.0,
        crossAxisCount: 2,
        childAspectRatio: 6.0);
    final featuresList = new GridView.builder(
        gridDelegate: itemGridDelegate,
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: _ownerPropertyDetails.feName.length,
        itemBuilder: (context, index) {
          return propertyFeatures(index, _ownerPropertyDetails.feName);
        });
    final divider = new Container(
      width: double.infinity,
      color: CommonColors.text_color,
      height: 1,
      margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
    );
    googleMap = GoogleMap(
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))..add(
            Factory<ScaleGestureRecognizer>(() =>
                ScaleGestureRecognizer()))..add(
            Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))..add(
            Factory<VerticalDragGestureRecognizer>(
                    () => VerticalDragGestureRecognizer())),
      mapType: MapType.normal,
      zoomGesturesEnabled: true,
      myLocationButtonEnabled: true,
      myLocationEnabled: false,
      markers: Set<Marker>.of(markers.values),
      minMaxZoomPreference: MinMaxZoomPreference.unbounded,
      initialCameraPosition: _initialCamera,
      onMapCreated: (GoogleMapController googleMapController) {
        _mapController = googleMapController;
        _controller.complete(googleMapController);
      },
    );
    final mapView = new Container(
        height: 200,
        width: double.infinity,
        margin: EdgeInsets.only(top: 20.0),
        child: new ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            child: googleMap));
    final detailListview = ListView(
      shrinkWrap: true,
      padding:
      EdgeInsets.only(top: 10.0, bottom: 10.0, right: 15.0, left: 15.0),
      children: <Widget>[
        ivPropertyImage,
        new Container(height: 80, child: propertyThumbnailList),
        tvPropertyName,
        tvPropertyAddress,
        facilityRow,
        tvSelectHome,
        isBtnAdVisible ?  btnVideoAd : new Container(),
        layoutAgent,
        lblPropertyDetails,
        tvPropertyDetail,
        divider,
        lblPropertyFeatures,
        featuresList,
        mapView
      ],
    );

    final scaffold = new Scaffold(
      backgroundColor: Colors.white,
      body: Builder(builder: (BuildContext context) {
        mContext = context;
        return detailListview;
      }),
    );

    final onWillPopScope = WillPopScope(
        child: new Scaffold(
          body: scaffold,
        ),
        onWillPop: () {
          backToDetail(_ownerPropertyDetails, mContext);
        });
    return onWillPopScope;
  }

  Future requestPermission() async {
    Map<PermissionGroup, PermissionStatus> permissions =
    await PermissionHandler()
        .requestPermissions([PermissionGroup.location]);
  }

  Future<int> checkPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    return permission.value;
  }

  Widget propertyThumb(int index, List<ImageDetails> propertyImages) {
    final ivPropertyImage = new Align(
        alignment: Alignment.centerLeft,
        child: new ProgressImageView(
          margin: EdgeInsets.only(right: 10.0),
          isCircule: false,
          borderRadius: 10.0,
          imageurl: propertyImages[index].image,
          height: 55,
          width: 80,
        ));

    return ivPropertyImage;
  }

  Widget propertyFeatures(int index, List<String> featuresList) {
    final tvFeatureName = new Container(
        width: 150,
        margin: EdgeInsets.only(top: 0.0),
        child: new CTextView(
          text: featuresList[index],
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w600,
        ));

    final detailsRow = new Container(
        width: double.infinity,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[tvFeatureName],
        ));
    return tvFeatureName;
  }

  redirectToAgentList() {
    Navigator.pushNamed(
        mContext,
        _loginDetails.userType == AppConstants.userTypeOwner
            ? AddEditPropertyPage.tag
            : AgentEditPropertyPage.tag,
        arguments: PropertyDetailsArguments(_ownerPropertyDetails, false))
        .then((value) {
      if (value != null) {
        var result = value as Map;
        if (result.containsKey("propertyDetails") &&
            result.containsKey(AppConstants.isAdded)) {
          print("object received");
          setState(() {
             isAdded = result[AppConstants.isAdded];
            _ownerPropertyDetails = result["propertyDetails"];
       //  print(_ownerPropertyDetails.addBy+"   AAAAAAAAAAAAAAAAAA");
          });
        }
      }
    });
  }

  void backToDetail(OwnerPropertyDetails ownerPropertyDetails,
      BuildContext context) {
    Navigator.of(context).pop({
      'propertyDetails': ownerPropertyDetails,
      AppConstants.isAdded: isAdded
    });
  }
}
