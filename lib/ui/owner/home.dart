import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:property_launch/model/login/DrawerItemsM.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/ui/OwnerList.dart';
import 'package:property_launch/ui/PropertList.dart';
import 'package:property_launch/ui/ThankYouPage.dart';
import 'package:property_launch/ui/agent/AgentAddPropertyPage.dart';
import 'package:property_launch/ui/agent/AgentProfilePage.dart';
import 'package:property_launch/ui/customUi/CirculeImageView.dart';
import 'package:property_launch/ui/login.dart';
import 'package:property_launch/ui/owner/OwnerAddPropertyPage.dart';
import 'package:property_launch/ui/owner/AreaSpendPage.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:property_launch/utils/CustomBoxShadow.dart';
import 'package:property_launch/utils/ImagePath.dart';
import 'package:http/http.dart' as http;

import '../AgentList.dart';
import '../HistoryPropertyDetailPage.dart';
import '../OrderHistory.dart';
import '../VideoStyleInfo.dart';
import 'OwnerEditPropertyPage.dart';
import 'ContactAdminPage.dart';
import 'HomeFragment.dart';
import 'OwnerProfilePage.dart';
import 'VideoStyleListPage.dart';

class HomePage extends StatefulWidget {
  static String tag = 'home-page';

  @override
  _HomePageState createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  HomeFragment homeFragment;
  double leftMargin = 12;
  double rightMargin = 12;
  double topMargin = 8;
  double bottomMargin = 8;
  bool isLoading = false;

  static const homeIndex = 0;
  static const logoutIndex = 1;
  int _selectedDrawerIndex = 1;
  int mChildIndex = -1;
  String mChildImage = LocalImages.ic_place_holder;
  LoginDetails loginDetails;
  AppSettings appSettings;
  String mTitle = AppConstants.home;
  OwnerPropertyDetails _propertyDetails;
  BuildContext _mContext;

  List<DrawerItemsM> drawerItemsList = new List();
  int prePos = 0;

  _HomePageState() {}
  OwnerPropertyDetails ownerPropertyDetails;

  @override
  void initState() {
    super.initState();
    appSettings = new AppSettings();
    appSettings.getLoginDetails().then((value) {
      if (value != null) {
        loginDetails = LoginDetails.fromJson(json.decode(value));
      }
    });

    addDrawerList();
  }

  void addDrawerList() {
    drawerItemsList.add(
        new DrawerItemsM(0, LocalImages.ic_agent, AppConstants.profile, false));
    drawerItemsList.add(new DrawerItemsM(1, LocalImages.ic_my_property_active,
        AppConstants.text_my_property, true));
    drawerItemsList.add(new DrawerItemsM(
        2,
        LocalImages.ic_add_edit_property_inactive,
        AppConstants.text_add_edit_property,
        false));
    drawerItemsList.add(new DrawerItemsM(
        3,
        LocalImages.ic_order_history_inactive,
        AppConstants.text_order_history,
        false));

    drawerItemsList.add(new DrawerItemsM(
        4,
        LocalImages.ic_contact_admin_inactive,
        AppConstants.text_contact_admin,
        false));

    drawerItemsList.add(new DrawerItemsM(
        5, LocalImages.ic_sign_out, AppConstants.logout, false));
  }

  @override
  Widget build(BuildContext context) {
    final tvTitle = Padding(
      padding: EdgeInsets.only(right: 5),
      child: Material(
        color: Colors.transparent,
        child: Text(mTitle,
            style: TextStyle(
                fontFamily: AppConstants.FONT_FAMILY,
                fontSize: 20,
                fontWeight: FontWeight.w400,
                color: Colors.white)),
      ),
    );

    final toolbar = Builder(builder: (BuildContext context) {
      return Row(
        children: <Widget>[
          InkWell(
              onTap: () {
                Scaffold.of(context).openDrawer();
              },
              child: Container(
                alignment: Alignment.center,
                height: 24,
                width: 24,
                margin: EdgeInsets.only(left: 12, right: 0),
                padding: EdgeInsets.all(0),
                child: Image.asset(LocalImages.ic_drawer),
              )),
        ],
      );
    });

    final ivNotification = new InkWell(
        onTap: () {},
        child: Container(
          alignment: Alignment.center,
          height: 30,
          width: 30,
          padding: EdgeInsets.all(3),
          margin: EdgeInsets.all(12),
          child: Image.asset(LocalImages.ic_notification),
        ));

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[ivNotification],
        title: tvTitle,
        leading: toolbar,
        backgroundColor: CommonColors.appBarColor,
      ),
      drawer: new Container(
        width: 200,
        child: Drawer(
          child: new ListView.separated(
              separatorBuilder: ((BuildContext context, int i) {
                return new Container(
                  height: 1,
                  color: CommonColors.hint_text_color,
                );
              }),
              shrinkWrap: true,
              itemCount: drawerItemsList.length,
              itemBuilder: (BuildContext context, int i) {
                return new GestureDetector(
                  child: _buildDrawerItems(i),
                  onTap: () {
                    _closeDrawer(i);
                    if (i == 2 || i == 5) {
                      redirectToPage(i);
                    } else {
                      _getDrawerFragment(i);
                      setState(() {
                        drawerItemsList[_selectedDrawerIndex].isSelected =
                            false;
                        drawerItemsList[i].isSelected = true;
                        mTitle = drawerItemsList[i].name;
                        _selectedDrawerIndex = i;
                      });
                    }
                  },
                );
              }),
        ),
      ), //  body: _getDrawerFragment(_selectedDrawerIndex),
      body: Builder(builder: (BuildContext context) {
        _mContext = context;
        return _getDrawerFragment(_selectedDrawerIndex);
      }),
    );
  }

  _closeDrawer(int index) {
    Navigator.of(context).pop();
  }

  Widget _buildDrawerItems(int i) {
    final layoutHome = Container(
      color: drawerItemsList[i].isSelected
          ? CommonColors.primaryColor
          : Colors.white,
      width: 200,
      height: 80,
      child: new Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5.0))),
              alignment: Alignment.center,
              height: 40,
              width: 40,
              padding: EdgeInsets.all(8),
              child: Image.asset(drawerItemsList[i].image,
                  color: drawerItemsList[i].isSelected
                      ? CommonColors.primaryColor
                      : CommonColors.text_color),
            ),
            Container(
              margin: EdgeInsets.only(top: 5.0),
              alignment: Alignment.center,
              child: Text(drawerItemsList[i].name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: AppConstants.FONT_FAMILY,
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      color: drawerItemsList[i].isSelected
                          ? Colors.white
                          : CommonColors.color_363636)),
            )
          ],
        ),
      ),
    );
    return layoutHome;
  }

  _getDrawerFragment(int pos) {
    print(pos.toString());
    switch (pos) {
      case 0:
        return loginDetails.userType == AppConstants.userTypeOwner
            ? new OwnerProfilePage()
            : new AgentProfilePage();
      case 1:
        return new PropertyList();
      case 2:
        return new AddPropertyPage();
      case 3:
        return new OrderHistory();
      case 4:
        return new ContactAdminPage();
      case 5:
        return new LoginPage();

      default:
        return new Text("");
    }
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(AppConstants.logout),
          content: new Text(AppConstants.logoutConfirmationMessage),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(AppConstants.no),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),

            new FlatButton(
              child: new Text(AppConstants.yes),
              onPressed: () {
                Navigator.of(context).pop();
                appSettings.setLoginDetails(null);
                Navigator.pushReplacementNamed(this.context, LoginPage.tag);
              },
            ),
          ],
        );
      },
    );
  }

  void redirectToPage(int index) {
    if (index == 2) {
      redirectToAddPropertyPage();
    } else if (index == 5) {
      _showDialog();
    }
  }

/*
  redirectToAgentList() {
    Navigator.pushNamed(_mContext, AddPropertyPage.tag).then((value) {
      if (value != null) {
        var result = value as Map;
        if (result.containsKey("propertyDetails")) {
          setState(() {
            _selectedDrawerIndex = 2;
          });
        }
      }
    });
  }
*/

  redirectToAddPropertyPage() {
    Navigator.pushNamed(
            _mContext,
            loginDetails.userType == AppConstants.userTypeOwner
                ? AddPropertyPage.tag
                : AgentAddPropertyPage.tag)
        .then((value) {
      if (value != null && !value is bool) {
        var result = value as Map;
        if (result.containsKey(AppConstants.propertyDetailsKey)) {
          bool isAdded = result[AppConstants.isAdded];
          print("Called");
          if (isAdded) {
            setState(() {
              drawerItemsList[_selectedDrawerIndex].isSelected = false;
              drawerItemsList[1].isSelected = true;
              mTitle = drawerItemsList[1].name;
              _selectedDrawerIndex = 1;
              _getDrawerFragment(1);
            });
          }
        }
      }
    });
  }


  @override
  void dispose() {
    super.dispose();
  }



}
