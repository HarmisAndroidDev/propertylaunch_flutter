import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:property_launch/arguments/PropertyDetailArguments.dart';
import 'package:property_launch/model/AddPropertyM.dart';
import 'package:property_launch/model/MessageMaster.dart';
import 'package:property_launch/model/PropertyFeaturesM.dart';
import 'package:property_launch/model/PropertyTypeM.dart';
import 'package:property_launch/model/agent/AgentDetails.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:property_launch/model/ownerPropertyList/AddpropertyMaster.dart';
import 'package:property_launch/model/ownerPropertyList/ImageDetails.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/ui/AgentList.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/ui/customUi/CustomImage.dart';
import 'package:property_launch/ui/customUi/ProgressImageView.dart';
import 'package:property_launch/ui/customUi/RoundCornerdTextField.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';
import 'package:property_launch/utils/CustomBoxShadow.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:property_launch/utils/ImagePath.dart';

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: AppConstants.googleApiKey);

class AddEditPropertyPage extends StatefulWidget {
  static String tag = "add-edit-property-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AddEditPropertyPageState();
  }
}

class _AddEditPropertyPageState extends State<AddEditPropertyPage> {
  List<PropertyTypeM> propertyTypeList = new List();
  List<PropertyFeaturesM> propertyFeatureList = new List();
  List<ImageDetails> imageList = new List();

  /* form variables*/
  final mPropertyNameController = TextEditingController();
  final mPropertyAddressController = TextEditingController();
  final mPrpertyPriceController = TextEditingController();
  final mPropertyInfoController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  /* google map variables*/
  Set<Marker> markerSet = new Set();
  static CameraPosition _initialCamera = CameraPosition(
    target: LatLng(23.023854, 72.538471),
    zoom: 16.0,
  );
  static LatLng propertyLatLong = new LatLng(23.023854, 72.538471);
  static String markerId = "102";
  String propertyLat = "0.0", propertyLong = "0.0";
  Completer<GoogleMapController> _gController = Completer();
  GoogleMapController _mapController;
  GoogleMap googleMap;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Marker propertyMarker = new Marker(
      markerId: new MarkerId(markerId),
      position: propertyLatLong,
      infoWindow: InfoWindow(title: "Driver Location", snippet: ""),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));

  /*object varibles*/
  int selectedPropertyTypeIndex = 0;
  OwnerPropertyDetails _ownerPropertyDetails;
  AgentDetails _agentDetails = new AgentDetails();
  BuildContext mContext;
  LoginDetails loginDetails;
  bool isFirstTime = true;
  String agentId = "",agentName = "",agentImage = "";

  void addPropertyTypeList() {
    propertyTypeList.add(new PropertyTypeM("House", true));
    propertyTypeList.add(new PropertyTypeM("Townhouse", false));
    propertyTypeList.add(new PropertyTypeM("Commercial", false));
    propertyTypeList.add(new PropertyTypeM("Apartment", false));
    propertyTypeList.add(new PropertyTypeM("Land", false));
  }

  void addPropertyFeaures() {
    propertyFeatureList.add(new PropertyFeaturesM("Air Condition", true));
    propertyFeatureList.add(new PropertyFeaturesM("Gas Cooking", false));
    propertyFeatureList.add(new PropertyFeaturesM("Nbn Ready", false));
    propertyFeatureList.add(new PropertyFeaturesM("Shed", false));
    propertyFeatureList.add(new PropertyFeaturesM("Granny Flat", false));
    propertyFeatureList.add(new PropertyFeaturesM("2nd Living Area", false));
    propertyFeatureList.add(new PropertyFeaturesM("Pool", false));
    propertyFeatureList.add(new PropertyFeaturesM("Electric Cooking", false));
    propertyFeatureList.add(new PropertyFeaturesM("Entertaining Area", false));
    propertyFeatureList.add(new PropertyFeaturesM("Security System", false));
    propertyFeatureList.add(new PropertyFeaturesM("Study", false));
  }

  @override
  void initState() {
    super.initState();
    AppSettings appSetings = new AppSettings();
    appSetings.getLoginDetails().then((onValue) {
      loginDetails = LoginDetails.fromJson(json.decode(onValue));
    });
    markers[new MarkerId(markerId)] = propertyMarker;
    addPropertyFeaures();
    addPropertyTypeList();
    requestPermission();
    Future.delayed(Duration.zero, () {});
    WidgetsBinding.instance.addPostFrameCallback((_) => setValueToWidgets());
  }

  /*receive arguments from pre page*/

  void receiveArguements(BuildContext context) {
    final PropertyDetailsArguments propertyDetailsArguments =
        ModalRoute.of(context).settings.arguments;
    _ownerPropertyDetails = propertyDetailsArguments.ownerPropertyDetails;

    setState(() {
      agentImage=_ownerPropertyDetails.agentImage;
      agentName=_ownerPropertyDetails.agentName;
      agentId =_ownerPropertyDetails.agentId;
    });
  }

  /*set values to widgets*/

  setValueToWidgets() {
    if (_ownerPropertyDetails != null) {
      imageList.addAll(_ownerPropertyDetails.propertyImage);
      setvalues(_ownerPropertyDetails);
      setSelectedValue();
      setMapValues();
    } else {
      CommonUtils.showErrorMessage(mContext, AppConstants.oops);
    }
  }

  void setMapValues() {
    double lat = _ownerPropertyDetails.lat == ""
        ? 0.0
        : double.parse(_ownerPropertyDetails.lat);
    double long = _ownerPropertyDetails.long == ""
        ? 0.0
        : double.parse(_ownerPropertyDetails.long);

    LatLng propertyLatLong = new LatLng(lat, long);
    _initialCamera = CameraPosition(
      target: propertyLatLong,
      zoom: 16.0,
    );

    propertyLat = _ownerPropertyDetails.lat;
    propertyLong = _ownerPropertyDetails.long;
    propertyMarker = new Marker(
        markerId: new MarkerId(markerId),
        position: propertyLatLong,
        infoWindow: InfoWindow(
            title: _ownerPropertyDetails.proName,
            snippet: _ownerPropertyDetails.propertyInfo),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));
    markers[new MarkerId(markerId)] = propertyMarker;
    if (_mapController != null) {
      _mapController.animateCamera(CameraUpdate.newCameraPosition(
          new CameraPosition(target: propertyLatLong)));
    }
  }

  void setvalues(OwnerPropertyDetails ownerPropertyDetails) {
    if (ownerPropertyDetails != null) {
      mPropertyAddressController.text = ownerPropertyDetails.proAddress;
      mPropertyNameController.text = ownerPropertyDetails.proName;
      mPrpertyPriceController.text = ownerPropertyDetails.estimatedSalePrice;
      mPropertyInfoController.text = ownerPropertyDetails.propertyInfo;
    }
  }

  void setSelectedValue() {
    for (int i = 0; i < propertyFeatureList.length; i++) {
      PropertyFeaturesM propertyFeaturesM = propertyFeatureList[i];
      for (String featureName in _ownerPropertyDetails.feName) {
        if (propertyFeaturesM.featureName == featureName) {
          propertyFeaturesM.isSelected = true;
          propertyFeatureList[i] = propertyFeaturesM;
          print("Conditin not working" +
              featureName +
              " == == " +
              propertyFeaturesM.featureName);
        }
      }
    }

    propertyTypeList[selectedPropertyTypeIndex].isSelected = false;
    selectedPropertyTypeIndex =
        int.parse(_ownerPropertyDetails.propertyType) - 1;
    propertyTypeList[selectedPropertyTypeIndex].isSelected = true;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (isFirstTime) {
      receiveArguements(context);
      isFirstTime = false;
    }
    return initViews();
  }

/* select image with camera and gallery*/
  void _selectImageOption(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.camera),
                    title: new Text(AppConstants.text_camera),
                    onTap: () {
                      _getImageFromDevice(false);
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.attach_file),
                  title: new Text(AppConstants.text_gallery),
                  onTap: () {
                    _getImageFromDevice(true);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  Future _getImageFromDevice(bool isGallery) async {
    var galleryFile = await ImagePicker.pickImage(
        source: isGallery ? ImageSource.gallery : ImageSource.camera);
    if (galleryFile != null) {
      setState(() {
        ImageDetails imageDetails = new ImageDetails();
        imageDetails.image = galleryFile.path;
        imageDetails.isFile = true;
        imageList.add(imageDetails);
        print("Image path: " + galleryFile.path);
      });
    } else {
      CommonUtils.showErrorMessage(mContext, "unable to get image path");
    }
  }

  /* define all widgets*/

  Widget initViews() {
    final tvAddImage = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 2.0),
          textAlign: TextAlign.left,
          text: AppConstants.add_image,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));

    final footerAddImage = new Align(
        alignment: Alignment.centerLeft,
        child: new InkWell(
          onTap: () {
            _selectImageOption(mContext);
          },
          child: new Container(
            width: 80,
            height: 55,
            padding: EdgeInsets.all(15.0),
            decoration: new BoxDecoration(
                boxShadow: [
                  CustomBoxShadow(
                    color: Colors.white,

                    blurRadius: 1.0, // has the effect of softening the shadow
                    // has the effect of extending the shadow
                    blurStyle: BlurStyle.outer,

                    offset: Offset(
                      0.0, // horizontal, move right 10
                      0.0, // vertical, move down 10
                    ),
                  )
                ],
                border: new Border.all(color: CommonColors.primaryColor),
                borderRadius: BorderRadius.circular(10)),
            child: new Center(
              child: Image.asset(
                LocalImages.ic_add_photo,
                color: CommonColors.primaryColor,
              ),
            ),
          ),
        ));

    final propertyThumbnailList = new Container(
        height: 60,
        margin: EdgeInsets.only(top: 10.0),
        child: new ListView.builder(
            physics: ClampingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: imageList == null ? 1 : imageList.length + 1,
            itemBuilder: (context, index) {
              if (index == imageList.length) {
                return footerAddImage;
              } else {
                return propertyThumb(
                    index, _ownerPropertyDetails.propertyImage);
              }
            }));

    final propertyName = RoundCornerTextField(
      margin: EdgeInsets.only(top: 30.0),
      borderColor: CommonColors.edt_border_color,
      hintText: AppConstants.property_name,
      hintTextColor: CommonColors.hint_text_color,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.enter_property_name;
        }
      },
      controller: mPropertyNameController,
      inputType: TextInputType.text,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final propertyAddress = RoundCornerTextField(
      margin: EdgeInsets.only(top: 20.0),
      borderColor: CommonColors.edt_border_color,
      hintText: AppConstants.property_address,
      hintTextColor: CommonColors.hint_text_color,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.property_address;
        }
      },
      controller: mPropertyAddressController,
      inputType: TextInputType.text,
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );
    final propertyPrice = RoundCornerTextField(
      margin: EdgeInsets.only(top: 20.0),
      borderColor: CommonColors.edt_border_color,
      hintText: AppConstants.price_hint,
      hintTextColor: CommonColors.hint_text_color,
      validator: (value) {
        if (value.isEmpty) {
          return AppConstants.property_price;
        }
      },
      controller: mPrpertyPriceController,
      inputType: TextInputType.numberWithOptions(decimal: true, signed: false),
      autoFocus: false,
      fontWeight: FontWeight.w400,
    );

    final tvChangeAgent = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.change_agent,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));

    final agentIconBox = new Container(
      height: 50,
      width: 50,
      margin: EdgeInsets.only(left: 5.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5)),
      child: new Center(
          child: new Center(
              child: ProgressImageView(
        height: 50,
        width: 50,
        borderRadius: 5,
        isCircule: false,
        imageurl:agentImage,
      ))),
    );
    final lblAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: AppConstants.agency_name,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final tvAgentName = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 10.0),
          maxLines: 1,
          textAlign: TextAlign.start,
          text:agentName,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));
    final agentDetails = new Container(
        margin: EdgeInsets.only(left: 10.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[lblAgentName, tvAgentName],
        ));
    final layoutAgent = new Container(
      height: 60,
      margin: const EdgeInsets.only(top: 20.0),
      decoration: new BoxDecoration(
          boxShadow: [
            CustomBoxShadow(
              color: Colors.white,

              blurRadius: 1.0, // has the effect of softening the shadow
              // has the effect of extending the shadow
              blurStyle: BlurStyle.outer,

              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          border: new Border.all(color: CommonColors.box_shadow_color),
          borderRadius: BorderRadius.circular(5.0)),
      child: new InkWell(
          onTap: () {

            if(loginDetails.userType == _ownerPropertyDetails.addBy){
              redirectToAgentList();
            }else{
              CommonUtils.showErrorMessage(mContext, AppConstants.you_cant_change+""+AppConstants.agent);
            }
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[agentIconBox, agentDetails],
          )),
    );

    final lblPropertyDetails = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_detail,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));

    final edPropertyDetail = new Container(
        height: 150,
        child: RoundCornerTextField(
          margin: EdgeInsets.only(top: 20.0),
          maxLines: 500,
          borderColor: CommonColors.edt_border_color,
          hintText: AppConstants.property_detail,
          hintTextColor: CommonColors.hint_text_color,
          validator: (value) {
            if (value.isEmpty) {
              return AppConstants.property_info;
            }
          },
          controller: mPropertyInfoController,
          inputType: TextInputType.text,
          autoFocus: false,
          fontWeight: FontWeight.w400,
        ));

    final tvSelect = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.select,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));

    final itemGridDelegate = new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 5.0,
        mainAxisSpacing: 5.0,
        crossAxisCount: 2,
        childAspectRatio: 5.0);

    final propertyTypeGridview = new Container(
      child: new Container(
        padding: EdgeInsets.only(top: 20.0),
        color: Colors.white,
        child: GridView.builder(
          physics: ClampingScrollPhysics(),
          gridDelegate: itemGridDelegate,
          shrinkWrap: true,
          itemCount: propertyTypeList.length,
          itemBuilder: (BuildContext context, int index) {
            return new GestureDetector(
              child: _buildPropertyTypeItems(index, context),
              onTap: () {
                setState(() {
                  propertyTypeList[index].isSelected = true;
                  propertyTypeList[selectedPropertyTypeIndex].isSelected =
                      false;
                  selectedPropertyTypeIndex = index;
                });
              },
            );
          },
        ),
      ),
    );

    final tvPropertyFeatures = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.left,
          text: AppConstants.property_features,
          textSize: 16.0,
          textColor: CommonColors.black,
          fontWeight: FontWeight.w700,
        ));

    final faeturesGridItems = new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 5.0,
        mainAxisSpacing: 5.0,
        crossAxisCount: 2,
        childAspectRatio: 5.0);

    final propertyFeaturesGrid = new Container(
      child: new Container(
        padding: EdgeInsets.only(top: 20.0),
        color: Colors.white,
        child: GridView.builder(
          gridDelegate: faeturesGridItems,
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: propertyFeatureList.length,
          itemBuilder: (BuildContext context, int index) {
            return new GestureDetector(
                onTap: () {
                  setState(() {
                    propertyFeatureList[index].isSelected =
                        !propertyFeatureList[index].isSelected;
                  });
                },
                child: _buildFeaturesItems(index, context));
          },
        ),
      ),
    );

    final lblSelectAgent = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: AppConstants.select_agent,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    googleMap = GoogleMap(
      onTap: (LatLng latLong) {
        OpenPlaceAutoComplete();
      },
      gestureRecognizers: Set()
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
        ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer())),
      mapType: MapType.normal,
      zoomGesturesEnabled: true,
      markers: Set<Marker>.of(markers.values),
      minMaxZoomPreference: MinMaxZoomPreference.unbounded,
      initialCameraPosition: _initialCamera,
      onMapCreated: (GoogleMapController googleMapController) {
        _mapController = googleMapController;
        //  _gController.complete(googleMapController);
      },
    );

    final tvActionSave = new Align(
        alignment: Alignment.centerRight,
        child: new InkWell(
          child: new CTextView(
            margin: EdgeInsets.only(right: 20.0),
            backgroundColor: Colors.transparent,
            maxLines: 1,
            textAlign: TextAlign.center,
            text: AppConstants.save,
            textSize: 16.0,
            textColor: Colors.white,
            fontWeight: FontWeight.w700,
          ),
          onTap: () {
            if (checkValidation()) {

              CommonUtils.isInternetConnected().then((isConnected){
                if(isConnected){
                  editPropertyApi();
                }else{
                  CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
                }
              });
            }
          },
        ));

    final mapView = new Container(
        height: 200,
        width: double.infinity,
        margin: EdgeInsets.only(top: 20.0),
        child: new ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            child: googleMap));

    final mainListview = new ListView(
      padding: EdgeInsets.all(15.0),
      children: <Widget>[
        tvAddImage,
        propertyThumbnailList,
        propertyName,
        propertyAddress,
        propertyPrice,
        tvChangeAgent,
        layoutAgent,
        lblPropertyDetails,
        edPropertyDetail,
        tvSelect,
        propertyTypeGridview,
        tvPropertyFeatures,
        propertyFeaturesGrid,
        mapView,
      ],
    );

    Form form = new Form(child: mainListview, key: _formKey);

    final scaffold = new Scaffold(
      appBar: AppBar(
        title: CTextView(
          text: AppConstants.text_edit_property,
          textSize: AppDimens.appBarTitleSize,
          backgroundColor: Colors.transparent,
          textColor: Colors.white,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(mContext, false),
          color: Colors.white,
        ),
        backgroundColor: CommonColors.primaryColor,
        automaticallyImplyLeading: true,
        actions: <Widget>[tvActionSave],
      ),
      body: new Builder(builder: (BuildContext context) {
        mContext = context;
        return form;
      }),
    );

    return scaffold;
  }

  Future requestPermission() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions(
            [PermissionGroup.location, PermissionGroup.storage]);
  }

  Future<int> checkPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    return permission.value;
  }

  Future<int> checkStoragePermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);
    return permission.value;
  }

  Widget propertyThumb(int index, List<ImageDetails> list) {
    final ivPropertyImage = new Align(
        alignment: Alignment.centerLeft,
        child: new CustomImage(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          imageUrl: imageList[index].image,
          height: 55,
          width: 80,
        ));

    final imageContainer = new Container(
      height: 65,
      width: 85,
      child: new Center(child: ivPropertyImage),
    );

    final btnCross = new Container(
        height: 15,
        width: 15,
        child: InkWell(
          child: new Container(
            height: 15,
            width: 15,
            padding: EdgeInsets.all(4.0),
            decoration: new BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(30.0))),
            child: Image.asset(
              "images/ic_delete.png",
              color: CommonColors.primaryColor,
            ),
          ),
          onTap: () {
            if (imageList.length > 1) {
              if (imageList[index].isFile) {
                setState(() {
                  imageList.removeAt(index);
                });
              } else {

                CommonUtils.isInternetConnected().then((isConnected){
                  if(isConnected){
                    deletePropertyImage(_ownerPropertyDetails.propertyId, list[index].id.toString(), index);                  }else{
                    CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
                  }
                });


              }
            } else {
              CommonUtils.showErrorMessage(
                  mContext, "You can't delete single property image");
            }
          },
        ));
    final stackImage = new Container(
        margin: EdgeInsets.only(right: 5.0),
        child: new Stack(children: <Widget>[
          imageContainer,
          Positioned(top: 0, right: 0, child: btnCross),
        ]));

    return stackImage;
  }

  Widget _buildPropertyTypeItems(int index, BuildContext context) {
    final icCheckbox = new Container(
      height: 20,
      width: 20,
      child: Icon(
        (propertyTypeList[index].isSelected &&
                propertyTypeList[index].isSelected != null)
            ? Icons.check_box
            : Icons.check_box_outline_blank,
        color: CommonColors.primaryColor,
      ),
    );

    final tvPropertyType = new Container(
        width: 120,
        margin: EdgeInsets.only(left: 10.0),
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: propertyTypeList[index].propertyName,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    final rowContainer = new Container(
      width: 150,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[icCheckbox, tvPropertyType],
      ),
    );

    return rowContainer;
  }

  Widget _buildFeaturesItems(int index, BuildContext context) {
    final icCheckbox = new Container(
      height: 20,
      width: 20,
      child: Icon(
        propertyFeatureList[index].isSelected
            ? Icons.check_box
            : Icons.check_box_outline_blank,
        color: CommonColors.primaryColor,
      ),
    );

    final tvPropertyFeature = new Container(
        width: 120,
        margin: EdgeInsets.only(left: 10.0),
        child: new CTextView(
          maxLines: 1,
          textAlign: TextAlign.start,
          text: propertyFeatureList[index].featureName,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    final rowContainer = new Container(
      width: 150,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[icCheckbox, tvPropertyFeature],
      ),
    );

    return rowContainer;
  }

  deletePropertyImage(String propertyId, String imageId, int index) async {
    CommonUtils.showProgressDialog(mContext);
    final response = await http.post(ApiUrls.deletePropertyImage,
        body: getImageParams(propertyId, imageId));
    print("URL: " + response.request.url.toString());
    if (response.statusCode == 200) {
      print("Response: " + response.body);
      CommonUtils.hideProgressDialog(mContext);
      MessageMaster loginMaster =
          MessageMaster.fromJson(json.decode(response.body));
      if (loginMaster != null) {
        if (loginMaster.success == 1) {
          setState(() {
            imageList.removeAt(index);
            _ownerPropertyDetails.propertyImage.removeAt(index);
          });
          CommonUtils.showErrorMessage(mContext, loginMaster.message);
        } else {
          CommonUtils.showErrorMessage(mContext, loginMaster.message);
        }
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showErrorMessage(mContext, "" + AppConstants.oops);
      throw Exception('Exception');
    }
  }

  String getImageParams(String propertyId, String imageId) {
    var map = new Map<String, dynamic>();
    map[ApiParams.proId] = propertyId;
    map[ApiParams.imageId] = imageId;
    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }

  redirectToAgentList() {
    Navigator.pushNamed(mContext, AgentList.tag).then((value) {
      if (value != null) {
        var result = value as Map;
        if (result.containsKey("agentDetails")) {
          setState(() {
            _agentDetails = result['agentDetails'];
          //  _ownerPropertyDetails.agentId = _agentDetails.userId.toString();
          //  _ownerPropertyDetails.agentName = _agentDetails.userName;
          //  _ownerPropertyDetails.profileImage = _agentDetails.profileImage;
            agentId =_agentDetails.userId.toString();
            agentName=_agentDetails.userName;
            agentImage=_agentDetails.profileImage;
          });
        }
      }
    });
  }

  Future OpenPlaceAutoComplete() async {
    /*enable language en and country us to load country wise*/
    Prediction p = await PlacesAutocomplete.show(
      context: mContext,
      apiKey: AppConstants.googleApiKey,
      mode: Mode.fullscreen,
      // Mode.fullscreen
      /*language: "en",
        components: [new Component(Component.country, "US")]*/
    );
    displayPrediction(p);
  }

  Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;
      print(lat.toString() + "//" + lng.toString());
      propertyLong = lng.toString();
      propertyLat = lat.toString();

      setState(() {
        propertyLatLong = new LatLng(lat, lng);
        _initialCamera = new CameraPosition(target: propertyLatLong, zoom: 16);
        propertyMarker = new Marker(
            markerId: new MarkerId(markerId),
            position: propertyLatLong,
            infoWindow:
                InfoWindow(title: detail.result.formattedAddress, snippet: ""),
            icon:
                BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));
        markers[propertyMarker.markerId] = propertyMarker;
        _mapController
            .animateCamera(CameraUpdate.newCameraPosition(_initialCamera));
      });
    }
  }

  editPropertyApi() async {
    CommonUtils.showProgressDialog(mContext);
    final request = await http.MultipartRequest(
        AppConstants.METHOD_POST, Uri.parse(ApiUrls.addProperty));
    request.fields[ApiParams.json_content] = getJsonParams();

    /*add images*/
    for (ImageDetails imagePath in imageList) {
      if (imagePath.isFile != null && imagePath.isFile) {
        try {
          File imageFile = new File(imagePath.image);
          var stream =
              new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
          var length = await imageFile.length();
          var multipartFile = new http.MultipartFile(
              ApiParams.images, stream, length,
              filename: basename(imageFile.path));
          request.files.add(multipartFile);
        } catch (e) {
          print("error: " + e);
        }
      }
    }
    /* add images to end*/

    request.send().then((response) {
      print("URL: " + response.request.url.toString());
      try {
        CommonUtils.hideProgressDialog(mContext);
        response.stream.transform(utf8.decoder).listen((value) {
          print("Response: " + value);
          if (response.statusCode == 200) {
            AddPropertyMaster addPropertyMaster =
                AddPropertyMaster.fromJson(json.decode(value));
            if (addPropertyMaster.success == 1) {
              CommonUtils.showErrorMessage(mContext, addPropertyMaster.message);
              backToDetail(addPropertyMaster.result, mContext);
            } else {
              CommonUtils.showErrorMessage(mContext, addPropertyMaster.message);
            }
          }
        });
      } catch (e) {
        CommonUtils.hideProgressDialog(mContext);
        CommonUtils.showErrorMessage(mContext, e);
        print("Error: " + e);
      }
    });
  }

  String getJsonParams() {
    AddPropertyP addPropertyM = new AddPropertyP();
    addPropertyM.propertyId = _ownerPropertyDetails.propertyId;
    addPropertyM.proName = mPropertyNameController.text;
    addPropertyM.proAddress = mPropertyAddressController.text;
    addPropertyM.suburb = _ownerPropertyDetails.suburb;
    addPropertyM.state = _ownerPropertyDetails.state;
    addPropertyM.postCode = _ownerPropertyDetails.postCode;
    addPropertyM.estimatedSalePrice = mPrpertyPriceController.text;
    addPropertyM.rentalPriceWeeklyPrice =
        _ownerPropertyDetails.rentalPriceWeeklyPrice;
    addPropertyM.propertyInfo = mPropertyInfoController.text;
    addPropertyM.propertyType = (selectedPropertyTypeIndex + 1).toString();
    addPropertyM.airCondition =
        propertyFeatureList[0].isSelected ? "true" : "false";
    addPropertyM.gasCooking =
        propertyFeatureList[1].isSelected ? "true" : "false";
    addPropertyM.nbnReady =
        propertyFeatureList[2].isSelected ? "true" : "false";
    addPropertyM.shed = propertyFeatureList[3].isSelected ? "true" : "false";
    addPropertyM.grannyFlat =
        propertyFeatureList[4].isSelected ? "true" : "false";
    addPropertyM.secondLivingArea =
        propertyFeatureList[5].isSelected ? "true" : "false";
    addPropertyM.pool = propertyFeatureList[6].isSelected ? "true" : "false";
    addPropertyM.eletecticCooking =
        propertyFeatureList[7].isSelected ? "true" : "false";
    addPropertyM.entertainingArea =
        propertyFeatureList[8].isSelected ? "true" : "false";
    addPropertyM.securitySystem =
        propertyFeatureList[9].isSelected ? "true" : "false";
    addPropertyM.study = propertyFeatureList[10].isSelected ? "true" : "false";
    addPropertyM.userId = _ownerPropertyDetails.ownerId;
    addPropertyM.user1Id = agentId;
    addPropertyM.userType = AppConstants.userTypeOwner;
    addPropertyM.lat = propertyLat;
    addPropertyM.long = propertyLong;
    String jsonStr = jsonEncode(addPropertyM);

    print("params == =" + jsonStr);

    return jsonStr;
  }

  bool checkValidation() {
    if (imageList.length == 0) {
      CommonUtils.showErrorMessage(
          mContext, AppConstants.property_image_required);
      return false;
    } else if (!_formKey.currentState.validate()) {
      return false;
    } else if (propertyLat == "0.0" || propertyLong == "0.0") {
      CommonUtils.showErrorMessage(
          mContext, AppConstants.property_location_required);
      return false;
    }
    return true;
  }

  void backToDetail(
      OwnerPropertyDetails ownerPropertyDetails, BuildContext context) {
    Navigator.of(context).pop(
        {'propertyDetails': ownerPropertyDetails, AppConstants.isAdded: false});
  }
}
