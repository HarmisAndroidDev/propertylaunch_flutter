import 'dart:async';

import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:property_launch/arguments/VideoStyleDetailArguments.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/model/videoStyleList/VideoStyleDetails.dart';
import 'package:property_launch/ui/customUi/BlueBorderButton.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/ui/owner/AreaSpendPage.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:video_player/video_player.dart';

class InfoAboutYourVideoPage extends StatefulWidget {
  static String tag = "selected-video-info-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return _InfoAboutYourVideoPageState();
  }
}

class _InfoAboutYourVideoPageState extends State<InfoAboutYourVideoPage> {
  Timer timer;
  VideoStyleListDetails videoStyleListDetails;
  OwnerPropertyDetails  ownerPropertyDetails;
  BuildContext mContext;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    receiveArguements(context);

    return initViews();
  }

  void receiveArguements(BuildContext context) {
    final VideoStyleDetailArguments propertyDetailsArguments =
        ModalRoute.of(context).settings.arguments;
    videoStyleListDetails = propertyDetailsArguments.videoStyleListDetails;
    ownerPropertyDetails =propertyDetailsArguments.ownerPropertyDetails;
  }

   VideoPlayerController videoPlayerController;
  Widget initViews() {
     videoPlayerController =
    VideoPlayerController.network(videoStyleListDetails.sampleVideo);

    final chewieController = ChewieController(
        videoPlayerController: videoPlayerController,
        aspectRatio: 3 / 2,
        autoPlay: true,
        looping: false,
        showControls: true);

    final playerWidget = Chewie(
      controller: chewieController,
    );

    final videoView = new Container(
        height: 200,
        margin: EdgeInsets.only(top: 10.0),
        child: new ClipRRect(
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            child: new Container(color: Colors.black, child: playerWidget)));

    final btnVideoAd = new InkWell(
        onTap: () {
          Navigator.pushNamed(mContext, AreaSpendPage.tag,
              arguments: VideoStyleDetailArguments(videoStyleListDetails,ownerPropertyDetails));        },
        child: new BlueBorderButton(
          margin: EdgeInsets.only(top: 20.0),
          height: AppDimens.btn_height,
          buttonText: AppConstants.how_many_views,
          width: double.infinity,
          radious: 5.0,
          backgroundColor: CommonColors.blue_btn_color,
        ));

    final tvVideoDetails = new Align(
        alignment: Alignment.centerLeft,
        child: new CTextView(
          margin: EdgeInsets.only(top: 20.0),
          textAlign: TextAlign.justify,
          text: videoStyleListDetails.description,
          textSize: 14.0,
          textColor: CommonColors.text_color,
          fontWeight: FontWeight.w700,
        ));

    final mainListview = new ListView(
      padding: EdgeInsets.all(10.0),
      children: <Widget>[videoView, btnVideoAd, tvVideoDetails],
    );

    return Scaffold(
        appBar: AppBar(
          title: CTextView(
            backgroundColor: Colors.transparent,
            text: AppConstants.video_style,
            textColor: Colors.white,
            textSize: AppDimens.appBarTitleSize,),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context, false),
            color: Colors.white,
          ),
          backgroundColor: CommonColors.primaryColor,
          automaticallyImplyLeading: true,
        ),
      body: Builder(builder: (BuildContext conext){
        mContext= conext;
         return mainListview;
      }) ,

    );
  }

  @override
  void dispose() {
    super.dispose();
    videoPlayerController.dispose();

  }
}
