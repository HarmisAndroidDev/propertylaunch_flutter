import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:property_launch/model/MessageMaster.dart';
import 'package:property_launch/model/login/LoginDetails.dart';
import 'dart:convert';

import 'package:property_launch/ui/customUi/RoundCornerdTextField.dart';
import 'package:property_launch/ui/customUi/SolidButton.dart';
import 'package:property_launch/utils/ApiUrls.dart';
import 'package:property_launch/utils/Api_Params.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/AppSettings.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/CommonUtils.dart';

class ContactAdminPage extends StatefulWidget {
  static String tag = "contact-admin-page";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ContactAdminPageState();
  }
}

class _ContactAdminPageState extends State<ContactAdminPage> {
  final _formKey = GlobalKey<FormState>();
  BuildContext mContext;
  final mSubjectController = new TextEditingController();
  final mDescriptionController = new TextEditingController();
  LoginDetails _loginDetails;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppSettings appSettings = new AppSettings();

    appSettings.getLoginDetails().then((onValue) {
      setState(() {
        _loginDetails = LoginDetails.fromJson(json.decode(onValue));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final lblSubject = new Container(
        margin: EdgeInsets.only(top: 20.0),
        alignment: Alignment.centerLeft,
        child: new Text(
          AppConstants.subject,
          style: TextStyle(
              fontFamily: AppConstants.FONT_FAMILY,
              fontSize: 16.0,
              fontWeight: FontWeight.w600,
              color: CommonColors.text_color),
        ));
    final lblDescription = new Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(top: 20.0),
      child: new Text(
        AppConstants.descrition,
        style: TextStyle(
            fontFamily: AppConstants.FONT_FAMILY,
            fontSize: 16.0,
            fontWeight: FontWeight.w600,
            color: CommonColors.text_color),
      ),
    );

    final edSubject = new RoundCornerTextField(
        borderColor: CommonColors.color_cbcbcb,
        controller: mSubjectController,
        margin: EdgeInsets.only(top: 20.0),
        validator: (value) {
          if (CommonUtils.isEmpty(value)) {
            return AppConstants.enter_subject;
          }
        });

    final edDescription = new Container(
        height: 200,
        child: new RoundCornerTextField(
          controller: mDescriptionController,
          margin: EdgeInsets.only(top: 20.0),
          borderColor: CommonColors.color_cbcbcb,
          maxLines: 50,
          validator: (value) {
            if (CommonUtils.isEmpty(value)) {
              return AppConstants.enter_description;
            }
          },
        ));

    final btnSend = new Align(
        alignment: Alignment.center,
        child: new SimpleRoundButton(
          buttonText: AppConstants.send,
          radious: 5.0,
          margin: EdgeInsets.only(top: 20.0),
          onPressed: () {
            if (_formKey.currentState.validate()) {

              CommonUtils.isInternetConnected().then((isConnected){
                if(isConnected){
                  reportApi();
                }else{
                  CommonUtils.showErrorMessage(mContext, AppConstants.no_internet);
                }
              });
            }
          },
        ));

    final mainListvIew = new ListView(
      padding: EdgeInsets.all(10.0),
      children: <Widget>[
        lblSubject,
        edSubject,
        lblDescription,
        edDescription,
        btnSend
      ],
    );

    Form form = new Form(
      child: mainListvIew,
      key: _formKey,
    );
    final scafold = new Scaffold(
      resizeToAvoidBottomInset: false,
      body: Builder(builder: (BuildContext context) {
        mContext = context;
        return form;
      }),
    );

    return scafold;
  }

  reportApi() async {
    CommonUtils.showProgressDialog(mContext);
    final response =
        await http.post(ApiUrls.contact_admin, body: getReportApiParams());
    print("URL: " + response.request.url.toString());
    MessageMaster messageMaster =
        MessageMaster.fromJson(json.decode(response.body));
    if (response.statusCode == 200) {
      CommonUtils.hideProgressDialog(mContext);
      if (messageMaster.success == 1) {
        CommonUtils.showErrorMessage(mContext, AppConstants.message_send_to_admin);
      } else {
        CommonUtils.showErrorMessage(mContext, messageMaster.message);
      }
      print("Response: " + response.body);
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showErrorMessage(mContext, "" + AppConstants.oops);
      throw Exception('Exception');
    }
  }

  String getReportApiParams() {
    var map = new Map<String, dynamic>();
    map[ApiParams.userId] = _loginDetails.userId;
    map[ApiParams.subject] = mSubjectController.text;
    map[ApiParams.message] = mDescriptionController.text;
    print("Parameter: " + json.encode(map));
    return json.encode(map);
  }
}
