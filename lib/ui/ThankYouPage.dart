

import 'package:flutter/material.dart';
import 'package:property_launch/ui/owner/home.dart';

import 'package:property_launch/utils/AppDimens.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'customUi/BlueBorderButton.dart';
import 'customUi/CTextview.dart';

class ThankYouPage extends StatefulWidget {
  static String tag = 'thankyou-page';

  @override
  _ThankYouPageState createState() => _ThankYouPageState();
}

class _ThankYouPageState extends State<ThankYouPage> {
  var isLoading = false;
  final _formKey = GlobalKey<FormState>();
  BuildContext mContext;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final image = new Container(
        margin: EdgeInsets.only(top: 20.0),
        width: double.infinity,
        height: 150,
        child: new Image.asset("images/img_logo.png"));

    final backToHome = new BlueBorderButton(
      margin: EdgeInsets.only(top: 30.0,left: 20.0,right: 20.0,bottom: 20.0),
      height: AppDimens.btn_height,
      buttonText: AppConstants.back_to_home,
      width: double.infinity,
      radious: 5.0,
      backgroundColor: CommonColors.blue_btn_color,
      onPressed: () {
        Navigator.of(context)
            .pushNamedAndRemoveUntil(HomePage.tag, (Route<dynamic> route) => false);
      },
    );

    final tvThankYouGoodLuck = new Align(
        alignment: Alignment.center,
        child:new Container(
          margin: EdgeInsets.only(top: 30.0),
          width: 300,
          child: new CTextView(
          text: AppConstants.thank_you,
          textSize: 25.0,
          textAlign: TextAlign.center,
          textColor: CommonColors.color_a9a9a9,
          fontWeight: FontWeight.w900,
        ),));

    final tvVideo = new Align(
        child:new Container(
          width: double.infinity,
          margin: EdgeInsets.only(top: 30.0,left: 10.0,right: 10.0),
        child:new CTextView(
      text: AppConstants.your_video_live,
      textSize: 16.0,
      textAlign: TextAlign.center,
      textColor: CommonColors.color_a9a9a9,
      fontWeight: FontWeight.w700,
    )));

    final mainListView = new ListView(
      shrinkWrap: true,
      children: <Widget>[image, tvThankYouGoodLuck, tvVideo],
    );

    final scaffold = new Scaffold(
      backgroundColor: Colors.white,
      body: new Builder(builder: ((BuildContext context) {
        mContext = context;
        return mainListView;
      })),
      bottomNavigationBar: new Container(
        padding: EdgeInsets.all(0.0),
        child: backToHome,
      ),
    );

    return scaffold;
  }
}
