class ApiUrls {
  /*Local*/
//  static String BASE_URL = "http://192.168.1.25/school_management/admin/";

  /*Live*/
  static String BASE_URL = "http://propertylaunch.pro/index.php/api/";

  static String login = BASE_URL + "login";

  static String dummyImage =
      "http:\/\/propertylaunch.pro\/public\/default-image.jpeg";
  static String dummyVideoUrl =
      "http://mirrors.standaloneinstaller.com/video-sample/star_trails.mkv";
  static String dummyPropertyImage =
      "http://blog.upasnagroup.com/wp-content/uploads/2018/08/1-2-bhk-flat-on-rent-resale-chinchwad-link-road-chinchwad-gaon.jpg";

  static var contact_admin = BASE_URL + "contactAdmin";

  static var getProperyList = BASE_URL + "property";

  static var getVideoInfo = BASE_URL + "templateVideo";

  static var getAgentList = BASE_URL + "agentOwner";

  static var changePassword = BASE_URL + "changePassword";

  static var getVideoStyleList = BASE_URL + "templateVideo";

  static var getPriceList = BASE_URL + "priceList";

  static var registration = BASE_URL + "registration";

  static String addProperty = BASE_URL + "addProperty";

  static var deletePropertyImage = BASE_URL + "deleteProImage";

  static var makePayment = BASE_URL + "paymentgatwaye";

  static String edit_owner_profile = BASE_URL + "updateProfile";

  static var getPaymentHistory = BASE_URL + "paymentHistory";

  static var getPropertyDetail =BASE_URL+"getPropertyById";
}
