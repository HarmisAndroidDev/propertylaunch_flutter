import 'package:property_launch/model/login/LoginDetails.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppSettings {
  ///
  /// Instantiation of the SharedPreferences library
  ///
  final String _kNotificationsPrefs = "allowNotifications";
  final String KEY_LOGIN_DETAILS = "KEY_LOGIN_DETAILS";
  final String KEY_CURRENCY_SYMBOL = "KEY_CURRENCY_SYMBOL";
  final String KEY_DEVICE_TOKEN = "KEY_DEVICE_TOKEN";

  /// ------------------------------------------------------------
  /// Method that returns the user decision to allow notifications
  /// ------------------------------------------------------------
  Future<bool> getAllowsNotifications() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(_kNotificationsPrefs) ?? false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the user decision to allow notifications
  /// ----------------------------------------------------------
  Future<bool> setAllowsNotifications(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setBool(_kNotificationsPrefs, value);
  }

  /// ------------------------------------------------------------
  /// Method that returns the user decision on sorting order
  /// ------------------------------------------------------------
  Future<String> getLoginDetails() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(KEY_LOGIN_DETAILS) ?? null;
  }

  /// ----------------------------------------------------------
  /// Method that saves the user decision on sorting order
  /// ----------------------------------------------------------
  Future<bool> setLoginDetails(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(KEY_LOGIN_DETAILS, value);
  }

  //////
  Future<String> getCurrencySymbol() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(KEY_CURRENCY_SYMBOL) ?? "\$";
  }

  /// ----------------------------------------------------------
  /// Method that saves the user decision on sorting order
  /// ----------------------------------------------------------
  Future<bool> setCurrencySybmol(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(KEY_CURRENCY_SYMBOL, value);
  }

  // Device Token
  Future<String> getDeviceToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(KEY_DEVICE_TOKEN) ?? "";
  }

  Future<bool> setDeviceToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(KEY_DEVICE_TOKEN, value);
  }
}
