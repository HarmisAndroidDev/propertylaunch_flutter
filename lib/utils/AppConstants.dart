import 'package:flutter/src/material/scaffold.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';

class AppConstants {
  static final String APP_NAME = 'Property Launch';
  static final String FONT_FAMILY = 'Roboto';
  static final String no_internet = 'No Internet Connected';
  static final String googleApiKey = 'AIzaSyDynSy7CIAMdBdomnmnO_p4TW8Im6utprA';

  static final String METHOD_POST = 'POST';
  static final String PUBLISH_KEY_STRIPE =
      'pk_test_zQm8B5ZcNuC0zdNyOdRxT5bt00mUd7EHk8';

  /*User Types*/
  static final String USER_TYPE_OWNER = 'owner';
  static final String USER_TYPE_AGENT = 'agent';
  static final String USER_TYPE_LAB = 'lab';
  static final String USER_TYPE_LIBRARY = 'library';
  static final String USER_TYPE_BUS_DRIVER = 'busdriver';

  static final String username = 'User Name';
  static final String password = 'Password';
  static final String login = 'Login';
  static final String forgotPassword = 'Forgot Password ?';

  /*Login Page*/
  static final String enter_username = ' please enter username';
  static final String enter_password = 'Please enter password';
  static final String enter_valid_email = 'Enter valid email';
  static final String oops = "oops something went wrong";
  static final String home = 'Home';
  static final String logoutConfirmationMessage =
      'Are you sure want to logout?';
  static final String no = 'No';
  static final String yes = 'Yes';

  /*Home*/
  static final String text_my_property = 'My properties';
  static final String text_add_edit_property = 'Add/Edit property';
  static final String text_order_history = 'Order History';
  static final String text_example_work = 'Example Work';
  static final String text_contact_admin = 'Contact Admin';
  static final String text_calendar = 'Calendar';
  static final String logout = 'LogOut';

  static final String enter_confirm_username = "Please enter confirm password";
  static final String enter_valid_mobile = "Please enter valid mobile";
  static var dont_have_account = "Don't have account ? ";
  static String signup = "SignUp";
  static var agent = "Agent";
  static var owner = "Owner";
  static final String already_have_account = "Already have a account";
  static var confirm_password = "Confirm password";
  static var enter_phone = "Phone number";
  static var register = "Register";
  static final String enter_email = "Please email Adress";

  static var want_to_do = "Want to do a video ad?";
  static var property_detail = "Property Details";

  static var property_features = "Property Features";

  static String text_signout = "SignOut";

  static var add_image = "Add Image";

  static var property_name = "Property Name";

  static var property_address = "Property Address";

  static var property_price = "Property Price in dollars";

  static var change_agent = "Change Agent";

  static var select = "Select";

  static String profile = "Profile";

  static String mobile_number = "Mobile Number";

  static String email = "Email";

  static String text_camera = "Camera";

  static String text_gallery = "Gallery";

  static var change_password = "Change Password";

  static String old_password = "Old Password";

  static var new_password = "New Password";

  static var submit = "Submit";

  static var edit = "Edit";

  static var state = "State";

  static var suberb = "Suberb";

  static var property_info = "Property Information";

  static var estimated_slaes_price = "Estimated sales price";

  static var postcode = "Postcode";

  static var rental_estaimated = "For Rental Estimated Weekly Rental Price";

  static String enter_data = "Enter Data";

  static String subject = "Subject";

  static String descrition = "Description";

  static var send = "Send";

  static String agency_name = "Agency Name";

  static var please_video_style = "Please choose your video style";

  static var choose_spend = "Choose Your Spend";

  static var next = "Next";
  static var dummyParagraph =
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,";

  static var thank_you = "Thank you and good luck with your property!";
  static var your_video_live = "Your video should be live on Facebook within new 6-12 hours,you will get an emil with a link when it is live!";

  static var back_to_home ="Back To Home";

  static var video_style= "Video Style";

  static var how_many_views ="How many views would you like ? ";

  static var view_video_styles ="View video styles";

  static var enter_address ="Please enter Address";

  static var enter_suberb ="Please enter suberb";

  static var enter_state ="Please enter state";

  static var please_enter_postcode ="Please enter postcode";

  static var please_enter_sales_price = "Please enter sales price";

  static var enter_rental_price = "Please enter rental price";

  static var enter_property_info = "Please enter property info";

  static  final String DEVICE_ACCESS_ANDROID  ="1" ;
  static  final String DEVICE_ACCESS_IOS  ="2" ;

  static String text_no_data_found = "No data found!!";

  static var userTypeOwner ="owner";

  static var userTypeAgent = "agent";

  static var ownerList ="Owner List";

  static var agentList ="Agent List";

  static var please_wait = "Please wait....";

  static var select_price = "Sele Price";

  static var enter_name = "Please enter name";

  static var fullName = "Full Name";

  static var done ="Done";

  static var text_add_property = "Add Property";

  static var enter_property_name ="Please enter proprty name";

  static String property_image_required = "Please select alteast one property image";

  static String property_location_required = "property Location required";

  static var select_agent = "Select Agent";
  static OwnerPropertyDetails ownerPropertyDetails  ;

  static var text_edit_property = "Edit Property";

  static var save = "Save";

  static String logout_success = "Logout Successfully";

  static var order_history_details = "Order History Detail";

  static var price_hint = "\$1000000";

  static var pay = "Pay";

  static var owner_name = "Owner Name";

  static var change_owner = "Change Owner";

  static var select_owner = "Select Owner";

  static String profile_updated = "Profile updated succesfully!!";

  static var isAdded= "isAdded";

  static Object propertyDetailsKey ="propertyDetails";

  static var agent_name = "Agent name";

  static var enter_description = "Enter Description";

  static var enter_subject = "Please enter subject";

  static String property_added = "Property added sucessfully!!";

  static String message_send_to_admin = "Message send to admin successfully!!";

  static String you_cant_change = "You can't change";
}
