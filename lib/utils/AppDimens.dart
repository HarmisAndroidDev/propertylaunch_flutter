
import 'package:flutter/material.dart';
class AppDimens {
  static final double appBarImageHeight = 26.0;
  static final double appBarImageWidth = 45.0;
  static final double appBarTitleSize = 20.0;
  static final double  edtTextSize = 20.0;
  static final double btn_height = 40.0;

}
