import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:property_launch/model/ownerPropertyList/OwnerPropertyDetails.dart';
import 'package:property_launch/ui/customUi/CTextview.dart';
import 'package:property_launch/ui/customUi/MyCustomDialog.dart';
import 'package:property_launch/utils/AppConstants.dart';
import 'package:property_launch/utils/CommonColors.dart';
import 'package:connectivity/connectivity.dart';

class CommonUtils {

  static bool isPropertyAdded = false;
  static bool isPropertyUpdated = false;
  static OwnerPropertyDetails ownerPropertyDetails = null;


  static void showErrorMessage(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(new SnackBar(
      content: new Text(message),
      duration: new Duration(seconds: 2),
    ));
  }

  static Future<bool> isConnected() async {
    bool isConnected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result[0].rawAddress.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        isConnected = true;
      }
    } on SocketException catch (_) {
      isConnected = false;
    }
    return isConnected;
  }

  static Future<bool>  isInternetConnected()async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a wifi network.
      return true;
    }

    return false;
  }

  static bool isEmpty(String string) {
    return string == null || string.length == 0;
  }

  static Widget getProgressBar() {
    return new Stack(
      children: [
        new Opacity(
          opacity: 0.2,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  static Widget getListItemProgressBar() {
    return new Container(
      height: 60,
      child: new Center(
        child: new CircularProgressIndicator(),
      ),
    );
  }

  static void enableStatusBar(Color color) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: color));
  }

  static void hideStatusBar() {
    // To make this screen full screen.
    // It will hide status bar and notch.
    SystemChrome.setEnabledSystemUIOverlays([]);

    // full screen image for splash screen.
  }

  static Widget showProgressBar(Widget body) {
    return new Stack(
      children: <Widget>[
        body,
        new Container(
          color: Colors.black45,
          height: double.infinity,
          width: double.infinity,
          child: new Center(child: new CircularProgressIndicator()),
        )
      ],
    );
  }

  static bool isvalidEmail(String email) {
    bool emailValid =
        RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
    return emailValid;
  }

  static bool isValidMobile(String mobile) {
    if (mobile.length != 10) {
      return false;
    } else {
      return true;
    }
  }

  static void setPotraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  static void showProgressDialog(BuildContext context){

    MyCustomDialog changePasswordDialog = MyCustomDialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      //this right here
      child: Container(

        padding: EdgeInsets.all(10.0),
        alignment: Alignment.center,
        width: 200,
        height: 200,
        child: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new CircularProgressIndicator(),new CTextView(
                margin: EdgeInsets.only(top: 20.0),
                textSize: 14,
                textColor: CommonColors.black,
                text: AppConstants.please_wait,
                textAlign: TextAlign.center,
              )
            ],
          ) ,
        ),
      ),
    );

    /*showDialog(
        context: context,
        builder: (BuildContext context) {
          return changePasswordDialog;
        });*/
    
      showMyDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            return changePasswordDialog;
          });






  }

  static void hideProgressDialog(BuildContext context){
      Navigator.of(context, rootNavigator: true).pop('dialog');
  }
}
