class ApiParams {
  static final String email = "email";
  static final String password = 'password';

  static var json_content = "json_content";

  static String images ="images[]";
  static String image ="image";

  static var device  ="device";

  static var deviceToken ="deviceToken";

  static var ownerid = "ownerid";
  static var userType = "userType";

  static var fullName = "fullName";

  static var deviceAccess = "deviceAccess";

  static var phone = "phone";
  static var uType = "uType";

  static var proId = "pro_id";
  static var propertyid = "propertyid";
  static var imageId = "id";

  static var amount= "amount";

  static var cardName="cardName";

  static var cardNumber ="cardNumber";

  static var cvc = "cvc";

  static var token ="token";

  static var expMonth = "expMonth";

  static var expYear = "expYear";

  static var userId = "userId";

  static var subject = "subject";

  static var message = "message";

  static var oldPassword = "oldPassword";

  static var name = "name";

  static var propertyId = "propertyId";

  static var ownerId = "ownerId";
}
