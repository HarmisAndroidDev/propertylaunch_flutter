import 'dart:ui';

class CommonColors {
  static const primaryColor =  Color(0xFF268fc9);
  static const PrimaryAssentColor =  Color(0xFF268fc9);
  static const PrimaryDarkColor =  Color(0xFF268fc9);
  static const ErroColor =  Color(0xFFff0000);
  static const underlineColor =  Color(0xFF73bfe9);
  static const appBarColor = Color(0xFF268fc9);
  static const black = Color(0xFF000000);
  static const tabColor = Color(0xFFe7f6ff);
  static const tabIndicatorColor = Color(0xFF349dd7);

  static const color_636363 =  Color(0xFF636363);
  static const color_363636 =  Color(0xFF363636);
  static const color_cbcbcb =  Color(0xFFcbcbcb);
  static const color_a9a9a9 =  Color(0xFFa9a9a9);
  static const color_040404 =  Color(0xFF040404);
  static const middle_date_color =  Color(0xFFE3F2FD);
  static const white_80 =  Color(0x80FFFFFF);
  static const login_statusbar_color = Color(0x802A8FC6);
  static const edt_border_color = Color(0xFFcbcbcb);
  static const blue_btn_color = Color(0xFF268fc9);
  static const hint_text_color = Color(0xFFcbcbcb) ;
  static const text_color = Color(0xFF808080) ;
  static const box_shadow_color = Color(0xFFcbcbcb) ;
  static const blackTramsperent = Color(0x99000000) ;
  static const btnBlueLight = Color(0x99F0F8FF) ;

  static const white =Color(0x99FFFFFF) ;


}