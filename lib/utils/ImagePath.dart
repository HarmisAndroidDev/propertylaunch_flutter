class LocalImages {
  static final String ic_place_holder = 'https://homepages.cae.wisc.edu/~ece533/images/pool.png';
  static final String ic_launcher = 'images/ic_launcher.png';
  static final String ic_down = 'images/ic_down.png';
  static final String ic_logo = 'images/ic_logo.png';
  static final String ic_drawer = 'images/ic_drawer.png';
  static final String ic_notification = 'images/ic_notification.png';
  static final String ic_add_edit_property_inactive = 'images/ic_add_edit_property_inactive.png';
  static final String ic_calendar = 'images/ic_calendar.png';
  static final String ic_contact_admin_inactive = 'images/ic_contact_admin_inactive.png';
  static final String ic_example_work_inactive = 'images/ic_example_work_inactive.png';
  static final String ic_order_history_inactive = 'images/ic_order_history_inactive.png';
  static final String ic_sign_out = 'images/ic_sign_out.png';
  static final String ic_my_property_active = 'images/ic_my_property_active.png';
  static final String img_logo = 'images/img_logo.png';
  static final String ic_agent = 'images/ic_agent.png';

  static String ic_camera = "images/ic_camera.png";
  static String ic_edit = "images/ic_edit.png";
  static String ic_add_photo = "images/ic_add_photo.png";
  static String img_default_image = "images/default_image.jpeg";
  static String ic_bad = "images/ic_bad.png";
  static String ic_bathtub = "images/ic_bathtub.png";
  static String ic_car = "images/ic_car.png";
}
